package com.cws.easybill.portal.merchantportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class})
public class MerchantPortalApplication {

	public static final String APP_NAME = "portal-ui-app";

	public static void main(String[] args) {
		SpringApplication.run(MerchantPortalApplication.class, args);
	}

}
