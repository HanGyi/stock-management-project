package com.cws.easybill.portal.merchantportal.common.annotation;

import com.cws.easybill.portal.merchantportal.common.response.PageMode;
import com.cws.easybill.portal.merchantportal.common.validation.BaseValidator;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target(METHOD)
public @interface ValidateEntity {
	Class<? extends BaseValidator> validator();

	String errorView() default "";

	PageMode pageMode() default PageMode.VIEW;
}
