package com.cws.easybill.portal.merchantportal.common.exceptionHandlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;

// To handle all exceptions out of Spring Controllers
@Component
public class ErrorHandlerFilter implements Filter {
	private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException {
		HttpServletRequest httpServletRequest = ((HttpServletRequest) request);

		HttpServletResponse httpServletResponse = ((HttpServletResponse) response);
		try {
			filterChain.doFilter(request, response);
		}
		catch (Exception e) {
			if (!response.isCommitted()) {
				if (httpServletRequest.getRequestURI().contains("/api/") || "XMLHttpRequest".equals(httpServletRequest.getHeader("X-Requested-With"))) {
					ObjectMapper mapper = new ObjectMapper();
					JsonObject jsonObject = new JsonObject();
					jsonObject.addProperty("timestamp", Instant.now().toString());
					jsonObject.addProperty("status", HttpStatus.FORBIDDEN.value());
					jsonObject.addProperty("error", HttpStatus.FORBIDDEN.getReasonPhrase());
					jsonObject.addProperty("message", "Unknown exception occured !");
					jsonObject.addProperty("path", httpServletRequest.getRequestURI());

					httpServletResponse.setStatus(HttpStatus.FORBIDDEN.value());
					httpServletResponse.getWriter().write(mapper.writeValueAsString(jsonObject));
				}
				else {
					redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/error/500");
				}
			}
		}
	}
}

