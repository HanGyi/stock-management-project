package com.cws.easybill.portal.merchantportal.common.response;

public enum PageMode {
	VIEW, CREATE, EDIT, DELETE, PROFILE, COMFIRM_REGISTER
}
