/*******************************************************************************
 * Copyright (C) 2018 Creative Web Studio
 * (http://www.creative-webstudio.com) All Rights Reserved.
 * easypay-persistence - merchantPortalDataSourceConfig.java
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package com.cws.easybill.portal.merchantportal.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = {"com.cws.easybill.portal.merchantportal.persistence.mapper"}, sqlSessionFactoryRef = MerchantPortalDataSourceConfig.SQL_SESSION_FACTORY_NAME)
public class MerchantPortalDataSourceConfig {

	public static final String SQL_SESSION_FACTORY_NAME = "merchantPortalSqlSessionFactory";
	public static final String TX_MANAGER = "merchantPortalTransactionManager";

	@Bean(name = "merchantPortalDSConfig")
	@ConfigurationProperties(prefix = "datasource.merchantportal-ds")
	public HikariConfig hikariConfig() {
		return new HikariConfig();
	}

	@Primary
	@Bean(name = "merchantPortalDataSource")
	public DataSource merchantPortalDataSource(@Qualifier("merchantPortalDSConfig") HikariConfig config) {
		return new HikariDataSource(config);
	}

	@Bean(name = MerchantPortalDataSourceConfig.SQL_SESSION_FACTORY_NAME)
	public SqlSessionFactory merchantPortalSqlSessionFactory(@Qualifier("merchantPortalDataSource") DataSource dataSource) throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource);
		bean.setConfigLocation(new PathMatchingResourcePatternResolver().getResource("classpath:mybatis-config.xml"));
		bean.setTypeAliasesPackage("com.cws.easybill.portal.merchantportal.model.bean,com.cws.easybill.portal.merchantportal.model.dto");
		bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:merchantportal-mybatis/mapper/*.xml"));
		return bean.getObject();
	}

	@Bean(name = "merchantPortalSqlSessionTemplate")
	public SqlSessionTemplate merchantPortalSqlSessionTemplate(@Qualifier("merchantPortalSqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

	@Bean(name = MerchantPortalDataSourceConfig.TX_MANAGER)
	public DataSourceTransactionManager merchantPortalTransactionManager(@Qualifier("merchantPortalDataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}
}
