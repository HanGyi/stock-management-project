//package com.cws.easybill.portal.merchantportal.config;
//
//import org.springframework.boot.web.client.RestTemplateBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.util.DefaultUriBuilderFactory;
//
//import java.time.Duration;
//
//@Configuration
//public class RestTemplateConfig {
//	@Bean
//	public RestTemplate restTemplate(RestTemplateBuilder builder) {
//		RestTemplate restTemplate = builder.setConnectTimeout(Duration.ofMinutes(3)).setReadTimeout(Duration.ofMinutes(3)).build();
//		DefaultUriBuilderFactory defaultUriBuilderFactory = new DefaultUriBuilderFactory();
//		defaultUriBuilderFactory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.NONE);
//		restTemplate.setUriTemplateHandler(defaultUriBuilderFactory);
//		return restTemplate;
//	}
//}
