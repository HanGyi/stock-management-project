/*******************************************************************************
 * Copyright (C) 2018 Creative Web Studio
 * (http://www.creative-webstudio.com) All Rights Reserved.
 * easypay-ui-app - AuthenticationUserService.java
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package com.cws.easybill.portal.merchantportal.config.security;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import static com.cws.easybill.portal.merchantportal.util.LoggerConstants.*;

@Component
public class AuthenticationUserService implements UserDetailsService {
	private static final Logger applicationLogger = LogManager.getLogger("applicationLogs." + AuthenticationUserService.class.getName());
	private static final Logger errorLogger = LogManager.getLogger("errorLogs." + AuthenticationUserService.class.getName());

	@Autowired
	private AdministratorRepository repository;

	@Override
	public final UserDetails loadUserByUsername(String loginId) throws UsernameNotFoundException {
		applicationLogger.info(LOG_BREAKER_OPEN);
		try {
			AuthenticatedClientBean client = repository.selectAuthenticatedClient(loginId);
			if (client == null) {
				throw new UsernameNotFoundException("Login User doesn`t exist");
			}
			applicationLogger.info(LOG_PREFIX + "Roles of :" + client.getName() + " are " + client.getRoles() + LOG_SUFFIX);
			// pass authUser object and roles to LoggedUser
			LoggedUserBean loggedUser = new LoggedUserBean(client, client.getRoles());
			applicationLogger.info(LOG_BREAKER_CLOSE);
			return loggedUser;
		}
		catch (DAOException e) {

			errorLogger.error(LOG_PREFIX + "Login User doesn`t exist." + LOG_SUFFIX, e);
			return null;
		}
	}
}
