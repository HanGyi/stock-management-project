/*******************************************************************************
 * Copyright (C) 2018 Creative Web Studio
 * (http://www.creative-webstudio.com) All Rights Reserved.
 * easypay-ui-app - RememberMeAuthenticationSuccessHandler.java
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package com.cws.easybill.portal.merchantportal.config.security;

import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.bean.LoginHistoryBean;
import com.cws.easybill.portal.merchantportal.persistence.repository.LoginHistoryRepository;
import com.cws.easybill.portal.merchantportal.util.SecurityUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

import static com.cws.easybill.portal.merchantportal.util.LoggerConstants.LOG_PREFIX;
import static com.cws.easybill.portal.merchantportal.util.LoggerConstants.LOG_SUFFIX;

@Component
public class RememberMeAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	private static final Logger applicationLogger = LogManager.getLogger("applicationLogs." + RememberMeAuthenticationSuccessHandler.class.getName());
	private static final Logger errorLogger = LogManager.getLogger("errorLogs." + RememberMeAuthenticationSuccessHandler.class.getName());
	@Autowired
	private LoginHistoryRepository repository;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

		String loginId = authentication.getName();
		applicationLogger.info(LOG_PREFIX + "Login User with Login ID '" + loginId + "' has successfully signed in." + LOG_SUFFIX);
		try {
			LoggedUserBean loginUser = (LoggedUserBean) authentication.getPrincipal();
			AuthenticatedClientBean client = loginUser.getUserDetail();
			LoginHistoryBean loginHistory = new LoginHistoryBean();
			loginHistory.setIpAddress(SecurityUtil.getClientIp(request));
			loginHistory.setOs(SecurityUtil.getOperatingSystem(request));
			loginHistory.setClientAgent(SecurityUtil.getUserAgent(request));
			loginHistory.setLoginDate(LocalDateTime.now());
			loginHistory.setClientId(client.getId());
			loginHistory.setClientType(client.getClientType());
			repository.insert(loginHistory, client.getId());
			applicationLogger.info(LOG_PREFIX + "Recorded in loginHistory for Login ID '" + loginId + "'." + LOG_SUFFIX);
		}
		catch (Exception e) {

			errorLogger.error(LOG_PREFIX + "Can't save in loginHistory for Login ID '" + loginId + "'" + LOG_SUFFIX, e);
		}

		super.setAlwaysUseDefaultTargetUrl(true);
		super.setDefaultTargetUrl(request.getRequestURL().toString());
		super.onAuthenticationSuccess(request, response, authentication);
	}
}
