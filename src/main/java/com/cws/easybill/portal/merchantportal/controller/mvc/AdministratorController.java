package com.cws.easybill.portal.merchantportal.controller.mvc;

import com.cws.easybill.portal.merchantportal.common.annotation.MVCLoggable;
import com.cws.easybill.portal.merchantportal.common.annotation.ValidateEntity;
import com.cws.easybill.portal.merchantportal.common.response.PageMessageStyle;
import com.cws.easybill.portal.merchantportal.common.response.PageMode;
import com.cws.easybill.portal.merchantportal.exception.*;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.dto.AdminDto;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRoleRepository;
import com.cws.easybill.portal.merchantportal.validators.AdminValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@MVCLoggable(profile = "dev")
@RequestMapping("/admins")
public class AdministratorController extends BaseMVCController {

	private static final Logger applicationLogger = LogManager.getLogger("applicationLogs." + AdministratorController.class.getName());

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AdministratorRepository repository;

	@Autowired
	private AdministratorRoleRepository adminRoleRepository;

	@GetMapping
	public String home() {
		return "/admin/home";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("pageMode", PageMode.CREATE);
		model.addAttribute("adminDto", new AdminDto());
		return "/admin/input";
	}

	@PostMapping("/add")
	@ValidateEntity(validator = AdminValidator.class, errorView = "/admin/input", pageMode = PageMode.CREATE)
	public String add(Model model, RedirectAttributes redirectAttributes, @ModelAttribute("adminDto") AdminDto admin, BindingResult bindResult) throws BusinessException, DuplicatedEntryException {
		admin.setPassword(passwordEncoder.encode(admin.getPassword()));
		long registeredAdminId = repository.createNewAdminWithRoles(admin, admin.getRoleIds(), getSignInClientId());
		applicationLogger.info("New administrator has been successfully registered with ID# <{}>", registeredAdminId);
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyRegistered", PageMessageStyle.SUCCESS, "Administrator");
		return "redirect:/admins";
	}

	@GetMapping("/{id}/edit")
	public String edit(@PathVariable long id, Model model) throws DAOException {
		model.addAttribute("pageMode", PageMode.EDIT);
		AdministratorBean admin = repository.select(id);
		if (admin == null) {
			throw new ContentNotFoundException("No matching administrator found for give ID # <" + id + ">");
		}

		AdminDto adminDto = new AdminDto();
		adminDto.setId(admin.getId());
		adminDto.setContentId(admin.getContentId());
		adminDto.setName(admin.getName());
		adminDto.setLoginId(admin.getLoginId());
		adminDto.setStatus(admin.getStatus());

		adminDto.setRoleIds(adminRoleRepository.selectByKey1(id));
		model.addAttribute("adminDto", adminDto);
		return "/admin/input";
	}

	@PostMapping("/{adminId}/edit")
	@ValidateEntity(validator = AdminValidator.class, errorView = "/admin/input", pageMode = PageMode.EDIT)
	public String edit(Model model, @PathVariable long adminId, RedirectAttributes redirectAttributes, @ModelAttribute("adminDto") AdminDto adminDto, BindingResult bindResult) throws DAOException, DuplicatedEntryException, BusinessException {
		AdministratorBean admin = repository.select(adminId);
		if (admin == null) {
			throw new ContentNotFoundException("No matching administrator found for give ID # <" + adminId + ">");
		}

		adminDto.setId(adminId);
		repository.updateAdminWithRoles(adminDto, adminDto.getRoleIds(), getSignInClientId());
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyUpdated", PageMessageStyle.SUCCESS, "Administrator");
		return "redirect:/admins";
	}

	@GetMapping("/{id}")
	public String detail(@PathVariable long id, Model model) throws DAOException {
		model.addAttribute("pageMode", PageMode.VIEW);
		AdministratorBean admin = repository.select(id);
		if (admin == null) {
			throw new ContentNotFoundException();
		}
		model.addAttribute("adminDto", admin);
		return "/admin/detail";
	}

	@GetMapping("/{id}/delete")
	public String delete(@PathVariable long id, RedirectAttributes redirectAttributes) throws DAOException, ConsistencyViolationException {
		if (id == getSignInClientId()) {
			setPageMessage(redirectAttributes, "Error", "Notification.User.Remove.Failed", PageMessageStyle.ERROR);
			return "redirect:/admins";
		}
		AdministratorBean admin = repository.select(id);
		if (admin == null) {
			throw new ContentNotFoundException();
		}
		repository.delete(id, getSignInClientId());
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyRemoved", PageMessageStyle.SUCCESS, "Administrator");
		return "redirect:/admins";
	}

	@Override
	public void subInit(Model model) {
		setAuthorities(model, "Administrator");
	}
}
