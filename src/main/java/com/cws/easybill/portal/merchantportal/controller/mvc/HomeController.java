package com.cws.easybill.portal.merchantportal.controller.mvc;

import com.cws.easybill.portal.merchantportal.common.thymeleaf.Layout;
import com.cws.easybill.portal.merchantportal.config.security.LoggedUserBean;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.UserRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.OrderRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.StockRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

@Controller
@RequestMapping("/")
public class HomeController extends BaseMVCController {
	@Autowired
	private AdministratorRepository administratorRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private StockRepository stockRepository;

	@Autowired
	private OrderRepository orderRepository;

	@GetMapping
	public String home() {
		return "redirect:/dashboard";
	}

	@GetMapping("/dashboard")
	public String dashboardPage(Model model, HttpServletRequest request) throws DAOException {
		//get sign in client info
		LoggedUserBean userInfo = getSignInClientInfo();
		String pftMessage = null;
		Long lessCount = 0L;
		setAuthorities(model, "Dashboard");
		if(userInfo.getClientType() == AuthenticatedClientBean.ClientType.USER){
			AdministratorBean admin = administratorRepository.select(getSignInClientId());
			model.addAttribute("userId",admin.getUserId());
			return "user_dashboard";
		}
		return "dashboard";
	}

	@Layout("error")
	@GetMapping("/error/{code}")
	public String errorPage(Model model, @RequestHeader(value = HttpHeaders.REFERER, required = false) String referer, @PathVariable String code) {
		model.addAttribute("pageName", "Error !");
		model.addAttribute("referer", referer);
		return "error/" + code;
	}

	@Layout("error")
	@GetMapping("/404.html")
	public String pageNotFound(Model model) {
		model.addAttribute("pageName", "Error !");
		return "error/404";
	}

	@Layout("error")
	@GetMapping("/accessDenied")
	public String accessDenied(Model model) {
		model.addAttribute("pageName", "Error !");
		return "error/403";
	}

	@Override
	public void subInit(Model model) {

	}

	private Date getLocalAcceptOrgTodayStartTime(BigDecimal localTimeZone) {
		Date date = new Date();
		float timezone = NumberUtils.toFloat(localTimeZone.toString(), 8) - 8;
		int minutes = (int) (timezone * 60);
		date = DateUtils.addMinutes(date, minutes);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
}
