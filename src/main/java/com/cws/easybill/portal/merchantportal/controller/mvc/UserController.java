package com.cws.easybill.portal.merchantportal.controller.mvc;

import com.cws.easybill.portal.merchantportal.common.annotation.MVCLoggable;
import com.cws.easybill.portal.merchantportal.common.annotation.ValidateEntity;
import com.cws.easybill.portal.merchantportal.common.response.PageMessageStyle;
import com.cws.easybill.portal.merchantportal.common.response.PageMode;
import com.cws.easybill.portal.merchantportal.common.validation.FieldValidator;
import com.cws.easybill.portal.merchantportal.exception.*;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import com.cws.easybill.portal.merchantportal.model.dto.PasswordDto;
import com.cws.easybill.portal.merchantportal.persistence.repository.UserRepository;
import com.cws.easybill.portal.merchantportal.validators.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.Map;

@Controller
@MVCLoggable(profile = "dev")
@RequestMapping("/users")
public class UserController extends BaseMVCController {

	private static final Logger applicationLogger = LogManager.getLogger("applicationLogs." + UserController.class.getName());

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private UserRepository repository;

	@GetMapping
	public String home() {
		return "/user/home";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("pageMode", PageMode.CREATE);
		model.addAttribute("user", new UserBean());
		return "/user/input";
	}

	@PostMapping("/add")
	@ValidateEntity(validator = UserValidator.class, errorView = "/user/input", pageMode = PageMode.CREATE)
	public String add(Model model, RedirectAttributes redirectAttributes, @ModelAttribute("user") UserBean user, BindingResult bindResult) throws DAOException, DuplicatedEntryException, BusinessException {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		long registerUserId = repository.createNewUserWithRoles(user, getSignInClientId());
		applicationLogger.info("New user has been successfully registered with ID# <{}>", registerUserId);
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyRegistered", PageMessageStyle.SUCCESS, "User");
		return "redirect:/users";
	}

	@GetMapping("/{id}/edit")
	public String edit(@PathVariable long id, Model model) throws DAOException {
		model.addAttribute("pageMode", PageMode.EDIT);
		UserBean user = repository.select(id);
		if (user == null) {
			throw new ContentNotFoundException("No matching user found for give ID # <" + id + ">");
		}
		model.addAttribute("user", user);
		return "/user/input";
	}

	@PostMapping("/{userId}/edit")
	@ValidateEntity(validator = UserValidator.class, errorView = "/user/input", pageMode = PageMode.EDIT)
	public String edit(Model model, @PathVariable long userId, RedirectAttributes redirectAttributes, @ModelAttribute("user") UserBean user, BindingResult bindResult) throws DAOException, DuplicatedEntryException, BusinessException {
		UserBean oldUser = repository.select(userId);
		if (user == null) {
			throw new ContentNotFoundException("No matching user found for give ID # <" + userId + ">");
		}
		oldUser.setName(user.getName());
		oldUser.setEmail(user.getEmail());
		oldUser.setStockLimit(user.getStockLimit());
		oldUser.setStatus(user.getStatus());
		oldUser.setCurrency(user.getCurrency());
		repository.updateUser(oldUser,getSignInClientId());
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyUpdated", PageMessageStyle.SUCCESS, "User");
		return "redirect:/users";
	}

	@GetMapping("/{id}")
	public String detail(@PathVariable long id, Model model) throws DAOException {
		model.addAttribute("pageMode", PageMode.VIEW);
		UserBean user = repository.select(id);
		if (user == null) {
			throw new ContentNotFoundException();
		}
		model.addAttribute("user", user);
		return "/user/detail";
	}

	@GetMapping("/{id}/delete")
	public String delete(@PathVariable long id, RedirectAttributes redirectAttributes) throws DAOException, ConsistencyViolationException, BusinessException {
		if (id == getSignInClientId()) {
			setPageMessage(redirectAttributes, "Error", "Notification.User.Remove.Failed", PageMessageStyle.ERROR);
			return "redirect:/users";
		}
		UserBean user = repository.select(id);
		if (user == null) {
			throw new ContentNotFoundException();
		}
		repository.deleteUser(id, getSignInClientId());
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyRemoved", PageMessageStyle.SUCCESS, "User");
		return "redirect:/users";
	}


	@GetMapping("/profile")
	public String profile(Model model) throws BusinessException, DAOException {
		UserBean user = repository.select(getUserId());
		if (user == null) {
			throw new ContentNotFoundException("User Not found !");
		}
		model.addAttribute("user", user);
		return "profile/user_profile";
	}

	@PostMapping("/profile")
	public String profile(@ModelAttribute("user") UserBean user,Model model,RedirectAttributes redirectAttributes) throws BusinessException, DAOException, DuplicatedEntryException {
		//get login user id
		user.setId(getUserId());
		repository.updateUser(user,getSignInClientId());
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyUpdated", PageMessageStyle.SUCCESS, "Profile");
		return "redirect:/users/profile";

	}

	@PostMapping("/update/password")
	//@HandleAjaxException
	public @ResponseBody Map<String, Object> changedPassword(@RequestParam String oldPassword, @RequestParam String newPassword, @RequestParam String confirmPassword) throws DAOException, DuplicatedEntryException, BusinessException {
		PasswordDto passwordDto = new PasswordDto();
		passwordDto.setOldPassword(oldPassword);
		passwordDto.setNewPassword(newPassword);
		passwordDto.setConfirmPassword(confirmPassword);
		Errors errors = new BeanPropertyBindingResult(passwordDto, "passwordDto");
		// validate passwords
		baseValidator.validateIsEqual("confirmPassword", new FieldValidator("newPassword", "Password", passwordDto.getNewPassword(), errors), new FieldValidator("confirmPassword", "Confirm Password", passwordDto.getConfirmPassword(), errors), errors);
		baseValidator.validateIsValidMinValue(new FieldValidator("oldPassword", "Old Password", passwordDto.getOldPassword(), errors), 8);
		baseValidator.validateIsValidMinValue(new FieldValidator("newPassword", "Password", passwordDto.getNewPassword(), errors), 8);
		baseValidator.validateIsValidMinValue(new FieldValidator("confirmPassword", "Confirm Password", passwordDto.getConfirmPassword(), errors), 8);
		UserBean user = repository.select(getUserId());

		//check old password correct or not
		if(!passwordEncoder.matches(oldPassword,user.getPassword())) {
			errors.rejectValue("oldPassword", "", "Incorrect old password.Try again !");
		}
		if (errors.hasErrors()) {
			return setAjaxFormFieldErrors(errors, "change_");
		}
		//add new password
		user.setPassword(passwordEncoder.encode(newPassword));
		long efectedRows = repository.updatePassword(user,getSignInClientId());

		Map<String, Object> response = new HashMap<>();
		if (efectedRows > 0) {
			response.put("status", HttpStatus.OK);
			return setAjaxPageMessage(response, "Success", "Notification.User.PasswordUpdate.Success", PageMessageStyle.SUCCESS);
		}
		else {
			response.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			return setAjaxPageMessage(response, "Password update failed !", "Notification.User.PasswordUpdate.FAILED", PageMessageStyle.ERROR);
		}
	}



	@Override
	public void subInit(Model model) {
		setAuthorities(model, "User");
	}
}
