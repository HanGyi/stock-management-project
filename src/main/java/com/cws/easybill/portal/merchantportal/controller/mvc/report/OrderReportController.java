package com.cws.easybill.portal.merchantportal.controller.mvc.report;

import com.cws.easybill.portal.merchantportal.common.enums.ExportType;
import com.cws.easybill.portal.merchantportal.controller.rest.BaseRESTController;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.OrderCriteria;
import com.cws.easybill.portal.merchantportal.service.report.OrderReportService;
import jakarta.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/ordReport")
public class OrderReportController extends BaseRESTController {

	@Autowired
	private OrderReportService orderReportService;

	//Test
	@GetMapping("/report")
	public String generateReport() {
		return orderReportService.generateReport();
	}

	@GetMapping("/reports")
	public ResponseEntity<Void> generateReport(@RequestParam(value="exportType") ExportType exportType, HttpServletResponse response) throws DAOException, JRException, IOException {
		OrderCriteria criteria = new OrderCriteria();
		criteria.setUserId(getUserId());
		criteria.setReportType(exportType);
		orderReportService.generateReport(criteria,response);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/reports/month")
	public ResponseEntity<Void> generateCurMonthReport(@RequestParam(value="exportType") ExportType exportType, HttpServletResponse response) throws DAOException, JRException, IOException {
		OrderCriteria criteria = new OrderCriteria();
		criteria.setUserId(getUserId());
		criteria.setReportType(exportType);
		orderReportService.generateCurMonthReport(criteria,response);
		return ResponseEntity.ok().build();
	}

	//Test
	@PostMapping("/reports/generate")
	public ResponseEntity<Void> generateReports(@RequestBody OrderCriteria orderCriteria, HttpServletResponse response) throws DAOException, JRException, IOException {
		orderReportService.generateReportWithCriteria(orderCriteria,response);
		return ResponseEntity.ok().build();
	}

}


