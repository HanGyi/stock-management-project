package com.cws.easybill.portal.merchantportal.controller.mvc.report;

import com.cws.easybill.portal.merchantportal.common.enums.ExportType;
import com.cws.easybill.portal.merchantportal.controller.rest.BaseRESTController;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.service.report.StockReportService;
import jakarta.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/stockReport")
public class StockReportController extends BaseRESTController {
	@Autowired
	private StockReportService stockReportService;

	@GetMapping("/lessOfStock")
	public ResponseEntity<Void> genLessOfReport(@RequestParam(value="exportType") ExportType exportType, HttpServletResponse response) throws DAOException, JRException, IOException {
		StockCriteria criteria = new StockCriteria();
		criteria.setUserId(getUserId());
		criteria.setReportType(exportType);
		stockReportService.genLessStockReport(criteria,response);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/outOfStock")
	public ResponseEntity<Void> genOutOfReport(@RequestParam(value="exportType") ExportType exportType, HttpServletResponse response) throws DAOException, JRException, IOException {
		StockCriteria criteria = new StockCriteria();
		criteria.setUserId(getUserId());
		criteria.setReportType(exportType);
		stockReportService.genOutofStockReport(criteria,response);
		return ResponseEntity.ok().build();
	}
}
