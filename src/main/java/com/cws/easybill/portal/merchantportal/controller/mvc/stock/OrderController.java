package com.cws.easybill.portal.merchantportal.controller.mvc.stock;

import com.cws.easybill.portal.merchantportal.common.annotation.MVCLoggable;
import com.cws.easybill.portal.merchantportal.common.annotation.ValidateEntity;
import com.cws.easybill.portal.merchantportal.common.response.PageMessageStyle;
import com.cws.easybill.portal.merchantportal.common.response.PageMode;
import com.cws.easybill.portal.merchantportal.config.security.LoggedUserBean;
import com.cws.easybill.portal.merchantportal.controller.mvc.BaseMVCController;
import com.cws.easybill.portal.merchantportal.exception.*;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.bean.stock.OrderBean;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.OrderRepository;
import com.cws.easybill.portal.merchantportal.validators.stock.OrderValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@MVCLoggable(profile = "dev")
@RequestMapping("/orders")
public class OrderController extends BaseMVCController {

	private static final Logger applicationLogger = LogManager.getLogger("applicationLogs." + OrderController.class.getName());

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private OrderRepository repository;

	@Autowired
	private AdministratorRepository administratorRepository;

	@GetMapping
	public String home(Model model) {

		LoggedUserBean loginUser = getSignInClientInfo();
		model.addAttribute("isAdmin",loginUser.getClientType() == AuthenticatedClientBean.ClientType.USER?"false":"true");
		return "/order/home";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("pageMode", PageMode.CREATE);
		model.addAttribute("isAdmin",getSignInClientInfo().getClientType() == AuthenticatedClientBean.ClientType.USER?"false":"true");
		model.addAttribute("order", new OrderBean());
		return "/order/input";
	}

	@PostMapping("/add")
	@ValidateEntity(validator = OrderValidator.class, errorView = "/order/input", pageMode = PageMode.CREATE)
	public String add(Model model, RedirectAttributes redirectAttributes, @ModelAttribute("order") OrderBean order, BindingResult bindResult) throws DAOException, DuplicatedEntryException, BusinessException {
		if(order.getUserId() == null){
			order.setUserId(getUserId());
		}
		long registerOrderId = repository.createOrder(order, getSignInClientId());
		applicationLogger.info("New order has been successfully registered with ID# <{}>", registerOrderId);
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyRegistered", PageMessageStyle.SUCCESS, "Order");
		return "redirect:/orders";
	}

	@GetMapping("/{id}/edit")
	public String edit(@PathVariable long id, Model model) throws DAOException {
		model.addAttribute("pageMode", PageMode.EDIT);
		OrderBean order = repository.select(id);
		if (order == null) {
			throw new ContentNotFoundException("No matching order found for give ID # <" + id + ">");
		}
		model.addAttribute("isAdmin",getSignInClientInfo().getClientType() == AuthenticatedClientBean.ClientType.USER?"false":"true");
		model.addAttribute("order", order);
		return "/order/input";
	}

	@PostMapping("/{orderId}/edit")
	@ValidateEntity(validator = OrderValidator.class, errorView = "/order/input", pageMode = PageMode.EDIT)
	public String edit(Model model, @PathVariable long orderId, RedirectAttributes redirectAttributes, @ModelAttribute("order") OrderBean order, BindingResult bindResult) throws DAOException, DuplicatedEntryException, BusinessException {
		OrderBean oldOrder = repository.select(orderId);
		Integer oldQty = oldOrder.getQuantity();
		if (order == null) {
			throw new ContentNotFoundException("No matching order found for give ID # <" + orderId + ">");
		}
		oldOrder.setStockCode(order.getStockCode());
		oldOrder.setQuantity(order.getQuantity());
		oldOrder.setAmount(order.getAmount());
		oldOrder.setTotalAmount(order.getTotalAmount());
		repository.upateOrder(oldOrder,oldQty,getSignInClientId());
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyUpdated", PageMessageStyle.SUCCESS, "Order");
		return "redirect:/orders";
	}

	@GetMapping("/{id}")
	public String detail(@PathVariable long id, Model model) throws DAOException {
		model.addAttribute("pageMode", PageMode.VIEW);
		OrderBean order = repository.select(id);
		if (order == null) {
			throw new ContentNotFoundException();
		}
		model.addAttribute("order", order);
		return "/order/detail";
	}

	@GetMapping("/{id}/delete")
	public String delete(@PathVariable long id, RedirectAttributes redirectAttributes) throws DAOException, ConsistencyViolationException {
		if (id == getSignInClientId()) {
			setPageMessage(redirectAttributes, "Error", "Notification.Order.Remove.Failed", PageMessageStyle.ERROR);
			return "redirect:/orders";
		}
		OrderBean order = repository.select(id);
		if (order == null) {
			throw new ContentNotFoundException();
		}
		repository.deleteOrder(order,id, getSignInClientId());
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyRemoved", PageMessageStyle.SUCCESS, "Order");
		return "redirect:/orders";
	}

	@Override
	public void subInit(Model model) {
		setAuthorities(model, "Order");
	}
}
