package com.cws.easybill.portal.merchantportal.controller.mvc.stock;

import com.cws.easybill.portal.merchantportal.common.annotation.MVCLoggable;
import com.cws.easybill.portal.merchantportal.common.annotation.ValidateEntity;
import com.cws.easybill.portal.merchantportal.common.response.PageMessageStyle;
import com.cws.easybill.portal.merchantportal.common.response.PageMode;
import com.cws.easybill.portal.merchantportal.config.security.LoggedUserBean;
import com.cws.easybill.portal.merchantportal.controller.mvc.BaseMVCController;
import com.cws.easybill.portal.merchantportal.exception.*;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.bean.stock.StockBean;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.StockRepository;
import com.cws.easybill.portal.merchantportal.validators.stock.StockValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@MVCLoggable(profile = "dev")
@RequestMapping("/stocks")
public class StockController extends BaseMVCController {

	private static final Logger applicationLogger = LogManager.getLogger("applicationLogs." + StockController.class.getName());

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private StockRepository repository;

	@GetMapping
	public String home(Model model) {
		LoggedUserBean loginUser = getSignInClientInfo();
		model.addAttribute("isAdmin",loginUser.getClientType() == AuthenticatedClientBean.ClientType.USER?"false":"true");
		return "/stock/home";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("pageMode", PageMode.CREATE);
		model.addAttribute("isAdmin",getSignInClientInfo().getClientType() == AuthenticatedClientBean.ClientType.USER?"false":"true");
		model.addAttribute("stock", new StockBean());
		return "/stock/input";
	}

	@PostMapping("/add")
	@ValidateEntity(validator = StockValidator.class, errorView = "/stock/input", pageMode = PageMode.CREATE)
	public String add(Model model, RedirectAttributes redirectAttributes, @ModelAttribute("stock") StockBean stock, BindingResult bindResult) throws DAOException, DuplicatedEntryException, BusinessException {
		if(stock.getUserId() == null){
			stock.setUserId(getUserId());
		}
		long registerStockId = repository.insert(stock, getSignInClientId());
		applicationLogger.info("New stock has been successfully registered with ID# <{}>", registerStockId);
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyRegistered", PageMessageStyle.SUCCESS, "Stock");
		return "redirect:/stocks";
	}

	@GetMapping("/{id}/edit")
	public String edit(@PathVariable long id, Model model) throws DAOException {
		model.addAttribute("pageMode", PageMode.EDIT);
		StockBean stock = repository.select(id);
		if (stock == null) {
			throw new ContentNotFoundException("No matching stock found for give ID # <" + id + ">");
		}
		model.addAttribute("isAdmin",getSignInClientInfo().getClientType() == AuthenticatedClientBean.ClientType.USER?"false":"true");
		model.addAttribute("stock", stock);
		return "/stock/input";
	}

	@PostMapping("/{stockId}/edit")
	@ValidateEntity(validator = StockValidator.class, errorView = "/stock/input", pageMode = PageMode.EDIT)
	public String edit(Model model, @PathVariable long stockId, RedirectAttributes redirectAttributes, @ModelAttribute("stock") StockBean stock, BindingResult bindResult) throws DAOException, DuplicatedEntryException, BusinessException {
		StockBean oldStock = repository.select(stockId);
		if (stock == null) {
			throw new ContentNotFoundException("No matching stock found for give ID # <" + stockId + ">");
		}
		oldStock.setCode(stock.getCode());
		oldStock.setImageUrl(stock.getImageUrl());
		oldStock.setQuantity(stock.getQuantity());
		oldStock.setOriginalPrice(stock.getOriginalPrice());
		oldStock.setSellPrice(stock.getSellPrice());
		repository.update(oldStock,getSignInClientId());
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyUpdated", PageMessageStyle.SUCCESS, "Stock");
		return "redirect:/stocks";
	}

	@GetMapping("/{id}")
	public String detail(@PathVariable long id, Model model) throws DAOException {
		model.addAttribute("pageMode", PageMode.VIEW);
		StockCriteria criteria = new StockCriteria();
		criteria.setWithUser(true);
		criteria.setId(id);

		StockBean stock = repository.select(criteria);
		if (stock == null) {
			throw new ContentNotFoundException();
		}
		model.addAttribute("stock", stock);
		return "/stock/detail";
	}

	@GetMapping("/{id}/delete")
	public String delete(@PathVariable long id, RedirectAttributes redirectAttributes) throws DAOException, ConsistencyViolationException {
		if (id == getSignInClientId()) {
			setPageMessage(redirectAttributes, "Error", "Notification.Stock.Remove.Failed", PageMessageStyle.ERROR);
			return "redirect:/stocks";
		}
		StockBean stock = repository.select(id);
		if (stock == null) {
			throw new ContentNotFoundException();
		}
		repository.delete(id, getSignInClientId());
		setPageMessage(redirectAttributes, "Success", "Notification.common.Page.SuccessfullyRemoved", PageMessageStyle.SUCCESS, "Stock");
		return "redirect:/stocks";
	}

	@Override
	public void subInit(Model model) {
		setAuthorities(model, "Stock");
	}
}
