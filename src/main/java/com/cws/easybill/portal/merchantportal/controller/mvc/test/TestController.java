package com.cws.easybill.portal.merchantportal.controller.mvc.test;

import com.cws.easybill.portal.merchantportal.common.annotation.MVCLoggable;
import com.cws.easybill.portal.merchantportal.controller.mvc.AdministratorController;
import com.cws.easybill.portal.merchantportal.model.test.Mail;
import com.cws.easybill.portal.merchantportal.model.test.UserInfo;
import com.cws.easybill.portal.merchantportal.service.EmailService;
import com.cws.easybill.portal.merchantportal.service.MailService;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
@MVCLoggable(profile = "dev")
@RequestMapping("/test")
public class TestController {

	private static final Logger applicationLogger = LogManager.getLogger("applicationLogs." + AdministratorController.class.getName());
	private static final Logger errorLogger = LogManager.getLogger("errorLogs." + AdministratorController.class.getName());



	@Autowired
	private EmailService emailService;

	@Autowired
	private MailService mailService;

	@RequestMapping(method = RequestMethod.POST,path = "/register")
	@ResponseBody
	public String register(@RequestBody UserInfo userInfo) throws Exception {
		emailService.sendEmail(userInfo);
		return "Email Sent..!";
	}

	@RequestMapping(method = RequestMethod.POST,path = "/registerNew")
	@ResponseBody
	public String registerNew(@RequestBody UserInfo userInfo, HttpServletRequest request)throws Exception{
		String contextPath = getApplicationContextPath(request);
		sendAccountConfirmEmail(userInfo,"ABc.%7lfdafadfadfaffafdfadfadfdfkl",contextPath);
		return  "Email Successfully Sent....!";
	}


	private void sendAccountConfirmEmail(UserInfo userInfo, String token, String requestContextPath) {

		Mail mail = new Mail();
		mail.setMailFrom("no_reply@thefourwatchers.com");
		mail.setMailTo(userInfo.getEmail());
		mail.setMailSubject("WATCHADS account activation");

		HashMap<String, Object> emailModel = new HashMap<>();
		emailModel.put("homeUrl", requestContextPath + "/pub/home");
		emailModel.put("termsUrl", requestContextPath + "/pub/terms");
		emailModel.put("cookieUrl", requestContextPath + "/pub/cookie_policy");
		emailModel.put("contactUsUrl", requestContextPath + "/pub/contact_us");
		emailModel.put("actionUrl", requestContextPath + "/pub/account/" + token + "/activate");
		//emailModel.put("logoURL", requestContextPath + "/static/images/logo/logo-with-text.png");

		emailModel.put("userName", userInfo.getName());
		emailModel.put("userName", userInfo.getName());
		mail.setModel(emailModel);

		boolean success = false;
		int count = 0;
		int MAX_TRIES = 3;
		while (!success && count++ < MAX_TRIES) {
			try {
				mailService.sendEmail(mail, "mail_send_user_confirm.ftl");
				success = true;
			}
			catch (Exception e) {

				errorLogger.error("Mail Sending Process Failed.");
				errorLogger.error(e);
				try {
					Thread.sleep(900000);
				}
				catch (InterruptedException e1) {
					errorLogger.error(e1);
					e1.printStackTrace();
				}
			}
		}
	}

	String getApplicationContextPath(HttpServletRequest request) {
		return request.getRequestURL().toString().replace(request.getServletPath(), "");
	}


}
