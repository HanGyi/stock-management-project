package com.cws.easybill.portal.merchantportal.controller.rest;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorRoleCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.RoleCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.PaginatedResultDto;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api/admins")
public class AdministratorApiController extends BaseRESTController {

	@Autowired
	private AdministratorRepository repository;

	@Autowired
	private AdministratorRoleRepository administratorRoleRepository;

	@PostMapping("/search/paging")
	public ResponseEntity<?> dataTableSearch(@RequestBody AdministratorCriteria criteria) throws DAOException {
		//Fetch user level from admin role table
		Set<Long> userIds = administratorRoleRepository.selectByKey2(3L);
		List<Long> userIdList = new ArrayList<Long>();
		userIdList.addAll(userIds);
		criteria.setExcludeIds(userIdList);
		//exclude users type admin
		criteria.setWithAdmins(true);
		criteria.setUserType(AuthenticatedClientBean.ClientType.USER.ordinal());
		PaginatedResultDto result = repository.selectWithCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		if (result == null) {
			results.put("iTotalDisplayRecords", 0);
			results.put("aaData", new ArrayList());
		}
		else {
			results.put("iTotalDisplayRecords", result.getCount());
			results.put("aaData", result.getData());
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@PostMapping("/search/list")
	public ResponseEntity<?> serachList(@RequestBody AdministratorCriteria criteria) throws DAOException {
		List<AdministratorBean> admins = repository.selectList(criteria);
		return new ResponseEntity<>(admins, HttpStatus.OK);
	}
}
