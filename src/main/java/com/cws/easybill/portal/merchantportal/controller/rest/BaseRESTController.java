package com.cws.easybill.portal.merchantportal.controller.rest;

import com.cws.easybill.portal.merchantportal.config.security.LoggedUserBean;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

public abstract class BaseRESTController {

	@Autowired
	protected RestTemplate restTemplate;

	@Autowired
	protected PasswordEncoder passwordEncoder;

	@Autowired
	private AdministratorRepository administratorRepository;

	@Autowired
	private ObjectMapper mapper;

	protected Long getSignInClientId() {
		LoggedUserBean loginUser = getSignInClientInfo();
		if (loginUser != null) {
			return loginUser.getId();
		}
		return null;
	}

	protected LoggedUserBean getSignInClientInfo() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && !auth.getPrincipal().equals("anonymousUser")) {
			return mapper.convertValue(auth.getPrincipal(), LoggedUserBean.class);
		}
		return null;
	}

	/**
	 * return userId of loginUser if login type is USER
	 * other will return null
	 * @return
	 * @throws DAOException
	 */
	protected Long getUserId() throws DAOException {
		AdministratorBean admin = new AdministratorBean();
		if(getSignInClientInfo().getClientType() == AuthenticatedClientBean.ClientType.USER){
			admin = administratorRepository.select(getSignInClientId());
			return admin.getUserId();
		}
		return admin.getUserId();
	}

}

