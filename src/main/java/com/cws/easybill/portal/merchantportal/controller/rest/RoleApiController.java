package com.cws.easybill.portal.merchantportal.controller.rest;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.RoleBean;
import com.cws.easybill.portal.merchantportal.model.criteria.RoleCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.PaginatedResultDto;
import com.cws.easybill.portal.merchantportal.persistence.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/roles")
public class RoleApiController extends BaseRESTController {

	@Autowired
	private RoleRepository repository;

	@PostMapping("/search/paging")
	public ResponseEntity<?> dataTableSearch(@RequestBody RoleCriteria criteria) throws DAOException {
		PaginatedResultDto result = repository.selectWithCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		if (result == null) {
			results.put("iTotalDisplayRecords", 0);
			results.put("aaData", new ArrayList());
		}
		else {
			results.put("iTotalDisplayRecords", result.getCount());
			results.put("aaData", result.getData());
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@PostMapping("/search/list")
	public ResponseEntity<?> selectAllRole(RoleCriteria criteria) throws DAOException {
		List<RoleBean> records = repository.selectList(criteria);
		return new ResponseEntity<>(records, HttpStatus.OK);
	}
}
