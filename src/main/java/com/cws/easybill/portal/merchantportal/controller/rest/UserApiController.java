package com.cws.easybill.portal.merchantportal.controller.rest;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import com.cws.easybill.portal.merchantportal.model.criteria.UserCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.PaginatedResultDto;
import com.cws.easybill.portal.merchantportal.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserApiController extends BaseRESTController {

	@Autowired
	private UserRepository repository;

	@PostMapping("/search/paging")
	public ResponseEntity<?> dataTableSearch(@RequestBody UserCriteria criteria) throws DAOException {
		PaginatedResultDto result = repository.selectWithCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		if (result == null) {
			results.put("iTotalDisplayRecords", 0);
			results.put("aaData", new ArrayList());
		}
		else {
			results.put("iTotalDisplayRecords", result.getCount());
			results.put("aaData", result.getData());
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@PostMapping("/search/list")
	public ResponseEntity<?> serachList(@RequestBody UserCriteria criteria) throws DAOException {
		List<UserBean> admins = repository.selectList(criteria);
		return new ResponseEntity<>(admins, HttpStatus.OK);
	}
}
