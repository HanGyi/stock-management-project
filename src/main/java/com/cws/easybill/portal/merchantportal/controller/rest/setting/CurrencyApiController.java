package com.cws.easybill.portal.merchantportal.controller.rest.setting;

import com.cws.easybill.portal.merchantportal.controller.rest.BaseRESTController;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.setting.CurrencyBean;
import com.cws.easybill.portal.merchantportal.model.criteria.setting.CurrencyCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.PaginatedResultDto;
import com.cws.easybill.portal.merchantportal.persistence.repository.setting.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/currencies")
public class CurrencyApiController extends BaseRESTController {

	@Autowired
	private CurrencyRepository repository;

	@PostMapping("/search/paging")
	public ResponseEntity<?> dataTableSearch(@RequestBody CurrencyCriteria criteria) throws DAOException {
		PaginatedResultDto result = repository.selectWithCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		if (result == null) {
			results.put("iTotalDisplayRecords", 0);
			results.put("aaData", new ArrayList());
		}
		else {
			results.put("iTotalDisplayRecords", result.getCount());
			results.put("aaData", result.getData());
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@PostMapping("/search/list")
	public ResponseEntity<?> selectAllCurrency(CurrencyCriteria criteria) throws DAOException {
		List<CurrencyBean> records = repository.selectList(criteria);
		return new ResponseEntity<>(records, HttpStatus.OK);
	}
}
