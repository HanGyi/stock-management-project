package com.cws.easybill.portal.merchantportal.controller.rest.stock;

import com.cws.easybill.portal.merchantportal.controller.rest.BaseRESTController;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.stock.OrderBean;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.OrderCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.PaginatedResultDto;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.OrderRepository;
import org.apache.commons.math3.dfp.DfpField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;

@RestController
@RequestMapping("/api/orders")
public class OrderApiController extends BaseRESTController {

	@Autowired
	private OrderRepository repository;

	@Autowired
	private AdministratorRepository administratorRepository;

	@PostMapping("/search/paging")
	public ResponseEntity<?> dataTableSearch(@RequestBody OrderCriteria criteria) throws DAOException {
		Long userId = getUserId();
		criteria.setUserId(criteria.getUserId() == null ?userId: criteria.getUserId());
		PaginatedResultDto result = repository.selectWithCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		if (result == null) {
			results.put("iTotalDisplayRecords", 0);
			results.put("aaData", new ArrayList());
		}
		else {
			results.put("iTotalDisplayRecords", result.getCount());
			results.put("aaData", result.getData());
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@PostMapping("/search/list")
	public ResponseEntity<?> serachList(@RequestBody OrderCriteria criteria) throws DAOException {
		//check super-user or not
		criteria.setUserId(getUserId());
		List<OrderBean> admins = repository.selectList(criteria);
		return new ResponseEntity<>(admins, HttpStatus.OK);
	}

	@PostMapping("/todayOrderStatistics")
	public ResponseEntity<?> todayOrderStatistics(@RequestBody OrderCriteria criteria) throws DAOException {
		//Get daily order amount
		Date todayStartDate = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date todayEndDate = Date.from(LocalTime.MAX.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant());
		criteria.setStartDate(todayStartDate);
		criteria.setEndDate(todayEndDate);
		Long todayOrdCount = repository.selectCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		results.put("todayOrdCount",todayOrdCount);
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@PostMapping("/monthlyOrderStatistics")
	public ResponseEntity<?> monthlyOrderStatistics(@RequestBody OrderCriteria criteria) throws DAOException {
		//current month orders
		LocalDate now = LocalDate.now();
		LocalDate firstDayOfMonth = now.withDayOfMonth(1);
		Date fDayOfMonth = Date.from(LocalTime.MIN.atDate(firstDayOfMonth).atZone(ZoneId.systemDefault()).toInstant());
		now.getMonth();
		LocalDate lastDayOfMonthDate = now.withDayOfMonth(
				now.getMonth().length(now.isLeapYear()));
		Date lDayOfMonth = Date.from(LocalTime.MAX.atDate(lastDayOfMonthDate).atZone(ZoneId.systemDefault()).toInstant());
		criteria.setStartDate(fDayOfMonth);
		criteria.setEndDate(lDayOfMonth);
		Long curMonthOrdCount = repository.selectCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		results.put("curMonthOrdCount",curMonthOrdCount);
		return new ResponseEntity<>(results, HttpStatus.OK);
	}


	@PostMapping("/profitStatistics")
	public ResponseEntity<?> profitStatistics(@RequestBody OrderCriteria orderCriteria)throws DAOException{
		String pftMessage = null;
		//current month orders
		LocalDate now = LocalDate.now();
		LocalDate firstDayOfMonth = now.withDayOfMonth(1);
		Date fDayOfMonth = Date.from(LocalTime.MIN.atDate(firstDayOfMonth).atZone(ZoneId.systemDefault()).toInstant());
		now.getMonth();
		LocalDate lastDayOfMonthDate = now.withDayOfMonth(
				now.getMonth().length(now.isLeapYear()));
		Date lDayOfMonth = Date.from(LocalTime.MAX.atDate(lastDayOfMonthDate).atZone(ZoneId.systemDefault()).toInstant());
		orderCriteria.setStartDate(fDayOfMonth);
		orderCriteria.setEndDate(lDayOfMonth);
		Long curMonthOrdCount = repository.selectCounts(orderCriteria);
		OrderBean orderStatistics = repository.orderStatistics(orderCriteria);
		BigDecimal curMonthTotalAmount = orderStatistics.getTotalAmount();
		//last month orders
		LocalDate prevMonth = now.minusMonths(1);
		LocalDate firstDayOfprevMonth = prevMonth.withDayOfMonth(1);
		LocalDate lastDayOfprevMonth = prevMonth.withDayOfMonth(
				prevMonth.getMonth().length(prevMonth.isLeapYear()));
		Date fDayOfPrevMonth = Date.from(LocalTime.MIN.atDate(firstDayOfprevMonth).atZone(ZoneId.systemDefault()).toInstant());
		Date lDayOfPrevMonth = Date.from(LocalTime.MAX.atDate(lastDayOfprevMonth).atZone(ZoneId.systemDefault()).toInstant());
		orderCriteria.setStartDate(fDayOfPrevMonth);
		orderCriteria.setEndDate(lDayOfPrevMonth);
		OrderBean orderStatisticsPrevMth = repository.orderStatistics(orderCriteria);
		BigDecimal prevMonthTotalAmount = orderStatisticsPrevMth.getTotalAmount();


		if(curMonthTotalAmount.equals(BigDecimal.ZERO)){
			pftMessage = " There is no profit for this month! ";
		}else{
			BigDecimal amount = curMonthTotalAmount;
			pftMessage = " Hi! You have profit "+amount+" for this month.";
			if(!prevMonthTotalAmount.equals(BigDecimal.ZERO)){
				amount = curMonthTotalAmount.subtract(prevMonthTotalAmount);
				BigDecimal percentage = amount.divide(prevMonthTotalAmount,6, DfpField.RoundingMode.ROUND_HALF_DOWN.ordinal()).multiply(new BigDecimal(100));
				pftMessage += percentage.abs() + "%" + (amount.compareTo(BigDecimal.ZERO) > 0? " more ":" less ")+"than last month.";
			}
		}
		Map<String, Object> results = new HashMap<>();
		results.put("pftMessage",pftMessage);
		return new ResponseEntity<>(results,HttpStatus.OK);
	}

}
