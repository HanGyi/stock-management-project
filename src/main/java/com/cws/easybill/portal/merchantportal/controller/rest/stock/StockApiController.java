package com.cws.easybill.portal.merchantportal.controller.rest.stock;

import com.cws.easybill.portal.merchantportal.controller.rest.BaseRESTController;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import com.cws.easybill.portal.merchantportal.model.bean.stock.StockBean;
import com.cws.easybill.portal.merchantportal.model.criteria.UserCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.PaginatedResultDto;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.UserRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/stocks")
public class StockApiController extends BaseRESTController {

	@Autowired
	private StockRepository repository;

	@Autowired
	private AdministratorRepository administratorRepository;
	@Autowired
	private UserRepository userRepository;

	@PostMapping("/search/paging")
	public ResponseEntity<?> dataTableSearch(@RequestBody StockCriteria criteria) throws DAOException {
		Long userId = getUserId();
		criteria.setUserId(criteria.getUserId() == null ?userId: criteria.getUserId());
		if(criteria.getWithLessValue() != null && criteria.getWithLessValue()){
			//get stock less than limit
			UserBean user = userRepository.select(criteria.getUserId() == null ?userId: criteria.getUserId());
			criteria.setQuantity(user.getStockLimit());
		}
		PaginatedResultDto result = repository.selectWithCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		if (result == null) {
			results.put("iTotalDisplayRecords", 0);
			results.put("aaData", new ArrayList());
		}
		else {
			results.put("iTotalDisplayRecords", result.getCount());
			results.put("aaData", result.getData());
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@PostMapping("/search/list")
	public ResponseEntity<?> serachList(@RequestBody StockCriteria criteria) throws DAOException {
		//check super-user or not
		Long userId = getUserId();
		criteria.setUserId(criteria.getUserId() == null ?userId: criteria.getUserId());
		List<StockBean> admins = repository.selectList(criteria);
		return new ResponseEntity<>(admins, HttpStatus.OK);
	}

	@PostMapping("/search/record")
	public ResponseEntity<?> serachSingleRecord(@RequestBody StockCriteria criteria) throws DAOException {
		StockBean stock = repository.select(criteria);
		return new ResponseEntity<>(stock, HttpStatus.OK);
	}

	@PostMapping("/lessStockStatistics")
	public ResponseEntity<?> lessStockStatistics(@RequestBody StockCriteria criteria)throws DAOException{
		UserCriteria usrCriteria = new UserCriteria();
		usrCriteria.setId(criteria.getUserId());
		UserBean user = userRepository.select(usrCriteria);

		criteria.setWithLessValue(true);
		criteria.setQuantity(user.getStockLimit());
		criteria.setUserId(user.getId());
		long lessStockCount = repository.selectCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		results.put("lessStockCount",lessStockCount);
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@PostMapping("/outOfStockStatistics")
	public ResponseEntity<?> outOfStockStatistics(@RequestBody StockCriteria criteria)throws DAOException{
		UserCriteria usrCriteria = new UserCriteria();
		usrCriteria.setId(criteria.getUserId());
		UserBean user = userRepository.select(usrCriteria);

		criteria.setWithExactValue(true);
		criteria.setQuantity(0L);
		criteria.setUserId(user.getId());
		long outOfStockCount = repository.selectCounts(criteria);
		Map<String, Object> results = new HashMap<>();
		results.put("outOfStockCount",outOfStockCount);
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
}
