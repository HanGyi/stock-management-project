package com.cws.easybill.portal.merchantportal.exception;

public class BusinessException extends Exception {
	private static final long serialVersionUID = 7816976914780713621L;

	public BusinessException() {
		super();
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(Throwable cause) {
		super(cause);
	}
}
