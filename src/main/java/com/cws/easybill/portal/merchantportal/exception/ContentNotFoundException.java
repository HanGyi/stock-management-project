package com.cws.easybill.portal.merchantportal.exception;

public class ContentNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1482043579012714991L;

	public ContentNotFoundException() {
		super();
	}

	public ContentNotFoundException(String message) {
		super(message);
	}

	public ContentNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ContentNotFoundException(Throwable cause) {
		super(cause);
	}
}
