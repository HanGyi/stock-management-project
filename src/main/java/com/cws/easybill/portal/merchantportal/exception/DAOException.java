package com.cws.easybill.portal.merchantportal.exception;

public class DAOException extends Exception {
	private static final long serialVersionUID = 2053867329965490426L;

	public DAOException() {
		super();
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}
}
