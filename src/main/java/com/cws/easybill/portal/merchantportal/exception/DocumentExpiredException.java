package com.cws.easybill.portal.merchantportal.exception;

public class DocumentExpiredException extends RuntimeException {
	private static final long serialVersionUID = 1482043579012714991L;

	public DocumentExpiredException() {
		super();
	}

	public DocumentExpiredException(String message) {
		super(message);
	}

	public DocumentExpiredException(String message, Throwable cause) {
		super(message, cause);
	}

	public DocumentExpiredException(Throwable cause) {
		super(cause);
	}
}
