package com.cws.easybill.portal.merchantportal.exception;

public class DuplicatedEntryException extends Exception {
	private static final long serialVersionUID = -1439272124681373160L;

	public DuplicatedEntryException() {
		super();
	}

	public DuplicatedEntryException(String message) {
		super(message);
	}

	public DuplicatedEntryException(String message, Throwable cause) {
		super(message, cause);
	}

	public DuplicatedEntryException(Throwable cause) {
		super(cause);
	}
}
