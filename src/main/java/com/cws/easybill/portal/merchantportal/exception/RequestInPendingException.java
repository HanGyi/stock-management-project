package com.cws.easybill.portal.merchantportal.exception;

public class RequestInPendingException extends RuntimeException {
	private static final long serialVersionUID = 1482043579012714991L;

	private String processName;

	public RequestInPendingException(String processName) {
		super();
		this.processName = processName;
	}

	public RequestInPendingException(String processName, String message) {
		super(message);
		this.processName = processName;
	}

	public RequestInPendingException(String processName, String message, Throwable cause) {
		super(message, cause);
		this.processName = processName;
	}

	public RequestInPendingException(String processName, Throwable cause) {
		super(cause);
		this.processName = processName;
	}

	public String getProcessName() {
		return processName;
	}
}
