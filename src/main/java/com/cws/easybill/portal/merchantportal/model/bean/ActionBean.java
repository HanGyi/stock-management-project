package com.cws.easybill.portal.merchantportal.model.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class ActionBean extends BaseBean {
	private static final long serialVersionUID = -4183081878456232743L;
	private String appName;
	private String page;
	private String actionName;
	private String displayName;
	private ActionType actionType;
	private String url;
	private String description;

	public enum ActionType {
		MAIN, SUB
	}
}
