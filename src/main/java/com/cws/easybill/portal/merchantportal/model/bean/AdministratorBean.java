package com.cws.easybill.portal.merchantportal.model.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString(callSuper = true)
public class AdministratorBean extends BaseBean {
	private static final long serialVersionUID = 5971336469051527577L;
	private Long contentId;
	private String name;
	private String loginId;
	@JsonIgnore
	private String password;
	private AuthenticatedClientBean.Status status;
	private List<RoleBean> roles;
	private Set<Long> roleIds;
	private StaticContentBean content;
	private Long userId;
	private Integer userType;
}
