package com.cws.easybill.portal.merchantportal.model.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class AdministratorRoleBean extends BaseBean {
	private static final long serialVersionUID = -7999816203197968625L;
	private long administratorId;
	private long roleId;

	private AdministratorBean administrator;
	private RoleBean role;

	public AdministratorRoleBean() {

	}

	public AdministratorRoleBean(AdministratorBean administrator, RoleBean role) {
		this.administrator = administrator;
		this.role = role;
	}

	public AdministratorRoleBean(long administratorId, long roleId) {
		this.administratorId = administratorId;
		this.roleId = roleId;
	}
}
