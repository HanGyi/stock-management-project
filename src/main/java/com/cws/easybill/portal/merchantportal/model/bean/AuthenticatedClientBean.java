package com.cws.easybill.portal.merchantportal.model.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
public class AuthenticatedClientBean extends BaseBean {

	private static final long serialVersionUID = 9102219677959520473L;
	private String name;
	private String loginId;
	private String password;
	private ClientType clientType;
	private Long contentId;
	private Status status;
	private List<Long> roleIds;
	private List<String> roles;

	public enum Status {
		TEMPORARY, ACTIVE, LOCKED, VERIFICATION_NEEDED
	}

	public enum ClientType {
		SUPER_USER,ADMINISTRATOR, USER
	}
}
