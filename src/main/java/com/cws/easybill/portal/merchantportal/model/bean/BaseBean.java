package com.cws.easybill.portal.merchantportal.model.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class BaseBean implements Serializable {
	private static final long serialVersionUID = -2800659136510453014L;
	private Long id;
	private Long recordRegId;
	private Long recordUpdId;
	@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private LocalDateTime recordRegDate;
	@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private LocalDateTime recordUpdDate;
}
