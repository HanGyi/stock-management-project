package com.cws.easybill.portal.merchantportal.model.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString(callSuper = true)
public class LoginHistoryBean extends BaseBean {
	private static final long serialVersionUID = 163561755991977228L;
	private long clientId;
	private AuthenticatedClientBean.ClientType clientType;
	private String ipAddress;
	private String os;
	private String clientAgent;
	@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private LocalDateTime loginDate;
}
