package com.cws.easybill.portal.merchantportal.model.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class RoleActionBean extends BaseBean {
	private static final long serialVersionUID = -3394991740341984377L;
	private long roleId;
	private long actionId;

	private RoleBean role;
	private ActionBean action;

	public RoleActionBean() {

	}

	public RoleActionBean(RoleBean role, ActionBean action) {
		this.role = role;
		this.action = action;
	}

	public RoleActionBean(long roleId, long actionId) {
		this.roleId = roleId;
		this.actionId = actionId;
	}
}
