package com.cws.easybill.portal.merchantportal.model.bean;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString(callSuper = true)
public class RoleBean extends BaseBean {
	private static final long serialVersionUID = 570651131374343251L;
	private String appName;
	private String name;
	private String description;
	private RoleType roleType;
	private Set<Long> actionIds;
	private Set<Long> adminIds;
	private Set<Long> userIds;

	public enum RoleType {
		SYSTEM("system"), BUILT_IN("built-in"), CUSTOM("custom");

		private String definition;

		RoleType(String definition) {
			this.definition = definition;
		}

		public static RoleType getEnum(String value) {
			RoleType _enum = null;
			RoleType[] var2 = values();
			int var3 = var2.length;

			for (int var4 = 0; var4 < var3; ++var4) {
				RoleType v = var2[var4];
				if (v.getDefinition().trim().equalsIgnoreCase(value)) {
					_enum = v;
					break;
				}
			}

			return _enum;
		}

		@JsonValue
		public String getDefinition() {
			return definition;
		}

		@Override
		public String toString() {
			return definition;
		}
	}
}
