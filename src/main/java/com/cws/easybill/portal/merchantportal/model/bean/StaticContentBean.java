package com.cws.easybill.portal.merchantportal.model.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class StaticContentBean extends BaseBean {
	private static final long serialVersionUID = 7934430701145504450L;
	private String fileName;
	private String filePath;
	private String fileSize;
	private FileType fileType;

	public enum FileType {
		IMAGE, TEXT, PDF, EXCEL, ZIP, UNKNOWN
	}
}
