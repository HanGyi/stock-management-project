package com.cws.easybill.portal.merchantportal.model.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class UserBean extends BaseBean{

	private static final long serialVersionUID = 5971336469051527677L;

	public enum Status {
		OFF, ON
	}
	private Long contentId;
	private String name;
	@JsonIgnore
	private String password;
	@JsonIgnore
	private String confirmPassword;

	private String email;
	private UserBean.Status status;
	private String currency;
	private Long stockLimit;
	private StaticContentBean content;

}
