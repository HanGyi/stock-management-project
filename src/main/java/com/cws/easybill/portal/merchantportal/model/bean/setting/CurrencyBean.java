package com.cws.easybill.portal.merchantportal.model.bean.setting;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString(callSuper = true)
public class CurrencyBean extends BaseBean {
	private static final long serialVersionID = 1L;

	private String code;
	private String name;
	private String symbol;
	private Integer convertUnit;
}
