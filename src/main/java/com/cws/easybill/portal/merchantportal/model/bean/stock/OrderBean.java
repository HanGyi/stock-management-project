package com.cws.easybill.portal.merchantportal.model.bean.stock;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import com.fasterxml.jackson.databind.ser.Serializers;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString(callSuper = true)
public class OrderBean extends BaseBean {
	private static final long serialVersionUID = 5973336469051527677L;
	private String stockCode;
	private Integer quantity;
	private BigDecimal amount;
	private BigDecimal totalAmount;
	private Integer availableQty;
	private Long userId;
	private UserBean user;
}
