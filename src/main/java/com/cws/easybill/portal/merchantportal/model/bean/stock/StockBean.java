package com.cws.easybill.portal.merchantportal.model.bean.stock;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString(callSuper = true)
public class StockBean extends BaseBean {
	private static final long serialVersionUID = 112233L;
	private String code;
	private String imageUrl;
	private Long quantity;
	private BigDecimal originalPrice;
	private BigDecimal sellPrice;
	private Long userId;
	private UserBean user;
}
