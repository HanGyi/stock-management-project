package com.cws.easybill.portal.merchantportal.model.criteria;

import com.cws.easybill.portal.merchantportal.model.bean.ActionBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class ActionCriteria extends CommonCriteria {
	private String appName;
	private String page;
	private String actionName;
	private String displayName;
	private ActionBean.ActionType actionType;
	private String url;

	@Override
	public Class<?> getObjectClass() {
		return ActionBean.class;
	}
}
