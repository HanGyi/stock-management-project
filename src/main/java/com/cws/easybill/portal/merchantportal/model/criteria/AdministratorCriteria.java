package com.cws.easybill.portal.merchantportal.model.criteria;

import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class AdministratorCriteria extends CommonCriteria {
	private String name;
	private String loginId;
	private AuthenticatedClientBean.Status status;
	private Long userId;
	private RoleCriteria role;
	private StaticContentCriteria staticContent;
	private Integer userType;

	private boolean withStaticContent, withRoles;
	private boolean withAdmins;

	@Override
	public Class<?> getObjectClass() {
		return AdministratorBean.class;
	}
}
