package com.cws.easybill.portal.merchantportal.model.criteria;

import com.cws.easybill.portal.merchantportal.model.bean.AdministratorRoleBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString(callSuper = true)
public class AdministratorRoleCriteria extends CommonCriteria {

	private Long administratorId;
	private Long roleId;
	private Set<Long> administratorIds;
	private Set<Long> roleIds;

	private boolean asPerAdministrator, asPerRole;
	private boolean withAdministrator, withRole;

	private AdministratorRoleCriteria administrator;
	private RoleCriteria role;

	@Override
	public Class<?> getObjectClass() {
		return AdministratorRoleBean.class;
	}
}
