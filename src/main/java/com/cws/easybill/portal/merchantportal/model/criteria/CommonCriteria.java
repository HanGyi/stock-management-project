package com.cws.easybill.portal.merchantportal.model.criteria;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public abstract class CommonCriteria {
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private static final Integer ROW_PER_PAGE = 20;

	private Long id;
	private Integer pageNumber;

	@Getter(AccessLevel.NONE)
	private long offset;

	@Getter(AccessLevel.NONE)
	private long limit;

	private String word;
	private String docType;
	private List<Long> includeIds;
	private List<Long> excludeIds;
	private String orderBy;
	private Order order;
	private boolean suppressLimit, withMetaData;

	public CommonCriteria() {
		offset = -1;
		limit = 0;
		order = Order.ASC;
	}

	public long getOffset() {
		if (pageNumber != null) {
			return pageNumber <= 1 ? 0 : (pageNumber - 1) * ROW_PER_PAGE;
		}
		return offset;
	}

	public long getLimit() {
		if (pageNumber != null) {
			return ROW_PER_PAGE;
		}
		return limit;
	}

	public abstract Class<?> getObjectClass();

	public enum Order {
		ASC("ASC"), DESC("DESC");

		private final String value;

		Order(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
}
