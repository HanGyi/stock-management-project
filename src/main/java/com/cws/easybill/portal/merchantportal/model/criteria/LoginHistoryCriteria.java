package com.cws.easybill.portal.merchantportal.model.criteria;

import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.bean.LoginHistoryBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class LoginHistoryCriteria extends CommonCriteria {
	private Long clientId;
	private AuthenticatedClientBean.ClientType clientType;

	@Override
	public Class<?> getObjectClass() {
		return LoginHistoryBean.class;
	}
}
