package com.cws.easybill.portal.merchantportal.model.criteria;

import com.cws.easybill.portal.merchantportal.model.bean.RoleActionBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString(callSuper = true)
public class RoleActionCriteria extends CommonCriteria {
	private Long roleId;
	private Long actionId;
	private Set<Long> roleIds;
	private Set<Long> actionIds;

	private boolean asPerAction;
	private boolean asPerRole;

	private RoleCriteria role;
	private  ActionCriteria action;

	private boolean withRole, withAction;

	@Override
	public Class<?> getObjectClass() {
		return RoleActionBean.class;
	}
}
