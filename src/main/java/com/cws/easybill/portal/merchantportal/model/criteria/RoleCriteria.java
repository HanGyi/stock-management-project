package com.cws.easybill.portal.merchantportal.model.criteria;

import com.cws.easybill.portal.merchantportal.model.bean.RoleBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class RoleCriteria extends CommonCriteria {
	private String appName;
	private String name;
	private RoleBean.RoleType roleType;
	private boolean withAdminIds, withActionIds, withUserIds;

	@Override
	public Class<?> getObjectClass() {
		return RoleBean.class;
	}
}
