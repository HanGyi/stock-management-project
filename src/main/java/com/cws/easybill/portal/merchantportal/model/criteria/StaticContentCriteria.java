package com.cws.easybill.portal.merchantportal.model.criteria;

import com.cws.easybill.portal.merchantportal.model.bean.StaticContentBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class StaticContentCriteria extends CommonCriteria {
	private String fileName;
	private String filePath;
	private String fileSize;
	private StaticContentBean.FileType fileType;

	@Override
	public Class<?> getObjectClass() {
		return StaticContentBean.class;
	}
}
