package com.cws.easybill.portal.merchantportal.model.criteria;

import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class UserCriteria extends CommonCriteria {

	private String name;
	private String email;
	private String currency;
	private Long stockLimit;
	private UserBean.Status status;

	private StaticContentCriteria staticContent;
	private boolean withStaticContent;

	@Override
	public Class<?> getObjectClass() {
		return UserBean.class;
	}
}
