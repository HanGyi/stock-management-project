package com.cws.easybill.portal.merchantportal.model.criteria.setting;

import com.cws.easybill.portal.merchantportal.model.bean.setting.CurrencyBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;

public class CurrencyCriteria extends CommonCriteria {

	private String code;
	private String name;
	private String symbol;
	@Override
	public Class<?> getObjectClass() {
		return CurrencyBean.class;
	}
}
