package com.cws.easybill.portal.merchantportal.model.criteria.stock;

import com.cws.easybill.portal.merchantportal.common.enums.ExportType;
import com.cws.easybill.portal.merchantportal.model.bean.stock.OrderBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@ToString(callSuper = true)
public class OrderCriteria extends CommonCriteria {

	private String stockCode;
	private Date startDate;
	private Date endDate;
	private Long userId;
	private ExportType reportType;

	@Override
	public Class<?> getObjectClass() {
		return OrderBean.class;
	}
}
