package com.cws.easybill.portal.merchantportal.model.criteria.stock;

import com.cws.easybill.portal.merchantportal.common.enums.ExportType;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.bean.stock.StockBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.UserCriteria;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString(callSuper = true)
public class StockCriteria extends CommonCriteria {
	private String code;
	private Long quantity;
	private Long userId;
	private ExportType reportType;
	private UserCriteria user;
	private Boolean withUser;
	private Boolean withLessValue;
	private Boolean withExactValue;

	@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private LocalDateTime startDate;
	@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private LocalDateTime endDate;

	@Override
	public Class<?> getObjectClass() {
		return StockBean.class;
	}
}
