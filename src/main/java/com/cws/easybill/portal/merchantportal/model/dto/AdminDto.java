package com.cws.easybill.portal.merchantportal.model.dto;

import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AdminDto extends AdministratorBean {
	private static final long serialVersionUID = 8727497535913250812L;
	private String confirmPassword;
}
