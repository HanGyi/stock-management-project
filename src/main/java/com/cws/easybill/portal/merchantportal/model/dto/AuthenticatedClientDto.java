package com.cws.easybill.portal.merchantportal.model.dto;

import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serial;
import java.util.List;

@Getter
@Setter
@ToString
public class AuthenticatedClientDto extends BaseBean {
	@Serial
	private static final long serialVersionUID = -4997836895728916010L;
	private String name;
	private String loginId;
	private String password;
	private AuthenticatedClientBean.ClientType clientType;
	private Long contentId;
	private AuthenticatedClientBean.Status status;
	private List<Long> roleIds;
	private List<String> roles;

}
