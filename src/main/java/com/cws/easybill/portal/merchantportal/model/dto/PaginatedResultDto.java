package com.cws.easybill.portal.merchantportal.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class PaginatedResultDto {
	private long count;
	private List<?> data;
}
