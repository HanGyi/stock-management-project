package com.cws.easybill.portal.merchantportal.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@ToString
public class RequestData {
	private String from;
	private String to;
	private Map<String, String> params;
	private String template;

	private RequestData(String from, String to, Map<String, String> params, String template) {
		this.from = from;
		this.to = to;
		this.params = params;
		this.template = template;

	}

	public static class RequestDataBuilder {
		private String from;
		private String to;
		private Map<String, String> params;

		private String template;

		public RequestDataBuilder(String to, String template) {
			this.to = to;
			this.template = template;

		}

		public RequestDataBuilder param(Map<String, String> params) {
			this.params = params;
			return this;
		}

		public RequestDataBuilder setFrom(String from) {
			this.from = from;
			return this;
		}

		public RequestData build() {
			return new RequestData(from, to, params, template);
		}
	}

}
