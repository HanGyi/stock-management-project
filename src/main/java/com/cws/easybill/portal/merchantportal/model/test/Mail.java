package com.cws.easybill.portal.merchantportal.model.test;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;

@Getter
@Setter
@ToString
public class Mail {

	private String mailFrom;
	private String mailTo;
	private String mailSubject;
	private HashMap<String,Object> model;
	private String contentType;
	private String mailCc;
	private String mailBcc;
}
