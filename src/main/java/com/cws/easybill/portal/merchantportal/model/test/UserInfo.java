package com.cws.easybill.portal.merchantportal.model.test;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class UserInfo {
	private String username;
	private String name;
	private String email;
}
