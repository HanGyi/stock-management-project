package com.cws.easybill.portal.merchantportal.persistence.epmpMappers;

public interface EpmpBaseMapper {
	void disableAllConstraints();

	void enableAllConstraints();
}
