package com.cws.easybill.portal.merchantportal.persistence.mapper;

import com.cws.easybill.portal.merchantportal.model.bean.ActionBean;
import com.cws.easybill.portal.merchantportal.model.criteria.ActionCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.SelectableMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActionMapper extends SelectableMapper<ActionBean, ActionCriteria> {
	List<String> selectAvailableActionsForAuthenticatedUser(@Param("pageName") String pageName, @Param("appName") String appName, @Param("roleIds") List<Long> roleIds);
}
