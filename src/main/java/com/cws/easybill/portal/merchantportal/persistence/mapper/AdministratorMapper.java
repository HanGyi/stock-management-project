package com.cws.easybill.portal.merchantportal.persistence.mapper;

import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.CommonGenericMapper;
import org.apache.ibatis.annotations.Param;

public interface AdministratorMapper extends CommonGenericMapper<AdministratorBean, AdministratorCriteria> {
	AuthenticatedClientBean selectAuthenticatedClient(@Param("loginId") String loginId);
}
