package com.cws.easybill.portal.merchantportal.persistence.mapper;

import com.cws.easybill.portal.merchantportal.model.bean.AdministratorRoleBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorRoleCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.XGenericMapper;

public interface AdministratorRoleMapper extends XGenericMapper<AdministratorRoleBean, AdministratorRoleCriteria> {}
