package com.cws.easybill.portal.merchantportal.persistence.mapper;

public interface BaseMapper {
	void disableAllConstraints();

	void enableAllConstraints();
}
