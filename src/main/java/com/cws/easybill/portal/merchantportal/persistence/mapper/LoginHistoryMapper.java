package com.cws.easybill.portal.merchantportal.persistence.mapper;

import com.cws.easybill.portal.merchantportal.model.bean.LoginHistoryBean;
import com.cws.easybill.portal.merchantportal.model.criteria.LoginHistoryCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.CommonGenericMapper;

public interface LoginHistoryMapper extends CommonGenericMapper<LoginHistoryBean, LoginHistoryCriteria> {}
