package com.cws.easybill.portal.merchantportal.persistence.mapper;

import com.cws.easybill.portal.merchantportal.model.bean.RoleActionBean;
import com.cws.easybill.portal.merchantportal.model.criteria.RoleActionCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.XGenericMapper;

public interface RoleActionMapper extends XGenericMapper<RoleActionBean, RoleActionCriteria> {}
