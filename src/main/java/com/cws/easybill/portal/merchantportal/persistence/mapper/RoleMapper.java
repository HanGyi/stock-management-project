package com.cws.easybill.portal.merchantportal.persistence.mapper;

import com.cws.easybill.portal.merchantportal.model.bean.RoleBean;
import com.cws.easybill.portal.merchantportal.model.criteria.RoleCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.CommonGenericMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper extends CommonGenericMapper<RoleBean, RoleCriteria> {
	List<String> selectRolesByActionUrl(@Param("appName") String appName, @Param("actionUrl") String actionUrl);
}
