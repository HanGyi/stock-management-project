package com.cws.easybill.portal.merchantportal.persistence.mapper;

import com.cws.easybill.portal.merchantportal.model.bean.StaticContentBean;
import com.cws.easybill.portal.merchantportal.model.criteria.StaticContentCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.CommonGenericMapper;

public interface StaticContentMapper extends CommonGenericMapper<StaticContentBean, StaticContentCriteria> {}
