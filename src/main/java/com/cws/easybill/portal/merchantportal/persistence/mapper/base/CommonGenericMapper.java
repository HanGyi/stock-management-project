package com.cws.easybill.portal.merchantportal.persistence.mapper.base;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;

public interface CommonGenericMapper<T extends BaseBean, C extends CommonCriteria> extends SelectableMapper<T, C>, InsertableMapper<T>, UpdateableMapper<T, C>, RemoveableMapper<T, C> {}
