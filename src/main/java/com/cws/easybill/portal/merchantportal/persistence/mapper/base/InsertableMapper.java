package com.cws.easybill.portal.merchantportal.persistence.mapper.base;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InsertableMapper<T extends BaseBean> {
	long insert(T record);

	long insertWithId(T record);

	long insertList(@Param("records") List<T> records);

	long insertListWithIds(@Param("records") List<T> records);
}
