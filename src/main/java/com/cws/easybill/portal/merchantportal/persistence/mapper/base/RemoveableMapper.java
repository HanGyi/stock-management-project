package com.cws.easybill.portal.merchantportal.persistence.mapper.base;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import org.apache.ibatis.annotations.Param;

public interface RemoveableMapper<T extends BaseBean, C extends CommonCriteria> {

	long deleteByPrimaryKey(@Param("primaryKey") long primaryKey);

	long deleteByCriteria(@Param("criteria") C criteria);
}
