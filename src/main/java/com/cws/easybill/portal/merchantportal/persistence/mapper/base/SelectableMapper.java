package com.cws.easybill.portal.merchantportal.persistence.mapper.base;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SelectableMapper<T extends BaseBean, C extends CommonCriteria> {

	T selectByPrimaryKey(@Param("primaryKey") long primaryKey);

	T selectSingleRecord(@Param("criteria") C criteria);

	List<T> selectMultiRecords(@Param("criteria") C criteria);

	long selectCounts(@Param("criteria") C criteria);
}
