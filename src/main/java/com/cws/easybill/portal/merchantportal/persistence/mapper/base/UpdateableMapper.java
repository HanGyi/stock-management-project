package com.cws.easybill.portal.merchantportal.persistence.mapper.base;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;

public interface UpdateableMapper<T extends BaseBean, C extends CommonCriteria> {
	long update(T record);

	long updateWithCriteria(@Param("criteria") C criteria, @Param("updateItems") HashMap<String, Object> updateItems);
}
