package com.cws.easybill.portal.merchantportal.persistence.mapper.base;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface XGenericMapper<T extends BaseBean, C extends CommonCriteria> extends InsertableMapper<T> {
	void insertWithRelatedKeys(@Param("key1") long key1, @Param("key2") long key2, @Param("recordRegId") long recordRegId);

	long deleteByKeys(@Param("key1") long key1, @Param("key2") long key2);

	long deleteByCriteria(@Param("criteria") C criteria);

	Set<Long> selectRelatedKeysByKey1(@Param("key1") long key1);

	Set<Long> selectRelatedKeysByKey2(@Param("key2") long key2);

	T selectByKeys(@Param("key1") long key1, @Param("key2") long key2);

	List<T> selectMultiRecords(@Param("criteria") C criteria);

	long selectCounts(@Param("criteria") C criteria);
}
