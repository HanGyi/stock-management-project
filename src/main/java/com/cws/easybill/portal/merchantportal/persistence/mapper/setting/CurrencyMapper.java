package com.cws.easybill.portal.merchantportal.persistence.mapper.setting;

import com.cws.easybill.portal.merchantportal.model.bean.setting.CurrencyBean;
import com.cws.easybill.portal.merchantportal.model.criteria.setting.CurrencyCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.CommonGenericMapper;

public interface CurrencyMapper extends CommonGenericMapper<CurrencyBean, CurrencyCriteria> {}
