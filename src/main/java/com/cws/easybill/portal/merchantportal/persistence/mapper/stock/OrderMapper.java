package com.cws.easybill.portal.merchantportal.persistence.mapper.stock;

import com.cws.easybill.portal.merchantportal.model.bean.stock.OrderBean;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.OrderCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.CommonGenericMapper;
import org.apache.ibatis.annotations.Param;

public interface OrderMapper extends CommonGenericMapper<OrderBean, OrderCriteria> {
	OrderBean orderStatistics(@Param("criteria") OrderCriteria criteria);
}
