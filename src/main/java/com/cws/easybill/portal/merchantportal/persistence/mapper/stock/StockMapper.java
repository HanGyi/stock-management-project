package com.cws.easybill.portal.merchantportal.persistence.mapper.stock;

import com.cws.easybill.portal.merchantportal.model.bean.stock.StockBean;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.CommonGenericMapper;

public interface StockMapper extends CommonGenericMapper<StockBean, StockCriteria> {}
