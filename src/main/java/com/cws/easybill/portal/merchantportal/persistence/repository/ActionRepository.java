package com.cws.easybill.portal.merchantportal.persistence.repository;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.ActionBean;
import com.cws.easybill.portal.merchantportal.model.criteria.ActionCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.ActionMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.SelectableRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.SelectableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ActionRepository extends SelectableRepositoryImpl<ActionBean, ActionCriteria> implements SelectableRepository<ActionBean, ActionCriteria> {

	private static final Logger repositoryLogger = LogManager.getLogger("repositoryLogs." + ActionRepository.class.getName());

	private ActionMapper mapper;

	@Autowired
	public ActionRepository(ActionMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}

	public List<String> selectAvailableActionsForAuthenticatedUser(String pageName, String appName, List<Long> roleIds) throws DAOException {
		repositoryLogger.debug("[START] : >>> --- Fetching all 'ActionNames' for Authenticated User with pageName = '{}' ---", pageName);
		List<String> actionNames;
		try {
			actionNames = mapper.selectAvailableActionsForAuthenticatedUser(pageName, appName, roleIds);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while fetching all 'ActionNames' for Authenticated User xxx";
			throw new DAOException(errorMsg, e);
		}
		repositoryLogger.debug("[FINISH] : <<< --- Fetching all 'ActionNames' for Authenticated User with pageName = '{}' ---", pageName);
		return actionNames;
	}
}
