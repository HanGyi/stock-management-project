package com.cws.easybill.portal.merchantportal.persistence.repository;

import com.cws.easybill.portal.merchantportal.exception.BusinessException;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorRoleBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.AdministratorMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.CommonGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.CommonGenericRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
public class AdministratorRepository extends CommonGenericRepositoryImpl<AdministratorBean, AdministratorCriteria> implements CommonGenericRepository<AdministratorBean, AdministratorCriteria> {

	private static final Logger repositoryLogger = LogManager.getLogger("repositoryLogs." + AdministratorRepository.class.getName());

	private AdministratorMapper mapper;

	@Autowired
	private AdministratorRoleRepository adminRoleRepository;

	@Autowired
	public AdministratorRepository(AdministratorMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}

	public AuthenticatedClientBean selectAuthenticatedClient(String loginId) throws DAOException {
		repositoryLogger.debug("[START] : >>> --- Fetching Authenticated 'Client' informations with LoginId # " + loginId + " ---");
		AuthenticatedClientBean client;
		try {
			client = mapper.selectAuthenticatedClient(loginId);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while fetching Authenticated 'Client' informations with LoginId ==> " + loginId + " xxx";
			throw new DAOException(errorMsg, e);
		}
		repositoryLogger.debug("[FINISH] : <<< --- Fetching Authenticated 'Client' informations with LoginId # " + loginId + " ---");
		return client;
	}

	@Transactional
	public long createNewAdminWithRoles(AdministratorBean admin, Set<Long> roleIds, long recordRegId) throws BusinessException, DuplicatedEntryException {
		String objectName = "Administrator";
		repositoryLogger.debug("[START] : >>> --- This transaction was initiated by User ID # " + recordRegId + " ---");
		repositoryLogger.debug("[START] : >>> --- Transaction start for registering new " + objectName + " informations. ---");

		LocalDateTime now = LocalDateTime.now();
		admin.setRecordRegDate(now);
		admin.setRecordUpdDate(now);
		admin.setRecordRegId(recordRegId);
		admin.setRecordUpdId(recordRegId);
		admin.setUserType(1);

		long lastInsertedId;
		try {
			lastInsertedId = insert(admin,recordRegId);
			if (roleIds != null && !roleIds.isEmpty()) {
				List<AdministratorRoleBean> adminRoles = new ArrayList<>();
				roleIds.forEach(roleId -> adminRoles.add(new AdministratorRoleBean(lastInsertedId, roleId)));
				adminRoleRepository.insert(adminRoles, recordRegId);
			}
			else {
				// register with 'Administrator' Role
				adminRoleRepository.insert(new AdministratorRoleBean(lastInsertedId, 2L), recordRegId);
			}
		}
		catch (DAOException e) {
			throw new BusinessException(e.getMessage(), e);
		}
		repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for registering new " + objectName + " informations. ---");
		return lastInsertedId;
	}

	public long updateAdminWithRoles(AdministratorBean admin, Set<Long> roleIds, long recordUpdId) throws BusinessException, DuplicatedEntryException {
		String objectName = "Administrator";
		repositoryLogger.debug("[START] : >>> --- This transaction was initiated by User ID # " + recordUpdId + " ---");
		repositoryLogger.debug("[START] : >>> --- Transaction start for registering new " + objectName + " informations. ---");

		admin.setRecordUpdId(recordUpdId);
		admin.setRecordUpdDate(LocalDateTime.now());

		long effectedRow;
		try {
			effectedRow = mapper.update(admin);
			if (!CollectionUtils.isEmpty(roleIds)) {
				adminRoleRepository.merge(admin.getId(), roleIds, recordUpdId);
			}
		}
		catch (DAOException e) {
			throw new BusinessException(e.getMessage(), e);
		}
		repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for updating " + objectName + " informations. ---");
		return effectedRow;
	}
}
