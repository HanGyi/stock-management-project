package com.cws.easybill.portal.merchantportal.persistence.repository;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorRoleBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorRoleCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.AdministratorRoleMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.XGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.XGenericRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.cws.easybill.portal.merchantportal.util.LoggerConstants.DUPLICATE_KEY_INSERT_FAILED_MSG;

@Repository
public class AdministratorRoleRepository extends XGenericRepositoryImpl<AdministratorRoleBean, AdministratorRoleCriteria> implements XGenericRepository<AdministratorRoleBean, AdministratorRoleCriteria> {

	private static final Logger repositoryLogger = LogManager.getLogger("repositoryLogs." + AdministratorRoleRepository.class.getName());

	private AdministratorRoleMapper mapper;

	@Autowired
	public AdministratorRoleRepository(AdministratorRoleMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}

	public void merge(long administratorId, Set<Long> roleIds, long recordUpdId) throws DAOException, DuplicatedEntryException {
		repositoryLogger.debug("[START] : >>> --- Merging  'AdministratorRole' informations for administratorId # =" + administratorId + " with related roleIds =" + roleIds + " ---");
		Set<Long> insertIds = new HashSet<>();
		Set<Long> removeIds = new HashSet<>();
		repositoryLogger.debug("[START] : $1 --- Fetching old related roleIds for administratorId # " + administratorId + " ---");
		Set<Long> oldRelatedRoles = selectByKey1(administratorId);
		repositoryLogger.debug("[FINISH] : $1 --- Fetching old related roleIds for administratorId # " + administratorId + " ==> " + oldRelatedRoles + " ---");
		if (!CollectionUtils.isEmpty(oldRelatedRoles)) {
			for (long roleId : roleIds) {
				if (!oldRelatedRoles.contains(roleId)) {
					insertIds.add(roleId);
				}
			}

			for (long roleId : oldRelatedRoles) {
				if (!roleIds.contains(roleId)) {
					removeIds.add(roleId);
				}
			}
		}
		else {
			insertIds = roleIds;
		}
		if (removeIds.size() > 0) {
			repositoryLogger.debug("[START] : $2 --- Removing  related roleIds " + removeIds + " for administratorId # " + administratorId + " these have been no longer used  ---");
			AdministratorRoleCriteria criteria = new AdministratorRoleCriteria();
			criteria.setAdministratorId(administratorId);
			criteria.setRoleIds(removeIds);
			mapper.deleteByCriteria(criteria);
			repositoryLogger.debug("[FINISH] : $2 --- Removing  related roleIds " + removeIds + " for administratorId # " + administratorId + " these have been no longer used  ---");
		}

		if (insertIds.size() > 0) {
			repositoryLogger.debug("[START] : $3 --- Inserting newly selected roleIds " + insertIds + " for administratorId # " + administratorId + " ---");
			List<AdministratorRoleBean> administratorRoles = new ArrayList<>();
			for (Long roleId : insertIds) {
				AdministratorRoleBean administratorRole = new AdministratorRoleBean(administratorId, roleId);
				administratorRoles.add(administratorRole);
			}
			LocalDateTime now = LocalDateTime.now();
			for (AdministratorRoleBean administratorRole : administratorRoles) {
				administratorRole.setRecordRegDate(now);
				administratorRole.setRecordUpdDate(now);
				administratorRole.setRecordRegId(recordUpdId);
				administratorRole.setRecordUpdId(recordUpdId);
			}
			try {
				mapper.insertList(administratorRoles);
			}
			catch (DuplicateKeyException e) {
				String errorMsg = "xxx " + DUPLICATE_KEY_INSERT_FAILED_MSG + " xxx";
				throw new DuplicatedEntryException(errorMsg, e);
			}
		}

		repositoryLogger.debug("[FINISH] : <<< --- Merging 'AdministratorRole' informations for administratorId # =" + administratorId + " with related roleIds =" + roleIds.toString() + " ---");
	}

	public void merge(Set<Long> administratorIds, long roleId, long recordUpdId) throws DAOException, DuplicatedEntryException {
		repositoryLogger.debug("[START] : >>> --- Merging  'AdministratorRole' informations for roleId # =" + roleId + " with related administratorIds =" + administratorIds + " ---");
		Set<Long> insertIds = new HashSet<>();
		Set<Long> removeIds = new HashSet<>();
		repositoryLogger.debug("[START] : $1 --- Fetching old related administratorIds for roleId # " + roleId + " ---");
		Set<Long> oldRelatedAdministratorIds = selectByKey2(roleId);
		repositoryLogger.debug("[FINISH] : $1 --- Fetching old related administratorIds for roleId # " + roleId + " ==> " + oldRelatedAdministratorIds + " ---");
		if (!CollectionUtils.isEmpty(oldRelatedAdministratorIds)) {
			for (long administratorId : administratorIds) {
				if (!oldRelatedAdministratorIds.contains(administratorId)) {
					insertIds.add(administratorId);
				}
			}

			for (long administratorId : oldRelatedAdministratorIds) {
				if (!administratorIds.contains(administratorId)) {
					removeIds.add(administratorId);
				}
			}
		}
		else {
			insertIds = administratorIds;
		}
		if (removeIds.size() > 0) {
			repositoryLogger.debug("[START] : $2 --- Removing  related administratorIds " + removeIds + " for roleId # " + roleId + " these have been no longer used  ---");
			AdministratorRoleCriteria criteria = new AdministratorRoleCriteria();
			criteria.setRoleId(roleId);
			criteria.setAdministratorIds(removeIds);
			mapper.deleteByCriteria(criteria);
			repositoryLogger.debug("[FINISH] : $2 --- Removing  related administratorIds " + removeIds + " for roleId # " + roleId + " these have been no longer used  ---");
		}

		if (insertIds.size() > 0) {
			repositoryLogger.debug("[START] : $3 --- Inserting newly selected administratorIds " + insertIds + " for roleId # " + roleId + " ---");
			List<AdministratorRoleBean> administratorRoles = new ArrayList<>();
			for (Long administratorId : insertIds) {
				AdministratorRoleBean administratorRole = new AdministratorRoleBean(administratorId, roleId);
				administratorRoles.add(administratorRole);
			}
			LocalDateTime now = LocalDateTime.now();
			for (AdministratorRoleBean administratorRole : administratorRoles) {
				administratorRole.setRecordRegDate(now);
				administratorRole.setRecordUpdDate(now);
				administratorRole.setRecordRegId(recordUpdId);
				administratorRole.setRecordUpdId(recordUpdId);
			}
			try {
				mapper.insertList(administratorRoles);
			}
			catch (DuplicateKeyException e) {
				String errorMsg = "xxx " + DUPLICATE_KEY_INSERT_FAILED_MSG + " xxx";
				throw new DuplicatedEntryException(errorMsg, e);
			}
			repositoryLogger.debug("[FINISH] : $3 --- Inserting newly selected administratorIds " + insertIds + " for roleId # " + roleId + " ---");
		}

		repositoryLogger.debug("[FINISH] : <<< --- Merging 'AdministratorRole' informations for roleId # =" + roleId + " with related administratorIds =" + administratorIds.toString() + " ---");
	}
}
