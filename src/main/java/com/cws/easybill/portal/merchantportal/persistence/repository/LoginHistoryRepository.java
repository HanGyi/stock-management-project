package com.cws.easybill.portal.merchantportal.persistence.repository;

import com.cws.easybill.portal.merchantportal.model.bean.LoginHistoryBean;
import com.cws.easybill.portal.merchantportal.model.criteria.LoginHistoryCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.AdministratorMapper;
import com.cws.easybill.portal.merchantportal.persistence.mapper.LoginHistoryMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.CommonGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.CommonGenericRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoginHistoryRepository extends CommonGenericRepositoryImpl<LoginHistoryBean, LoginHistoryCriteria> implements CommonGenericRepository<LoginHistoryBean, LoginHistoryCriteria> {

	private LoginHistoryMapper mapper;

	@Autowired
	public LoginHistoryRepository(LoginHistoryMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}
}
