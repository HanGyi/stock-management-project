package com.cws.easybill.portal.merchantportal.persistence.repository;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.RoleActionBean;
import com.cws.easybill.portal.merchantportal.model.criteria.RoleActionCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.RoleActionMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.XGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.XGenericRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.cws.easybill.portal.merchantportal.util.LoggerConstants.DUPLICATE_KEY_INSERT_FAILED_MSG;

@Repository
public class RoleActionRepository extends XGenericRepositoryImpl<RoleActionBean, RoleActionCriteria> implements XGenericRepository<RoleActionBean, RoleActionCriteria> {

	private static final Logger repositoryLogger = LogManager.getLogger("repositoryLogs." + RoleActionRepository.class.getName());

	private RoleActionMapper mapper;

	@Autowired
	public RoleActionRepository(RoleActionMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}

	public void merge(long roleId, Set<Long> actionIds, long recordUpdId) throws DAOException, DuplicatedEntryException {
		repositoryLogger.debug("[START] : >>> --- Merging  'RoleAction' informations for roleId # =" + roleId + " with related actionIds =" + actionIds + " ---");
		Set<Long> insertIds = new HashSet<>();
		Set<Long> removeIds = new HashSet<>();
		repositoryLogger.debug("[START] : $1 --- Fetching old related actionIds for roleId # " + roleId + " ---");
		Set<Long> oldRelatedActions = selectByKey1(roleId);
		repositoryLogger.debug("[FINISH] : $1 --- Fetching old related actionIds for roleId # " + roleId + " ==> " + oldRelatedActions + " ---");
		if (!CollectionUtils.isEmpty(oldRelatedActions)) {
			for (long actionId : actionIds) {
				if (!oldRelatedActions.contains(actionId)) {
					insertIds.add(actionId);
				}
			}

			for (long actionId : oldRelatedActions) {
				if (!actionIds.contains(actionId)) {
					removeIds.add(actionId);
				}
			}
		}
		else {
			insertIds = actionIds;
		}
		if (removeIds.size() > 0) {
			repositoryLogger.debug("[START] : $2 --- Removing  related actionIds " + removeIds + " for roleId # " + roleId + " these have been no longer used  ---");
			RoleActionCriteria criteria = new RoleActionCriteria();
			criteria.setRoleId(roleId);
			criteria.setActionIds(removeIds);
			mapper.deleteByCriteria(criteria);
			repositoryLogger.debug("[FINISH] : $2 --- Removing  related actionIds " + removeIds + " for roleId # " + roleId + " these have been no longer used  ---");
		}

		if (insertIds.size() > 0) {
			repositoryLogger.debug("[START] : $3 --- Inserting newly selected actionIds " + insertIds + " for roleId # " + roleId + " ---");
			List<RoleActionBean> roleActions = new ArrayList<>();
			for (Long actionId : insertIds) {
				RoleActionBean roleAction = new RoleActionBean(roleId, actionId);
				roleActions.add(roleAction);
			}
			LocalDateTime now = LocalDateTime.now();
			for (RoleActionBean roleAction : roleActions) {
				roleAction.setRecordRegDate(now);
				roleAction.setRecordUpdDate(now);
				roleAction.setRecordRegId(recordUpdId);
				roleAction.setRecordUpdId(recordUpdId);
			}
			try {
				mapper.insertList(roleActions);
			}
			catch (DuplicateKeyException e) {
				String errorMsg = "xxx " + DUPLICATE_KEY_INSERT_FAILED_MSG + " xxx";
				throw new DuplicatedEntryException(errorMsg, e);
			}
		}

		repositoryLogger.debug("[FINISH] : <<< --- Merging 'RoleAction' informations for roleId # =" + roleId + " with related actionIds =" + actionIds.toString() + " ---");
	}

	public void merge(Set<Long> roleIds, long actionId, long recordUpdId) throws DAOException, DuplicatedEntryException {
		repositoryLogger.debug("[START] : >>> --- Merging  'RoleAction' informations for actionId # =" + actionId + " with related roleIds =" + roleIds + " ---");
		Set<Long> insertIds = new HashSet<>();
		Set<Long> removeIds = new HashSet<>();
		repositoryLogger.debug("[START] : $1 --- Fetching old related roleIds for actionId # " + actionId + " ---");
		Set<Long> oldRelatedRoleIds = selectByKey2(actionId);
		repositoryLogger.debug("[FINISH] : $1 --- Fetching old related roleIds for actionId # " + actionId + " ==> " + oldRelatedRoleIds + " ---");
		if (!CollectionUtils.isEmpty(oldRelatedRoleIds)) {
			for (long roleId : roleIds) {
				if (!oldRelatedRoleIds.contains(roleId)) {
					insertIds.add(roleId);
				}
			}

			for (long roleId : oldRelatedRoleIds) {
				if (!roleIds.contains(roleId)) {
					removeIds.add(roleId);
				}
			}
		}
		else {
			insertIds = roleIds;
		}
		if (removeIds.size() > 0) {
			repositoryLogger.debug("[START] : $2 --- Removing  related roleIds " + removeIds + " for actionId # " + actionId + " these have been no longer used  ---");
			RoleActionCriteria criteria = new RoleActionCriteria();
			criteria.setActionId(actionId);
			criteria.setRoleIds(removeIds);
			mapper.deleteByCriteria(criteria);
			repositoryLogger.debug("[FINISH] : $2 --- Removing  related roleIds " + removeIds + " for actionId # " + actionId + " these have been no longer used  ---");
		}

		if (insertIds.size() > 0) {
			repositoryLogger.debug("[START] : $3 --- Inserting newly selected roleIds " + insertIds + " for actionId # " + actionId + " ---");
			List<RoleActionBean> roleActions = new ArrayList<>();
			for (Long roleId : insertIds) {
				RoleActionBean roleAction = new RoleActionBean(roleId, actionId);
				roleActions.add(roleAction);
			}
			LocalDateTime now = LocalDateTime.now();
			for (RoleActionBean roleAction : roleActions) {
				roleAction.setRecordRegDate(now);
				roleAction.setRecordUpdDate(now);
				roleAction.setRecordRegId(recordUpdId);
				roleAction.setRecordUpdId(recordUpdId);
			}
			try {
				mapper.insertList(roleActions);
			}
			catch (DuplicateKeyException e) {
				String errorMsg = "xxx " + DUPLICATE_KEY_INSERT_FAILED_MSG + " xxx";
				throw new DuplicatedEntryException(errorMsg, e);
			}
			repositoryLogger.debug("[FINISH] : $3 --- Inserting newly selected roleIds " + insertIds + " for actionId # " + actionId + " ---");
		}

		repositoryLogger.debug("[FINISH] : <<< --- Merging 'RoleAction' informations for actionId # =" + actionId + " with related roleIds =" + roleIds.toString() + " ---");
	}
}
