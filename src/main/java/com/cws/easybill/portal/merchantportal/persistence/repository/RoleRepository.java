package com.cws.easybill.portal.merchantportal.persistence.repository;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.RoleBean;
import com.cws.easybill.portal.merchantportal.model.criteria.RoleCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.RoleMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.CommonGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.CommonGenericRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepository extends CommonGenericRepositoryImpl<RoleBean, RoleCriteria> implements CommonGenericRepository<RoleBean, RoleCriteria> {

	private static final Logger repositoryLogger = LogManager.getLogger("repositoryLogs." + RoleRepository.class.getName());

	private RoleMapper mapper;

	@Autowired
	public RoleRepository(RoleMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}

	public List<String> selectRolesByActionUrl(String appName, String actionUrl) throws DAOException {
		repositoryLogger.debug("[START] : >>> --- Fetching all 'RoleNames' by appName = '{}' and actionUrl = '{}' ---", appName, actionUrl);
		List<String> roleNames;
		try {
			roleNames = mapper.selectRolesByActionUrl(appName, actionUrl);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while fetching all 'RoleNames' by appName = " + appName + " and actionUrl = '" + actionUrl + "' ---";
			throw new DAOException(errorMsg, e);
		}
		repositoryLogger.debug("[FINISH] : <<< --- Fetching all 'RoleNames' by appName = '{}' and actionUrl = '{}' ---", appName, actionUrl);
		return roleNames;
	}
}
