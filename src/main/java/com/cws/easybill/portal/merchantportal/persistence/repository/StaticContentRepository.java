package com.cws.easybill.portal.merchantportal.persistence.repository;

import com.cws.easybill.portal.merchantportal.model.bean.StaticContentBean;
import com.cws.easybill.portal.merchantportal.model.criteria.StaticContentCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.StaticContentMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.CommonGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.CommonGenericRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StaticContentRepository extends CommonGenericRepositoryImpl<StaticContentBean, StaticContentCriteria> implements CommonGenericRepository<StaticContentBean, StaticContentCriteria> {

	@Autowired
	public StaticContentRepository(StaticContentMapper mapper) {
		super(mapper);
	}
}
