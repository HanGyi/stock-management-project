package com.cws.easybill.portal.merchantportal.persistence.repository;

import com.cws.easybill.portal.merchantportal.exception.BusinessException;
import com.cws.easybill.portal.merchantportal.exception.ConsistencyViolationException;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorRoleBean;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.UserCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.UserMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.CommonGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.CommonGenericRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Repository
public class UserRepository extends CommonGenericRepositoryImpl<UserBean, UserCriteria> implements CommonGenericRepository<UserBean, UserCriteria> {

	private static final Logger repositoryLogger = LogManager.getLogger("repositoryLogs." + UserRepository.class.getName());

	private UserMapper mapper;

	@Autowired
	private AdministratorRoleRepository adminRoleRepository;

	@Autowired
	private AdministratorRepository adminRepository;
	@Autowired
	public UserRepository(UserMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}

	@Transactional
	public long createNewUserWithRoles(UserBean user, long recordRegId) throws BusinessException, DuplicatedEntryException {
		String objectName = "User";
		repositoryLogger.debug("[START] : >>> --- This transaction was initiated by User ID # " + recordRegId + " ---");
		repositoryLogger.debug("[START] : >>> --- Transaction start for registering new " + objectName + " informations. ---");

		LocalDateTime now = LocalDateTime.now();
		user.setRecordRegDate(now);
		user.setRecordUpdDate(now);
		user.setRecordRegId(recordRegId);
		user.setRecordUpdId(recordRegId);

		long lastInsertedId;
		try {
			lastInsertedId = insert(user,recordRegId);
			//register in Administrator
			AdministratorBean admin = new AdministratorBean();
			admin.setName(user.getName());
			admin.setLoginId(user.getName());
			admin.setPassword(user.getPassword());
			admin.setStatus(AuthenticatedClientBean.Status.ACTIVE);
			admin.setUserId(lastInsertedId);
			//set default for user
			admin.setUserType(AuthenticatedClientBean.ClientType.USER.ordinal());
			admin.setRecordRegDate(now);
			admin.setRecordUpdDate(now);
			admin.setRecordRegId(recordRegId);
			admin.setRecordUpdId(recordRegId);
			long lastInsertedAdminId = adminRepository.insert(admin,recordRegId);
			// register with 'User' Role
			adminRoleRepository.insert(new AdministratorRoleBean(lastInsertedAdminId, 3L), recordRegId);
		}
		catch (DAOException e) {
			throw new BusinessException(e.getMessage(), e);
		}
		repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for registering new " + objectName + " informations. ---");
		return lastInsertedId;
	}

	@Transactional
	public long updateUser(UserBean user, long recordUpdId) throws BusinessException, DuplicatedEntryException {
		String objectName = "User";
		repositoryLogger.debug("[START] : >>> --- This transaction was initiated by User ID # " + recordUpdId + " ---");
		repositoryLogger.debug("[START] : >>> --- Transaction start for updating " + objectName + " informations. ---");

		LocalDateTime now = LocalDateTime.now();
		user.setRecordUpdDate(now);
		user.setRecordUpdId(recordUpdId);

		long effectedRow;
		try {
			//update in Administrator
			AdministratorCriteria criteria = new AdministratorCriteria();
			criteria.setUserId(user.getId());
			HashMap<String,Object> updateItems = new HashMap<>();
			updateItems.put("name",user.getName());
			updateItems.put("loginId",user.getName());
			effectedRow = adminRepository.update(criteria,updateItems,recordUpdId);
			repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for updating Admin informations. --- effectedRows"+ effectedRow);
			effectedRow = mapper.update(user);
		}
		catch (DAOException e) {
			throw new BusinessException(e.getMessage(), e);
		}
		repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for updating " + objectName + " informations. ---");
		return effectedRow;
	}

	@Transactional
	public long deleteUser(long id, long recordUpdId) throws BusinessException, ConsistencyViolationException {
		String objectName = "User";
		repositoryLogger.debug("[START] : >>> --- This transaction was initiated by User ID # " + recordUpdId + " ---");
		repositoryLogger.debug("[START] : >>> --- Transaction start for removing " + objectName + " informations. ---");

		long deletedRows = 0;
		try {
			//remove in administrator
			AdministratorCriteria criteria = new AdministratorCriteria();
			criteria.setUserId(id);
			adminRepository.delete(criteria,recordUpdId);
			//remove in user
			deletedRows =  mapper.deleteByPrimaryKey(id);
		}
		catch (DAOException e) {
			throw new BusinessException(e.getMessage(), e);
		}
		repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for removing " + objectName + " informations. ---");
		return deletedRows;
	}

	@Transactional
	public long updatePassword(UserBean user, long recordUpdId) throws BusinessException, DuplicatedEntryException {
		String objectName = "User";
		repositoryLogger.debug("[START] : >>> --- This transaction was initiated by User ID # " + recordUpdId + " ---");
		repositoryLogger.debug("[START] : >>> --- Transaction start for updating  password informations. ---");


		long effectedRow;
		try {
			//update password user
			AdministratorCriteria criteria = new AdministratorCriteria();
			criteria.setUserId(user.getId());
			HashMap<String,Object> updateItems = new HashMap<>();
			updateItems.put("password",user.getPassword());
			updateItems.put("recordUpdId",recordUpdId);
			effectedRow = adminRepository.update(criteria,updateItems,recordUpdId);
			repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for updating password of admin. --- effectedRows"+ effectedRow);
			UserCriteria userCriteria = new UserCriteria();
			userCriteria.setId(user.getId());
			effectedRow = mapper.updateWithCriteria(userCriteria,updateItems);
		}
		catch (DAOException e) {
			throw new BusinessException(e.getMessage(), e);
		}
		repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for updating password informations. ---");
		return effectedRow;
	}
}
