package com.cws.easybill.portal.merchantportal.persistence.repository.base;

import com.cws.easybill.portal.merchantportal.exception.ConsistencyViolationException;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.PaginatedResultDto;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.CommonGenericMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.CommonGenericRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.InsertableRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.RemoveableRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.SelectableRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.UpdateableRepository;

import java.util.HashMap;
import java.util.List;

public class CommonGenericRepositoryImpl<T extends BaseBean, C extends CommonCriteria> implements CommonGenericRepository<T, C> {

	private final InsertableRepository<T> insertableRepository;
	private final UpdateableRepository<T, C> updateableRepository;
	private final RemoveableRepository<T, C> removeableRepository;
	private final SelectableRepository<T, C> selectableRepository;

	public CommonGenericRepositoryImpl(CommonGenericMapper<T, C> mapper) {
		insertableRepository = new InsertableRepositoryImpl<>(mapper);
		updateableRepository = new UpdateableRepositoryImpl<>(mapper);
		removeableRepository = new RemoveableRepositoryImpl<>(mapper);
		selectableRepository = new SelectableRepositoryImpl<>(mapper);
	}

	@Override
	public long insert(T record, long recordRegId) throws DuplicatedEntryException, DAOException {
		return insertableRepository.insert(record, recordRegId);
	}

	@Override
	public long insertWithId(T record, long recordRegId) throws DuplicatedEntryException, DAOException {
		return insertableRepository.insertWithId(record, recordRegId);
	}

	@Override
	public void insert(List<T> records, long recordRegId) throws DuplicatedEntryException, DAOException {
		insertableRepository.insert(records, recordRegId);
	}

	@Override
	public void insertWithId(List<T> records, long recordRegId) throws DuplicatedEntryException, DAOException {
		insertableRepository.insertWithId(records, recordRegId);
	}

	@Override
	public long delete(long primaryKey, long recordUpdId) throws ConsistencyViolationException, DAOException {
		return removeableRepository.delete(primaryKey, recordUpdId);
	}

	@Override
	public long delete(C criteria, long recordUpdId) throws ConsistencyViolationException, DAOException {
		return removeableRepository.delete(criteria, recordUpdId);
	}

	@Override
	public T select(long primaryKey) throws DAOException {
		return selectableRepository.select(primaryKey);
	}

	@Override
	public T select(C criteria) throws DAOException {
		return selectableRepository.select(criteria);
	}

	@Override
	public List<T> selectList(C criteria) throws DAOException {
		return selectableRepository.selectList(criteria);
	}

	@Override
	public long selectCounts(C criteria) throws DAOException {
		return selectableRepository.selectCounts(criteria);
	}

	@Override
	public PaginatedResultDto selectWithCounts(C criteria) throws DAOException {
		return selectableRepository.selectWithCounts(criteria);
	}

	@Override
	public long update(T record, long recordUpdId) throws DuplicatedEntryException, DAOException {
		return updateableRepository.update(record, recordUpdId);
	}

	@Override
	public long update(C criteria, HashMap<String, Object> updateItems, long recordUpdId) throws DAOException, DuplicatedEntryException {
		return updateableRepository.update(criteria, updateItems, recordUpdId);
	}
}
