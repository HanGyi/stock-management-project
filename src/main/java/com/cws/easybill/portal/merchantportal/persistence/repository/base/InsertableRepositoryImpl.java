package com.cws.easybill.portal.merchantportal.persistence.repository.base;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.InsertableMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.InsertableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.List;

import static com.cws.easybill.portal.merchantportal.util.LoggerConstants.DUPLICATE_KEY_INSERT_FAILED_MSG;
import static com.cws.easybill.portal.merchantportal.util.ObjectUtil.getObjectName;

public class InsertableRepositoryImpl<T extends BaseBean> implements InsertableRepository<T> {

	private static final Logger logger = LogManager.getLogger("repositoryLogs." + InsertableRepositoryImpl.class.getName());

	private final InsertableMapper<T> mapper;

	public InsertableRepositoryImpl(InsertableMapper<T> mapper) {
		this.mapper = mapper;
	}

	@Override
	public long insert(T record, long recordRegId) throws DuplicatedEntryException, DAOException {
		Assert.notNull(record, "Record shouldn't be Null.");
		String objectName = getObjectName(record);
		logger.debug("[START] : >>> --- Inserting single {} information with data ==> {} ---", objectName, record);
		try {
			LocalDateTime now = LocalDateTime.now();
			record.setRecordRegDate(now);
			record.setRecordUpdDate(now);
			record.setRecordRegId(recordRegId);
			record.setRecordUpdId(recordRegId);
			mapper.insert(record);
		}
		catch (DuplicateKeyException e) {
			String errorMsg = "xxx " + DUPLICATE_KEY_INSERT_FAILED_MSG + " xxx";
			throw new DuplicatedEntryException(errorMsg, e);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while inserting " + objectName + " data ==> " + record + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Inserting single {} informations with new Id # {} ---", objectName, record.getId());
		return record.getId();
	}

	@Override
	public long insertWithId(T record, long recordRegId) throws DuplicatedEntryException, DAOException {
		Assert.notNull(record, "Record shouldn't be Null.");
		String objectName = getObjectName(record);
		logger.debug("[START] : >>> --- Inserting single {} information with data ==> {} ---", objectName, record);
		try {
			LocalDateTime now = LocalDateTime.now();
			record.setRecordRegDate(now);
			record.setRecordUpdDate(now);
			record.setRecordRegId(recordRegId);
			record.setRecordUpdId(recordRegId);
			mapper.insertWithId(record);
		}
		catch (DuplicateKeyException e) {
			String errorMsg = "xxx " + DUPLICATE_KEY_INSERT_FAILED_MSG + " xxx";
			throw new DuplicatedEntryException(errorMsg, e);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while inserting " + objectName + " data ==> " + record + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Inserting single {} informations with new Id # {} ---", objectName, record.getId());
		return record.getId();
	}

	@Override
	public void insert(List<T> records, long recordRegId) throws DuplicatedEntryException, DAOException {
		Assert.notNull(records, "Records shouldn't be Null.");
		Assert.notEmpty(records, "Records shouldn't be Empty.");
		String objectName = getObjectName(records.get(0));
		logger.debug("[START] : >>> --- Inserting multi {} informations ---", objectName);
		LocalDateTime now = LocalDateTime.now();
		for (T record : records) {
			record.setRecordRegDate(now);
			record.setRecordUpdDate(now);
			record.setRecordRegId(recordRegId);
			record.setRecordUpdId(recordRegId);
		}
		try {
			mapper.insertList(records);
		}
		catch (DuplicateKeyException e) {
			String errorMsg = "xxx " + DUPLICATE_KEY_INSERT_FAILED_MSG + ". xxx";
			throw new DuplicatedEntryException(errorMsg, e);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while inserting multi " + objectName + " datas ==> " + records + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Inserting multi {} informations ---", objectName);
	}

	@Override
	public void insertWithId(List<T> records, long recordRegId) throws DuplicatedEntryException, DAOException {
		Assert.notNull(records, "Records shouldn't be Null.");
		Assert.notEmpty(records, "Records shouldn't be Empty.");
		String objectName = getObjectName(records.get(0));
		logger.debug("[START] : >>> --- Inserting multi {} informations ---", objectName);
		LocalDateTime now = LocalDateTime.now();
		for (T record : records) {
			record.setRecordRegDate(now);
			record.setRecordUpdDate(now);
			record.setRecordRegId(recordRegId);
			record.setRecordUpdId(recordRegId);
		}
		try {
			mapper.insertListWithIds(records);
		}
		catch (DuplicateKeyException e) {
			String errorMsg = "xxx " + DUPLICATE_KEY_INSERT_FAILED_MSG + ". xxx";
			throw new DuplicatedEntryException(errorMsg, e);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while inserting multi " + objectName + " datas ==> " + records + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Inserting multi {} informations ---", objectName);
	}
}
