package com.cws.easybill.portal.merchantportal.persistence.repository.base;

import com.cws.easybill.portal.merchantportal.exception.ConsistencyViolationException;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.RemoveableMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.RemoveableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.util.Assert;

import static com.cws.easybill.portal.merchantportal.util.LoggerConstants.DATA_INTEGRITY_VIOLATION_MSG;
import static com.cws.easybill.portal.merchantportal.util.ObjectUtil.getObjectName;

public class RemoveableRepositoryImpl<T extends BaseBean, C extends CommonCriteria> implements RemoveableRepository<T, C> {
	private static final Logger logger = LogManager.getLogger("repositoryLogs." + RemoveableRepositoryImpl.class.getName());

	private final RemoveableMapper<T, C> mapper;

	public RemoveableRepositoryImpl(RemoveableMapper<T, C> mapper) {
		this.mapper = mapper;
	}

	@Override
	public long delete(long primaryKey, long recordUpdId) throws ConsistencyViolationException, DAOException {
		logger.debug("[START] : >>> --- Deleting single object information with primaryKey # {} ---", primaryKey);
		long totalEffectedRows;
		try {
			totalEffectedRows = mapper.deleteByPrimaryKey(primaryKey);
		}
		catch (DataIntegrityViolationException e) {
			String errorMsg = "xxx " + DATA_INTEGRITY_VIOLATION_MSG + " xxx";
			throw new ConsistencyViolationException(errorMsg, e);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while deleting with primaryKey ==> " + primaryKey + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Deleting single object information with primaryKey ---");
		return totalEffectedRows;
	}

	@Override
	public long delete(C criteria, long recordUpdId) throws ConsistencyViolationException, DAOException {
		Assert.notNull(criteria, "Criteria shouldn't be Null.");
		String objectName = getObjectName(criteria.getObjectClass());
		long totalEffectedRows;
		logger.debug("[START] : >>> --- Deleting {} informations with criteria ==> {} ---", objectName, criteria);
		try {
			totalEffectedRows = mapper.deleteByCriteria(criteria);
		}
		catch (DataIntegrityViolationException e) {
			String errorMsg = "xxx " + DATA_INTEGRITY_VIOLATION_MSG + " xxx";
			throw new ConsistencyViolationException(errorMsg, e);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while deleting " + objectName + " data with criteria ==> " + criteria + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Deleting {} informations with criteria  ---", objectName);
		return totalEffectedRows;
	}
}
