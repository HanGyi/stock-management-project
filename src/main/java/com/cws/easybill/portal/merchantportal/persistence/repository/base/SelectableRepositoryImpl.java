package com.cws.easybill.portal.merchantportal.persistence.repository.base;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.PaginatedResultDto;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.SelectableMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.SelectableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.Assert;

import java.util.List;

import static com.cws.easybill.portal.merchantportal.util.ObjectUtil.getObjectName;

public class SelectableRepositoryImpl<T extends BaseBean, C extends CommonCriteria> implements SelectableRepository<T, C> {

	private static final Logger logger = LogManager.getLogger("repositoryLogs." + SelectableRepositoryImpl.class.getName());

	private final SelectableMapper<T, C> mapper;

	public SelectableRepositoryImpl(SelectableMapper<T, C> mapper) {
		this.mapper = mapper;
	}

	@Override
	public T select(long primaryKey) throws DAOException {
		logger.debug("[START] : >>> --- Fetching single object information with primaryKey # {} ---", primaryKey);
		T record;
		try {
			record = mapper.selectByPrimaryKey(primaryKey);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while fetching single object information with primaryKey ==> " + primaryKey + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Fetching single object information with primaryKey ---");
		return record;
	}

	@Override
	public T select(C criteria) throws DAOException {
		Assert.notNull(criteria, "Criteria shouldn't be Null.");
		String objectName = getObjectName(criteria.getObjectClass());
		logger.debug("[START] : >>> --- Fetching single {} information with criteria ==> {} ---", objectName, criteria);
		T record;
		try {
			record = mapper.selectSingleRecord(criteria);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while fetching single " + objectName + " information with criteria ==> " + criteria + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Fetching single {} information with criteria ---", objectName);
		return record;
	}

	@Override
	public List<T> selectList(C criteria) throws DAOException {
		Assert.notNull(criteria, "Criteria shouldn't be Null.");
		String objectName = getObjectName(criteria.getObjectClass());
		logger.debug("[START] : >>> --- Fetching multi {} informations with criteria ==> {} ---", objectName, criteria);
		List<T> records;
		try {
			records = mapper.selectMultiRecords(criteria);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while fetching multiple " + objectName + " informations with criteria ==> " + criteria + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Fetching multi {} informations with criteria ---", objectName);
		return records;
	}

	@Override
	public long selectCounts(C criteria) throws DAOException {
		Assert.notNull(criteria, "Criteria shouldn't be Null.");
		String objectName = getObjectName(criteria.getObjectClass());
		logger.debug("[START] : >>> --- Fetching {} counts with criteria ==> {} ---", objectName, criteria);
		long count;
		try {
			count = mapper.selectCounts(criteria);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while counting " + objectName + " records with criteria ==> " + criteria + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Fetching {} counts with criteria ---", objectName);
		return count;
	}

	@Override
	public PaginatedResultDto selectWithCounts(C criteria) throws DAOException {
		Assert.notNull(criteria, "Criteria shouldn't be Null.");
		String objectName = getObjectName(criteria.getObjectClass());
		logger.debug("[START] : >>> --- Transaction start for fetching paginated multi records by criteria ==> {} ---", criteria);

		PaginatedResultDto result = new PaginatedResultDto();
		try {
			result.setData(mapper.selectMultiRecords(criteria));
			result.setCount(mapper.selectCounts(criteria));
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while fetching " + objectName + " paginated multi records with criteria ==> " + criteria + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Fetching {} paginated multi records with criteria ---", objectName);
		return result;
	}
}
