package com.cws.easybill.portal.merchantportal.persistence.repository.base;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.base.UpdateableMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.UpdateableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.util.Assert;

import java.util.HashMap;

import static com.cws.easybill.portal.merchantportal.util.LoggerConstants.DUPLICATE_KEY_UPDATE_FAILED_MSG;
import static com.cws.easybill.portal.merchantportal.util.ObjectUtil.getObjectName;

public class UpdateableRepositoryImpl<T extends BaseBean, C extends CommonCriteria> implements UpdateableRepository<T, C> {
	private static final Logger logger = LogManager.getLogger("repositoryLogs." + UpdateableRepositoryImpl.class.getName());

	private final UpdateableMapper<T, C> mapper;

	public UpdateableRepositoryImpl(UpdateableMapper<T, C> mapper) {
		this.mapper = mapper;
	}

	@Override
	public long update(T record, long recordUpdId) throws DuplicatedEntryException, DAOException {
		Assert.notNull(record, "Record shouldn't be Null.");
		String objectName = getObjectName(record);
		long totalEffectedRows;
		logger.debug("[START] : >>> --- Updating single {} informations with Id # {} ---", objectName, record.getId());
		try {
			record.setRecordUpdId(recordUpdId);
			totalEffectedRows = mapper.update(record);
		}
		catch (DuplicateKeyException e) {
			String errorMsg = "xxx " + DUPLICATE_KEY_UPDATE_FAILED_MSG + " xxx";
			throw new DuplicatedEntryException(errorMsg, e);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while updating " + objectName + " data ==> " + record + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Updating single {} informations with Id ---", objectName);
		return totalEffectedRows;
	}

	@Override
	public long update(C criteria, HashMap<String, Object> updateItems, long recordUpdId) throws DAOException, DuplicatedEntryException {
		Assert.notNull(criteria, "Criteria shouldn't be Null.");
		Assert.notEmpty(updateItems, "UpdateItems shouldn't be Empty.");
		long totalEffectedRows;
		String objectName = getObjectName(criteria.getObjectClass());
		logger.debug("[START] : >>> --- Updating multi {} informations with criteria ==> {} ---", objectName, criteria);
		try {
			updateItems.put("recordUpdId", recordUpdId);
			totalEffectedRows = mapper.updateWithCriteria(criteria, updateItems);
		}
		catch (DuplicateKeyException e) {
			String errorMsg = "xxx " + DUPLICATE_KEY_UPDATE_FAILED_MSG + " xxx";
			throw new DuplicatedEntryException(errorMsg, e);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while updating multiple " + objectName + " informations [Values] ==> " + updateItems + " with [Criteria] ==> " + criteria + " xxx";
			throw new DAOException(errorMsg, e);
		}
		logger.debug("[FINISH] : <<< --- Updating multi {} informations with criteria ---", objectName);
		return totalEffectedRows;
	}
}
