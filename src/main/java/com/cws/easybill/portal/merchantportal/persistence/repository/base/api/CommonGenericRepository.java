package com.cws.easybill.portal.merchantportal.persistence.repository.base.api;

import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.InsertableRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.RemoveableRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.SelectableRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root.UpdateableRepository;

public interface CommonGenericRepository<T extends BaseBean, C extends CommonCriteria> extends SelectableRepository<T, C>, InsertableRepository<T>, UpdateableRepository<T, C>, RemoveableRepository<T, C> {

}
