package com.cws.easybill.portal.merchantportal.persistence.repository.base.api;

import com.cws.easybill.portal.merchantportal.exception.ConsistencyViolationException;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;

import java.util.List;
import java.util.Set;

public interface XGenericRepository<T extends BaseBean, C extends CommonCriteria> {

	void insert(T record, long recordRegId) throws DuplicatedEntryException, DAOException;

	void insert(List<T> records, long recordRegId) throws DuplicatedEntryException, DAOException;

	void insert(long key1, long key2, long recordRegId) throws DuplicatedEntryException, DAOException;

	long delete(long key1, long key2, long recordUpdId) throws ConsistencyViolationException, DAOException;

	long delete(C criteria, long recordUpdId) throws ConsistencyViolationException, DAOException;

	Set<Long> selectByKey1(long key1) throws DAOException;

	Set<Long> selectByKey2(long key2) throws DAOException;

	T select(long key1, long key2) throws DAOException;

	List<T> selectList(C criteria) throws DAOException;

	long selectCounts(C criteria) throws DAOException;
}
