package com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;

import java.util.List;

public interface InsertableRepository<T extends BaseBean> {
	long insert(T record, long recordRegId) throws DuplicatedEntryException, DAOException;

	long insertWithId(T record, long recordRegId) throws DuplicatedEntryException, DAOException;

	void insert(List<T> records, long recordRegId) throws DuplicatedEntryException, DAOException;

	void insertWithId(List<T> records, long recordRegId) throws DuplicatedEntryException, DAOException;
}
