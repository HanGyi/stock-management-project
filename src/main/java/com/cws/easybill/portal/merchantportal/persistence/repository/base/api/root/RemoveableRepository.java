package com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root;

import com.cws.easybill.portal.merchantportal.exception.ConsistencyViolationException;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;

public interface RemoveableRepository<T extends BaseBean, C extends CommonCriteria> {
	long delete(long primaryKey, long recordUpdId) throws ConsistencyViolationException, DAOException;

	long delete(C criteria, long recordUpdId) throws ConsistencyViolationException, DAOException;
}
