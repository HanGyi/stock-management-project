package com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.PaginatedResultDto;

import java.util.List;

public interface SelectableRepository<T extends BaseBean, C extends CommonCriteria> {
	T select(long primaryKey) throws DAOException;

	T select(C criteria) throws DAOException;

	List<T> selectList(C criteria) throws DAOException;

	long selectCounts(C criteria) throws DAOException;

	PaginatedResultDto selectWithCounts(C criteria) throws DAOException;
}
