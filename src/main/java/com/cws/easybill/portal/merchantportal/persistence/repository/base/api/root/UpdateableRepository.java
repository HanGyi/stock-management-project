package com.cws.easybill.portal.merchantportal.persistence.repository.base.api.root;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.BaseBean;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;

import java.util.HashMap;

public interface UpdateableRepository<T extends BaseBean, C extends CommonCriteria> {
	long update(T record, long recordUpdId) throws DuplicatedEntryException, DAOException;

	long update(C criteria, HashMap<String, Object> updateItems, long recordUpdId) throws DAOException, DuplicatedEntryException;
}
