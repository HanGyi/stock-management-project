package com.cws.easybill.portal.merchantportal.persistence.repository.setting;


import com.cws.easybill.portal.merchantportal.exception.BusinessException;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorRoleBean;
import com.cws.easybill.portal.merchantportal.model.bean.setting.CurrencyBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.criteria.setting.CurrencyCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.setting.CurrencyMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRoleRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.CommonGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.CommonGenericRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
public class CurrencyRepository extends CommonGenericRepositoryImpl<CurrencyBean, CurrencyCriteria> implements CommonGenericRepository<CurrencyBean, CurrencyCriteria> {

	private static final Logger repositoryLogger = LogManager.getLogger("repositoryLogs." + CurrencyRepository.class.getName());

	private CurrencyMapper mapper;

	@Autowired
	public CurrencyRepository(CurrencyMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}

}

