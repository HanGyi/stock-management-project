package com.cws.easybill.portal.merchantportal.persistence.repository.stock;

import com.cws.easybill.portal.merchantportal.exception.BusinessException;
import com.cws.easybill.portal.merchantportal.exception.ConsistencyViolationException;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorRoleBean;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import com.cws.easybill.portal.merchantportal.model.bean.stock.OrderBean;
import com.cws.easybill.portal.merchantportal.model.bean.stock.StockBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.UserCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.OrderCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.RequestData;
import com.cws.easybill.portal.merchantportal.model.test.Mail;
import com.cws.easybill.portal.merchantportal.persistence.mapper.stock.OrderMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRoleRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.UserRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.CommonGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.CommonGenericRepository;
import com.cws.easybill.portal.merchantportal.service.MailService;
import com.cws.easybill.portal.merchantportal.service.MessageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.print.attribute.HashDocAttributeSet;
import java.time.LocalDateTime;
import java.util.*;

@Repository
public class OrderRepository extends CommonGenericRepositoryImpl<OrderBean, OrderCriteria> implements CommonGenericRepository<OrderBean, OrderCriteria> {

	private static final Logger repositoryLogger = LogManager.getLogger("repositoryLogs." + OrderRepository.class.getName());

	private OrderMapper mapper;

	@Autowired
	private StockRepository stockRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AdministratorRepository administratorRepository;

	@Value("${USER_CREATE_EMAIL_TEMPLATE}")
	private String createUser;

	@Autowired
	private MessageService messagingService;

	@Autowired
	private MailService mailService;

	@Autowired
	public OrderRepository(OrderMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}

	@Transactional
	public long createOrder(OrderBean order, long recordRegId) throws BusinessException, DuplicatedEntryException {
		String objectName = "Order";
		repositoryLogger.debug("[START] : >>> --- This transaction was initiated by User ID # " + recordRegId + " ---");
		repositoryLogger.debug("[START] : >>> --- Transaction start for registering new " + objectName + " informations. ---");

		LocalDateTime now = LocalDateTime.now();
		order.setRecordRegDate(now);
		order.setRecordUpdDate(now);
		order.setRecordRegId(recordRegId);
		order.setRecordUpdId(recordRegId);

		long lastInsertedId,remainingQty = 0;
		try {
			lastInsertedId = insert(order,recordRegId);
			//minus from available quantity to selling quantity
			StockCriteria criteria = new StockCriteria();
			criteria.setCode(order.getStockCode());
			StockBean stock = stockRepository.select(criteria);
			if(stock != null) {
				remainingQty = Math.subtractExact(stock.getQuantity(),order.getQuantity());
			}
			//update stock with new quantity
			HashMap<String,Object> updateItems = new HashMap<>();
			updateItems.put("quantity",remainingQty);
			stockRepository.update(criteria,updateItems,recordRegId);

			AdministratorCriteria adminCriteria = new AdministratorCriteria();
			adminCriteria.setUserId(order.getUserId());
			AdministratorBean admin = administratorRepository.select(adminCriteria);
			UserCriteria usrCriteria = new UserCriteria();
			usrCriteria.setName(admin.getLoginId());
			UserBean user = userRepository.select(usrCriteria);
			if(user != null && remainingQty <= user.getStockLimit()){

				Mail mail = new Mail();
				mail.setMailFrom("no_reply@innovative.com");
				mail.setMailTo(user.getEmail());
				mail.setMailSubject("Stock Balance Information Report");

				HashMap<String, Object> emailModel = new HashMap<>();
				emailModel.put("userName", user.getName());
				emailModel.put("stockLimit", user.getStockLimit());
				emailModel.put("stockCode", stock.getCode());
				emailModel.put("quantity", remainingQty);
				mail.setModel(emailModel);

				boolean success = false;
				int count = 0;
				int MAX_TRIES = 3;
				while (!success && count++ < MAX_TRIES) {
					try {
						mailService.sendEmail(mail, "stock_over_limit_email.ftl");
						success = true;
					}
					catch (Exception e) {

						repositoryLogger.error("Mail Sending Process Failed.");
						repositoryLogger.error(e);
						try {
							Thread.sleep(900000);
						}
						catch (InterruptedException e1) {
							repositoryLogger.error(e1);
							e1.printStackTrace();
						}
					}
				}
			}
			//TODO (for sendgrid)
			//comment for using sendgrid mail
			/*if(user != null){

				Map<String, String> param = new HashMap<>();
				param.put("userName", user.getName());
				param.put("stockLimit", user.getStockLimit().toString());
				param.put("quantity", String.valueOf(remainingQty));
				param.put("stockCode",stock.getCode());

				RequestData requestData = new RequestData.RequestDataBuilder(user.getEmail(), createUser)
						.param(param)
						.build();
				try {
					messagingService.send(requestData);
				} catch (Exception e) {
					String errorMessage = "Email Exception: " + e.getMessage();
					repositoryLogger.error(errorMessage);
				}
			}*/
		}
		catch (DAOException e) {
			throw new BusinessException(e.getMessage(), e);
		}
		repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for registering new " + objectName + " informations. ---");
		return lastInsertedId;
	}

	@Transactional
	public long upateOrder(OrderBean order,Integer oldQty, long recordRegId) throws BusinessException, DuplicatedEntryException {
		String objectName = "Order";
		repositoryLogger.debug("[START] : >>> --- This transaction was initiated by User ID # " + recordRegId + " ---");
		repositoryLogger.debug("[START] : >>> --- Transaction start for updating  " + objectName + " informations. ---");

		LocalDateTime now = LocalDateTime.now();
		order.setRecordUpdDate(now);
		order.setRecordUpdId(recordRegId);

		long updatedId,remainingQty = 0;
		try {
			updatedId = update(order,recordRegId);
			//minus from available quantity to selling quantity
			StockCriteria criteria = new StockCriteria();
			criteria.setCode(order.getStockCode());
			StockBean stock = stockRepository.select(criteria);
			if(stock != null) {
				Integer quantity = 0;
				if(oldQty >= order.getQuantity()){
					quantity = Math.subtractExact(oldQty,order.getQuantity());
					remainingQty = Math.addExact(stock.getQuantity(),quantity);
				}else{
					quantity = Math.subtractExact(order.getQuantity(),oldQty);
					remainingQty = Math.subtractExact(stock.getQuantity(),quantity);
				}
			}
			//update stock with new quantity
			HashMap<String,Object> updateItems = new HashMap<>();
			updateItems.put("quantity",remainingQty);
			stockRepository.update(criteria,updateItems,recordRegId);

			AdministratorCriteria adminCriteria = new AdministratorCriteria();
			adminCriteria.setUserId(order.getUserId());
			AdministratorBean admin = administratorRepository.select(adminCriteria);
			UserCriteria usrCriteria = new UserCriteria();
			usrCriteria.setName(admin.getLoginId());
			UserBean user = userRepository.select(usrCriteria);
			if(user != null && remainingQty <= user.getStockLimit()){

				Mail mail = new Mail();
				mail.setMailFrom("no_reply@innovative.com");
				mail.setMailTo(user.getEmail());
				mail.setMailSubject("Stock Balance Information Report");

				HashMap<String, Object> emailModel = new HashMap<>();
				emailModel.put("userName", user.getName());
				emailModel.put("stockLimit", user.getStockLimit());
				emailModel.put("stockCode", stock.getCode());
				emailModel.put("quantity", remainingQty);
				mail.setModel(emailModel);

				boolean success = false;
				int count = 0;
				int MAX_TRIES = 3;
				while (!success && count++ < MAX_TRIES) {
					try {
						mailService.sendEmail(mail, "stock_over_limit_email.ftl");
						success = true;
					}
					catch (Exception e) {

						repositoryLogger.error("Mail Sending Process Failed.");
						repositoryLogger.error(e);
						try {
							Thread.sleep(900000);
						}
						catch (InterruptedException e1) {
							repositoryLogger.error(e1);
							e1.printStackTrace();
						}
					}
				}
			}
		}
		catch (DAOException e) {
			throw new BusinessException(e.getMessage(), e);
		}
		repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for updating " + objectName + " informations. ---");
		return updatedId;
	}

	@Transactional
	public long deleteOrder(OrderBean order,long id, long recordRegId) throws  DAOException {
		String objectName = "Order";
		repositoryLogger.debug("[START] : >>> --- This transaction was initiated by User ID # " + recordRegId + " ---");
		repositoryLogger.debug("[START] : >>> --- Transaction start for removing  " + objectName + " informations. ---");
		long effectedRows,remainingQty = 0;
		try{
			//add removing quality to available quantity in stock
			StockCriteria criteria = new StockCriteria();
			criteria.setCode(order.getStockCode());
			StockBean stock = stockRepository.select(criteria);
			if(stock != null) {
				remainingQty = Math.addExact(stock.getQuantity(),order.getQuantity());
			}
			//update stock with new quantity
			HashMap<String,Object> updateItems = new HashMap<>();
			updateItems.put("quantity",remainingQty);
			stockRepository.update(criteria,updateItems,recordRegId);

			//delete order
			effectedRows = mapper.deleteByPrimaryKey(id);
		}
		catch (Exception e) {
			String errorMsg = "xxx Error occured while removing" + objectName + " informations. ---";
			throw new DAOException(errorMsg, e);
		}
		repositoryLogger.debug("[FINISH] : >>> --- Transaction finished successfully for removing " + objectName + " informations. ---");
		return effectedRows;
	}

	public OrderBean orderStatistics(OrderCriteria criteria) {
		String objectName = "Order";
		repositoryLogger.debug("[START] : >>> --- Transaction start for fetching order statistics " + objectName + " informations. ---");
		OrderBean order = mapper.orderStatistics(criteria);
		return order;
	}
}
