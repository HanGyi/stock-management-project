package com.cws.easybill.portal.merchantportal.persistence.repository.stock;

import com.cws.easybill.portal.merchantportal.model.bean.stock.StockBean;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.persistence.mapper.stock.StockMapper;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.CommonGenericRepositoryImpl;
import com.cws.easybill.portal.merchantportal.persistence.repository.base.api.CommonGenericRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StockRepository extends CommonGenericRepositoryImpl<StockBean, StockCriteria> implements CommonGenericRepository<StockBean, StockCriteria> {

	@Autowired
	public StockRepository(StockMapper mapper) {
		super(mapper);
	}
}
