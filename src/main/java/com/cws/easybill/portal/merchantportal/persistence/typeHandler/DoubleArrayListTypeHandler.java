package com.cws.easybill.portal.merchantportal.persistence.typeHandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DoubleArrayListTypeHandler extends BaseTypeHandler<ArrayList<Double>> {
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, ArrayList<Double> parameter, JdbcType jdbcType) throws SQLException {
		StringBuilder str = new StringBuilder(parameter.toString());
		ps.setString(i, str.substring(1, str.length() - 1));
	}

	@Override
	public ArrayList<Double> getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String str = rs.getString(columnName);

		ArrayList<Double> results = new ArrayList<>();
		String[] arr = str.split(",");
		for (String s : arr) {
			if (s.trim().length() > 0) {
				results.add(Double.parseDouble(s));
			}
		}
		return results;
	}

	@Override
	public ArrayList<Double> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		String str = rs.getString(columnIndex);

		ArrayList<Double> results = new ArrayList<>();
		String[] arr = str.split(",");
		for (String s : arr) {
			if (s.trim().length() > 0) {
				results.add(Double.parseDouble(s));
			}
		}

		return results;
	}

	@Override
	public ArrayList<Double> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String str = cs.getString(columnIndex);

		ArrayList<Double> results = new ArrayList<>();
		String[] arr = str.split(",");
		for (String s : arr) {
			if (s.trim().length() > 0) {
				results.add(Double.parseDouble(s));
			}
		}

		return results;
	}
}
