package com.cws.easybill.portal.merchantportal.persistence.typeHandler;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.io.*;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class HashMapTypeHandler implements TypeHandler<Map<String, Object>> {

	public static Object toObject(byte[] bytes) throws IOException, ClassNotFoundException {
		Object obj;
		try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes); ObjectInputStream ois = new ObjectInputStream(bis)) {
			obj = ois.readObject();
		}
		return obj;
	}

	@Override
	public void setParameter(PreparedStatement ps, int i, Map<String, Object> parameter, JdbcType jdbcType) throws SQLException {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(parameter);
			byte[] bytes = baos.toByteArray();
			ps.setBytes(i, bytes);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<String, Object> getResult(ResultSet rs, String columnName) throws SQLException {
		try {
			Object obj = toObject(rs.getBytes(columnName));
			if (obj instanceof Map) {
				return (Map<String, Object>) obj;
			}
		}
		catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Map<String, Object> getResult(ResultSet rs, int columnIndex) throws SQLException {
		return null;
	}

	@Override
	public Map<String, Object> getResult(CallableStatement cs, int columnIndex) throws SQLException {
		return null;
	}
}
