package com.cws.easybill.portal.merchantportal.persistence.typeHandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LongArrayListTypeHandler extends BaseTypeHandler<ArrayList<Long>> {
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, ArrayList<Long> parameter, JdbcType jdbcType) throws SQLException {
		StringBuilder str = new StringBuilder(parameter.toString());
		ps.setString(i, str.substring(1, str.length() - 1));
	}

	@Override
	public ArrayList<Long> getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String str = rs.getString(columnName);

		ArrayList<Long> results = new ArrayList<>();
		String[] arr = str.split(",");
		for (String s : arr) {
			if (s.trim().length() > 0) {
				results.add(Long.parseLong(s));
			}
		}
		return results;
	}

	@Override
	public ArrayList<Long> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		String str = rs.getString(columnIndex);

		ArrayList<Long> results = new ArrayList<>();
		String[] arr = str.split(",");
		for (String s : arr) {
			if (s.trim().length() > 0) {
				results.add(Long.parseLong(s));
			}
		}

		return results;
	}

	@Override
	public ArrayList<Long> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String str = cs.getString(columnIndex);

		ArrayList<Long> results = new ArrayList<>();
		String[] arr = str.split(",");
		for (String s : arr) {
			if (s.trim().length() > 0) {
				results.add(Long.parseLong(s));
			}
		}

		return results;
	}
}
