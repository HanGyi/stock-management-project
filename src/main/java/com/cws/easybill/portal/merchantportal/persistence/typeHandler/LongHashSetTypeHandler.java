package com.cws.easybill.portal.merchantportal.persistence.typeHandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;

public class LongHashSetTypeHandler extends BaseTypeHandler<HashSet<Long>> {
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, HashSet<Long> parameter, JdbcType jdbcType) throws SQLException {
		StringBuilder str = new StringBuilder(parameter.toString());
		ps.setString(i, str.substring(1, str.length() - 1));
	}

	@Override
	public HashSet<Long> getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String str = rs.getString(columnName);

		HashSet<Long> results = new HashSet<>();
		String[] arr = str.split(",");
		for (String s : arr) {
			if (s.trim().length() > 0) {
				results.add(Long.parseLong(s));
			}
		}
		return results;
	}

	@Override
	public HashSet<Long> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		String str = rs.getString(columnIndex);

		HashSet<Long> results = new HashSet<>();
		String[] arr = str.split(",");
		for (String s : arr) {
			if (s.trim().length() > 0) {
				results.add(Long.parseLong(s));
			}
		}

		return results;
	}

	@Override
	public HashSet<Long> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String str = cs.getString(columnIndex);

		HashSet<Long> results = new HashSet<>();
		String[] arr = str.split(",");
		for (String s : arr) {
			if (s.trim().length() > 0) {
				results.add(Long.parseLong(s));
			}
		}

		return results;
	}
}
