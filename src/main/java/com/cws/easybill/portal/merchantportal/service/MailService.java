package com.cws.easybill.portal.merchantportal.service;

import com.cws.easybill.portal.merchantportal.model.test.Mail;
import freemarker.template.Configuration;
import freemarker.template.Template;
import jakarta.mail.Message;
import jakarta.mail.Multipart;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.util.Date;


@Service
public class MailService {

	@Autowired
	private JavaMailSender sender;

	@Autowired
	private Configuration freemarkerConfig;

	public void sendEmail(Mail mail, String templateName) throws Exception {
		MimeMessage message = sender.createMimeMessage();
		message.addHeader("format", "flowed");
		message.addHeader("Content-Transfer-Encoding", "8bit");
		message.setSentDate(new Date());
		message.setReplyTo(InternetAddress.parse("no_reply@thefourwatchers.com", false));

		message.setFrom(new InternetAddress("no_reply@thefourwatchers.com", mail.getMailFrom()));

		freemarkerConfig.setClassForTemplateLoading(getClass(), "/template/freemaker");
		Template t = freemarkerConfig.getTemplate(templateName);
		String body = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());
		message.setSubject(mail.getMailSubject(), "UTF-8");

		Multipart multipart = new MimeMultipart();

		MimeBodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(body, "text/html; charset=utf-8");
		multipart.addBodyPart(htmlPart);

		message.setContent(multipart);

		message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getMailTo(), false));
		if (mail.getMailCc() != null) {
			message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(mail.getMailCc(), false));
		}
		if (mail.getMailBcc() != null) {
			message.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(mail.getMailBcc(), false));
		}

		sender.send(message);
	}
}


