package com.cws.easybill.portal.merchantportal.service;

import com.cws.easybill.portal.merchantportal.model.dto.RequestData;

public interface MessageService {
	void send(RequestData requestData);
}
