package com.cws.easybill.portal.merchantportal.service.impl;

import com.cws.easybill.portal.merchantportal.model.dto.RequestData;
import com.cws.easybill.portal.merchantportal.service.MessageService;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sendgrid.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class SendGridMessagingServiceImpl implements MessageService {

	private static final Logger logger = LoggerFactory.getLogger(SendGridMessagingServiceImpl.class);

	@Value("${FROM_EMAIL}")
	private String fromMail;

	@Value("${SEND_GRID_API_KEY}")
	private String apiKey;

	@Override
	public void send(RequestData requestData) {
		logger.info(">>> start sending email >>>" + requestData.getTo());
		Email from = new Email(fromMail);
		Email to = new Email(requestData.getTo());
		from.setName("Merchant");
		to.setEmail(requestData.getTo());

		DynamicTemplatePersonalization personalization = new DynamicTemplatePersonalization();
		personalization.addTo(to);

		for (Map.Entry<String, String> entry : requestData.getParams().entrySet()) {
			personalization.addDynamicTemplateData(entry.getKey(), entry.getValue());
		}

		Mail mail = new Mail();
		mail.setFrom(from);
		mail.addPersonalization(personalization);
		mail.setTemplateId(requestData.getTemplate());


		SendGrid sg = new SendGrid(apiKey);
		Request requests = new Request();

		try {
			requests.setMethod(Method.POST);
			requests.setEndpoint("mail/send");
			requests.setBody(mail.build());

			Response response = sg.api(requests);
			logger.info("Sent Successfully");

		} catch (IOException ex) {
			logger.info("Fail Sending Email");

		}
	}

	private static class DynamicTemplatePersonalization extends Personalization {
		@JsonProperty(value = "dynamic_template_data")
		private Map<String, String> dynamic_template_data;

		@JsonProperty("dynamic_template_data")
		public Map<String, String> getDynamicTemplateData() {
			if (dynamic_template_data == null) {
				return Collections.<String, String>emptyMap();
			}
			return dynamic_template_data;
		}

		public void addDynamicTemplateData(String key, String value) {
			if (dynamic_template_data == null) {
				dynamic_template_data = new HashMap<String, String>();
				dynamic_template_data.put(key, value);
			} else {
				dynamic_template_data.put(key, value);
			}
		}

	}
}