package com.cws.easybill.portal.merchantportal.service.report;

import com.cws.easybill.portal.merchantportal.common.enums.ExportType;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.stock.OrderBean;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.OrderCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.OrderRepository;
import jakarta.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class OrderReportService {

	@Autowired
	private OrderRepository orderRepository;

	public String generateReport() {
		try {

			OrderCriteria orderCriteria = new OrderCriteria();
			//Get daily order amount
			Date todayStartDate = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
			Date todayEndDate = Date.from(LocalTime.MAX.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant());
			orderCriteria.setStartDate(todayStartDate);
			orderCriteria.setEndDate(todayEndDate);
			orderCriteria.setWithMetaData(true);

			List<OrderBean> orders = orderRepository.selectList(orderCriteria);

			String reportPath = "D:\\Content\\Report";

			File file = ResourceUtils.getFile("classpath:order_list_report.jrxml");


			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager
					.compileReport(file.getAbsolutePath());

			// Get your data source
			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(orders);

			// Add parameters
			Map<String, Object> parameters = new HashMap<>();

			parameters.put("companyName", "Innovative Solutoin");

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);

			// Export the report to a PDF file
			JasperExportManager.exportReportToPdfFile(jasperPrint, reportPath + "\\order_list_report.pdf");
			//JasperExportManager.exportReportToPdf(jasperPrint);

			//JasperExportManager.exportReportToXmlFile(jasperPrint, reportPath + "\\Emp-Rpt-Database.xls",false);


			// Step 5: Export the filled report to Excel
			/*JRXlsExporter exporter = new JRXlsExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE );
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE );
			exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS,Boolean.FALSE);
			exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER,Boolean.TRUE);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, reportPath+"\\output_file_tt.xls"); // Output file name
			exporter.exportReport();*/

			System.out.println("Done");

			return "Report successfully generated @path= " + reportPath;
		} catch (Exception e) {
			e.printStackTrace();
			return "Error--> check the console log";
		}
	}

	public void generateReport(OrderCriteria orderCriteria, HttpServletResponse response) throws JRException, IOException, DAOException {
		//Get daily order amount
		Date todayStartDate = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date todayEndDate = Date.from(LocalTime.MAX.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant());
		orderCriteria.setStartDate(todayStartDate);
		orderCriteria.setEndDate(todayEndDate);
		orderCriteria.setWithMetaData(true);
		List<OrderBean> orders = orderRepository.selectList(orderCriteria);
		exportReport(orders, orderCriteria.getReportType(), response);
	}

	public void generateCurMonthReport(OrderCriteria orderCriteria, HttpServletResponse response) throws JRException, IOException, DAOException {
		//Get monthly order amount
		LocalDate now = LocalDate.now();
		LocalDate firstDayOfMonth = now.withDayOfMonth(1);
		Date fDayOfMonth = Date.from(LocalTime.MIN.atDate(firstDayOfMonth).atZone(ZoneId.systemDefault()).toInstant());
		now.getMonth();
		LocalDate lastDayOfMonthDate = now.withDayOfMonth(
				now.getMonth().length(now.isLeapYear()));
		Date lDayOfMonth = Date.from(LocalTime.MAX.atDate(lastDayOfMonthDate).atZone(ZoneId.systemDefault()).toInstant());
		orderCriteria.setStartDate(fDayOfMonth);
		orderCriteria.setEndDate(lDayOfMonth);
		orderCriteria.setWithMetaData(true);
		List<OrderBean> orders = orderRepository.selectList(orderCriteria);
		exportReport(orders, orderCriteria.getReportType(), response);
	}

	public void generateReportWithCriteria(OrderCriteria orderCriteria, HttpServletResponse response) throws JRException, IOException, DAOException {
		//Get daily order amount
		Date todayStartDate = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date todayEndDate = Date.from(LocalTime.MAX.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant());
		orderCriteria.setStartDate(todayStartDate);
		orderCriteria.setEndDate(todayEndDate);
		orderCriteria.setWithMetaData(true);
		List<OrderBean> orders = orderRepository.selectList(orderCriteria);
		exportReport(orders, orderCriteria.getReportType(), response);
	}

	private void exportReport(Collection<?> beanCollection, ExportType exportType, HttpServletResponse response) throws JRException, IOException {
		InputStream transactionReportStream =
				getClass()
						.getResourceAsStream(
								"/order_list_report.jrxml");
		String titleTransactionBy = "Order List Report";

		JasperReport jasperReport = JasperCompileManager.compileReport(transactionReportStream);
		JRBeanCollectionDataSource beanColDataSource =
				new JRBeanCollectionDataSource(beanCollection);
		HashMap<String, Object> parameters = new HashMap();
		parameters.put("companyName", titleTransactionBy);

		JasperPrint jasperPrint =
				JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		var dateTimeNow = LocalDateTime.now().format(formatter);
		var fileName = titleTransactionBy.replace(" ", "") + dateTimeNow;

		if (exportType == ExportType.PDF) {

			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
			response.setContentType("application/pdf");
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName + ".pdf;");
			exporter.exportReport();

		} else if (exportType == ExportType.EXCEL) {

			JRXlsxExporter exporter = new JRXlsxExporter();
			SimpleXlsxReportConfiguration reportConfigXLS = new SimpleXlsxReportConfiguration();
			reportConfigXLS.setSheetNames(new String[]{titleTransactionBy});
			reportConfigXLS.setDetectCellType(true);
			reportConfigXLS.setCollapseRowSpan(false);
			exporter.setConfiguration(reportConfigXLS);
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName + ".xlsx;");
			response.setContentType("application/octet-stream");
			exporter.exportReport();

		} else if (exportType == ExportType.CSV) {

			JRCsvExporter exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			var outputStream = response.getOutputStream();
			exporter.setExporterOutput((new SimpleWriterExporterOutput(outputStream)));
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName + ".csv;");
			response.setContentType("text/csv");
			exporter.exportReport();

		} else if (exportType == ExportType.DOCX) {

			JRDocxExporter exporter = new JRDocxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName + ".docx;");
			response.setContentType("application/octet-stream");
			exporter.exportReport();

		} else {
			throw new RuntimeException("File Format isn't supported!");
		}
	}
}
