package com.cws.easybill.portal.merchantportal.service.report;

import com.cws.easybill.portal.merchantportal.common.enums.ExportType;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import com.cws.easybill.portal.merchantportal.model.bean.stock.StockBean;
import com.cws.easybill.portal.merchantportal.model.criteria.UserCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.UserRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.StockRepository;
import jakarta.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class StockReportService {

	@Autowired
	private StockRepository stockRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AdministratorRepository adminRepository;

	public void genLessStockReport(StockCriteria criteria, HttpServletResponse response) throws JRException, IOException, DAOException {
		UserBean user = userRepository.select(criteria.getUserId());
		criteria.setWithLessValue(true);
		criteria.setUserId(user.getId());
		criteria.setQuantity(user.getStockLimit());
		List<StockBean> stocks = stockRepository.selectList(criteria);
		exportReport(stocks, criteria.getReportType(), response,"Less of Stock List Report");
	}

	public void genOutofStockReport(StockCriteria criteria, HttpServletResponse response) throws JRException, IOException, DAOException {
		UserBean user = userRepository.select(criteria.getUserId());
		criteria.setWithExactValue(true);
		criteria.setQuantity(0L);
		criteria.setUserId(user.getId());
		List<StockBean> stocks = stockRepository.selectList(criteria);
		exportReport(stocks, criteria.getReportType(), response,"Out of Stock List Report");
	}

	private void exportReport(Collection<?> beanCollection, ExportType exportType, HttpServletResponse response,String title) throws JRException, IOException {
		InputStream transactionReportStream =
				getClass()
						.getResourceAsStream(
								"/stock_list_report.jrxml");
		String titleTransactionBy = title;

		JasperReport jasperReport = JasperCompileManager.compileReport(transactionReportStream);
		JRBeanCollectionDataSource beanColDataSource =
				new JRBeanCollectionDataSource(beanCollection);
		HashMap<String, Object> parameters = new HashMap();
		parameters.put("companyName", titleTransactionBy);
		parameters.put("title", "Stock Listing");

		JasperPrint jasperPrint =
				JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		var dateTimeNow = LocalDateTime.now().format(formatter);
		var fileName = titleTransactionBy.replace(" ", "") + dateTimeNow;

		if (exportType == ExportType.PDF) {

			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
			response.setContentType("application/pdf");
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName + ".pdf;");
			exporter.exportReport();

		} else if (exportType == ExportType.EXCEL) {

			JRXlsxExporter exporter = new JRXlsxExporter();
			SimpleXlsxReportConfiguration reportConfigXLS = new SimpleXlsxReportConfiguration();
			reportConfigXLS.setSheetNames(new String[]{titleTransactionBy});
			reportConfigXLS.setDetectCellType(true);
			reportConfigXLS.setCollapseRowSpan(false);
			exporter.setConfiguration(reportConfigXLS);
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName + ".xlsx;");
			response.setContentType("application/octet-stream");
			exporter.exportReport();

		} else if (exportType == ExportType.CSV) {

			JRCsvExporter exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			var outputStream = response.getOutputStream();
			exporter.setExporterOutput((new SimpleWriterExporterOutput(outputStream)));
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName + ".csv;");
			response.setContentType("text/csv");
			exporter.exportReport();

		} else if (exportType == ExportType.DOCX) {

			JRDocxExporter exporter = new JRDocxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
			response.setHeader(
					HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName + ".docx;");
			response.setContentType("application/octet-stream");
			exporter.exportReport();

		} else {
			throw new RuntimeException("File Format isn't supported!");
		}
	}
}
