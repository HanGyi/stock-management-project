package com.cws.easybill.portal.merchantportal.util;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "external-urls")
@Data
@NoArgsConstructor
public class ExternalAPIURLs {
	private String arttha;
	private SmsBrixConnection smsBrixConnection;

	@Data
	public static class SmsBrixConnection {
		private String smsSendUrl;
		private String userName;
		private String password;
		private String senderId;
	}

}
