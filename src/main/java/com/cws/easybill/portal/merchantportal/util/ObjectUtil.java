package com.cws.easybill.portal.merchantportal.util;

public class ObjectUtil {
	public static String getObjectName(Object record) {
		return " '" + record.getClass().getSimpleName().replace("Bean", "") + "' ";
	}

	public static String getObjectName(Class<?> clazz) {
		return " '" + clazz.getSimpleName().replace("Bean", "") + "' ";
	}
}
