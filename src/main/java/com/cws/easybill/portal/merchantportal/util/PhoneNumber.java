package com.cws.easybill.portal.merchantportal.util;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "phonenumbers")
@Data
@NoArgsConstructor
public class PhoneNumber {

	private AgentTransaction agent;
	private CustomerTransaction customer;

	@Data
	public static class AgentTransaction {
		private String aungbarlay;
	}

	@Data
	public static class CustomerTransaction {
		private String aungbarlay;
	}

}
