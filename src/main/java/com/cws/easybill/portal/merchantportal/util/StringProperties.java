package com.cws.easybill.portal.merchantportal.util;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "string-properties")
@NoArgsConstructor
public class StringProperties {

	private String secretKey;
	private String otpLifeTime;
	private String deepLink;

	private AblErrorCode ablErrorCode;
	private TransactionStatus transactionStatus;
	private ApiResponseTitle apiResponseTitle;
	private ApiResponseMessage apiResponseMessage;

	@Data
	public static class AblErrorCode {
		private String success;
		private String unknown;
	}
	@Data
	public static class TransactionStatus {
		private String initiated;
		private String otpSent;
		private String success;
		private String failed;
	}

	@Data
	public static class ApiResponseTitle {
		private String payRequestSuccess;
		private String paymentSuccess;
		private String paymentFailed;
		private String duplicatedTransaction;
		private String noMatchingInfo;
		private String otpSent;
		private String invalidOtp;
	}

	@Data
	public static class ApiResponseMessage {
		private String payRequestSuccess;
		private String paymentSuccess;
		private String paymentFailed;
		private String duplicatedTransaction;
		private String noMatchingInfo;
		private String otpSent;
		private String invalidOtp;
	}

}
