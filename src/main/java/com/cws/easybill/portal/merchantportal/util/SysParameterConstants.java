package com.cws.easybill.portal.merchantportal.util;

public class SysParameterConstants {
	public static final Integer USER_TYPE_SUPER_USER = 0;
	public static final Integer USER_TYPE_ADMIN = 1;
	public static final Integer USER_TYPE_USER = 2;
}
