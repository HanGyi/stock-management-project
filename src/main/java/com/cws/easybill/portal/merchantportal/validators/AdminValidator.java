package com.cws.easybill.portal.merchantportal.validators;

import com.cws.easybill.portal.merchantportal.common.response.PageMode;
import com.cws.easybill.portal.merchantportal.common.validation.BaseValidator;
import com.cws.easybill.portal.merchantportal.common.validation.FieldValidator;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorCriteria;
import com.cws.easybill.portal.merchantportal.model.dto.AdminDto;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AdminValidator extends BaseValidator {

	@Autowired
	private AdministratorRepository repository;

	@Override
	public boolean supports(Class clazz) {
		return AdministratorBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		AdminDto admin = (AdminDto) obj;
		//Name
		validateIsEmpty(new FieldValidator("name", "Name", admin.getName(), errors));
		validateIsValidMaxValue(new FieldValidator("name", "Name", admin.getName(), errors), 50);

		// Roles
		validateIsEmpty(new FieldValidator("roleIds", "Role", admin.getRoleIds(), errors));

		// Status
		validateIsEmpty(new FieldValidator("status", "Status", admin.getStatus(), errors));

		if (pageMode == PageMode.CREATE) {
			//Login ID
			validateIsEmpty(new FieldValidator("loginId", "Login ID", admin.getLoginId(), errors));
			validateIsValidMaxValue(new FieldValidator("loginId", "Login ID", admin.getLoginId(), errors), 50);

			if (errors.getFieldErrors("loginId").size() == 0) {
				AdministratorCriteria criteria = new AdministratorCriteria();
				criteria.setLoginId(admin.getLoginId());
				Long count = null;
				try {
					count = repository.selectCounts(criteria);
				}
				catch (DAOException e) {

					errors.rejectValue("loginId", "", messageSource.getMessage("Validation.Administrator.Page.DuplicateLoginID", admin.getLoginId()));
				}
				if (count != null && count > 0) {
					errors.rejectValue("loginId", "", messageSource.getMessage("Validation.Administrator.Page.DuplicateLoginID", admin.getLoginId()));
				}
			}

			//Password
			validateIsEmpty(new FieldValidator("password", "Password", admin.getPassword(), errors));
			validateIsValidMinValue(new FieldValidator("password", "Password", admin.getPassword(), errors), 8);
			//Confirm Password
			validateIsEmpty(new FieldValidator("confirmPassword", "Confirm Password", admin.getConfirmPassword(), errors));
			validateIsValidMinValue(new FieldValidator("confirmPassword", "Confirm Password", admin.getConfirmPassword(), errors), 8);

			validateIsEqual("confirmPassword", new FieldValidator("password", "Password", admin.getPassword(), errors), new FieldValidator("confirmPassword", "Confirm Password", admin.getConfirmPassword(), errors), errors);
		}
	}
}
