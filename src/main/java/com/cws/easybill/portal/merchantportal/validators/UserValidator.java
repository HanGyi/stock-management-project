package com.cws.easybill.portal.merchantportal.validators;

import com.cws.easybill.portal.merchantportal.common.response.PageMode;
import com.cws.easybill.portal.merchantportal.common.validation.BaseValidator;
import com.cws.easybill.portal.merchantportal.common.validation.FieldValidator;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.UserCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserValidator extends BaseValidator {

	@Autowired
	private UserRepository repository;

	@Autowired
	private AdministratorRepository adminRepository;

	@Override
	public boolean supports(Class clazz) {
		return UserBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		UserBean user = (UserBean) obj;
		//Name
		validateIsEmpty(new FieldValidator("name", "Name", user.getName(), errors));
		validateIsValidMaxValue(new FieldValidator("name", "Name", user.getName(), errors), 50);

		//Email
		validateIsEmpty(new FieldValidator("email","Email",user.getEmail(),errors));
		validateIsValidMaxValue(new FieldValidator("email", "Email", user.getName(), errors), 50);

		//Currency
		validateIsEmpty(new FieldValidator("currency","Currency",user.getCurrency(),errors));

		//Stock Limit
		validateIsEmpty(new FieldValidator("stockLimit","Stock Limit",user.getStockLimit(),errors));

		// Status
		validateIsEmpty(new FieldValidator("status", "Status", user.getStatus(), errors));

		if (pageMode == PageMode.CREATE) {
			//check Login ID(Name of User)
			if (errors.getFieldErrors("name").size() == 0) {
				UserCriteria criteria = new UserCriteria();
				criteria.setName(user.getName());
				AdministratorCriteria adminCriteria = new AdministratorCriteria();
				adminCriteria.setLoginId(user.getName());
				Long count = null;
				Long userCount = null;
				try {
					count = adminRepository.selectCounts(adminCriteria);
					userCount = repository.selectCounts(criteria);
				} catch (DAOException e) {

					errors.rejectValue("name", "", messageSource.getMessage("Validation.User.Page.DuplicateLoginID", "Name", user.getName()));
				}
				if ((count != null && count > 0) || (userCount != null && userCount > 0)) {
					errors.rejectValue("name", "", messageSource.getMessage("Validation.User.Page.DuplicateLoginID", "Name", user.getName()));
				}
			}

			//Password
			validateIsEmpty(new FieldValidator("password", "Password", user.getPassword(), errors));
			validateIsValidMinValue(new FieldValidator("password", "Password", user.getPassword(), errors), 8);
//			//Confirm Password
//			validateIsEmpty(new FieldValidator("confirmPassword", "Confirm Password", user.getConfirmPassword(), errors));
//			validateIsValidMinValue(new FieldValidator("confirmPassword", "Confirm Password", user.getConfirmPassword(), errors), 8);
//
//			validateIsEqual("confirmPassword", new FieldValidator("password", "Password", user.getPassword(), errors), new FieldValidator("confirmPassword", "Confirm Password", user.getConfirmPassword(), errors), errors);
		}
		if(errors.getFieldErrors("email").size() == 0){
			UserBean userInfo = new UserBean();
			UserCriteria criteria = new UserCriteria();
			criteria.setEmail(user.getEmail());
			Long count = null;
			try{
				count = repository.selectCounts(criteria);
				if (pageMode == PageMode.EDIT) {
					criteria = new UserCriteria();
					criteria.setName(user.getName());
					userInfo = repository.select(criteria);
				}
			}catch (DAOException e) {

				errors.rejectValue("email", "", messageSource.getMessage("Validation.User.Page.DuplicateLoginID","Email",user.getEmail()));
			}
			if (pageMode == PageMode.CREATE) {
				if ((count != null && count > 0)) {
					errors.rejectValue("email", "", messageSource.getMessage("Validation.User.Page.DuplicateLoginID", "Email", user.getEmail()));
				}
			}else{
				if ((count != null && count > 0) && !userInfo.getEmail().equals(user.getEmail()) ) {
					errors.rejectValue("email", "", messageSource.getMessage("Validation.User.Page.DuplicateLoginID", "Email", user.getEmail()));
				}
			}
		}


	}
}
