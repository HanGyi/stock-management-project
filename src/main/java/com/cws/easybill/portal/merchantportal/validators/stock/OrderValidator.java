package com.cws.easybill.portal.merchantportal.validators.stock;

import com.cws.easybill.portal.merchantportal.common.response.PageMode;
import com.cws.easybill.portal.merchantportal.common.validation.BaseValidator;
import com.cws.easybill.portal.merchantportal.common.validation.FieldValidator;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.stock.OrderBean;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.OrderCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderValidator extends BaseValidator {
	@Autowired
	private OrderRepository repository;


	@Override
	public boolean supports(Class clazz) {
		return OrderBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		OrderBean order = (OrderBean) obj;
		//Code
		if(pageMode == PageMode.CREATE){
			validateIsEmpty(new FieldValidator("orderCode", "Stock Code", order.getStockCode(), errors));
			validateIsValidMaxValue(new FieldValidator("orderCode", "Stock Code", order.getStockCode(), errors), 5);
		}

		//Quantity
		validateIsEmpty(new FieldValidator("quantity","Quantity",order.getQuantity(),errors));

		//Amount
		validateIsEmpty(new FieldValidator("amount","Amount",order.getAmount(),errors));

		//Total Amount
		validateIsEmpty(new FieldValidator("totalAmount","Total Amount",order.getTotalAmount(),errors));

		//Check quantity
		if (errors.getFieldErrors("quantity").size() == 0) {
			if(order.getQuantity() > order.getAvailableQty()) {
				errors.rejectValue("quantity", "", messageSource.getMessage("Validation.Order.Page.InvalidQty", order.getQuantity(), order.getAvailableQty()));
			}
		}
	}
}
