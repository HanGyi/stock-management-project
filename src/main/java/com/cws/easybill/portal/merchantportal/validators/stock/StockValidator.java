package com.cws.easybill.portal.merchantportal.validators.stock;

import com.cws.easybill.portal.merchantportal.common.response.PageMode;
import com.cws.easybill.portal.merchantportal.common.validation.BaseValidator;
import com.cws.easybill.portal.merchantportal.common.validation.FieldValidator;
import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.model.bean.stock.StockBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StockValidator extends BaseValidator {

	@Autowired
	private StockRepository repository;


	@Override
	public boolean supports(Class clazz) {
		return StockBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		StockBean stock = (StockBean) obj;
		//Code
		validateIsEmpty(new FieldValidator("code", "Code", stock.getCode(), errors));
		validateIsValidMaxValue(new FieldValidator("code", "Code", stock.getCode(), errors), 5);

		//Currency
		validateIsEmpty(new FieldValidator("originalPrice","Original Price",stock.getOriginalPrice(),errors));

		//Stock Limit
		validateIsEmpty(new FieldValidator("sellPrice","Sell Price",stock.getSellPrice(),errors));


		if (pageMode == PageMode.CREATE) {
			//Quantity
			validateIsEmpty(new FieldValidator("quantity","Quantity",stock.getQuantity(),errors));

			//check Code
			if (errors.getFieldErrors("code").size() == 0) {
				StockCriteria criteria = new StockCriteria();
				criteria.setCode(stock.getCode());
				Long count = null;
				try {
					count = repository.selectCounts(criteria);
				}
				catch (DAOException e) {

					errors.rejectValue("code", "", messageSource.getMessage("Validation.User.Page.DuplicateLoginID", "Code", stock.getCode()));
				}
				if ((count != null && count > 0)) {
					errors.rejectValue("code", "", messageSource.getMessage("Validation.User.Page.DuplicateLoginID", "Code", stock.getCode()));
				}
			}
		}
	}
}
