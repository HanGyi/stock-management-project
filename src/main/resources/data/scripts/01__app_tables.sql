/* Create Extensions */
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
/* Drop Tables */

/* Drop Tables */
DROP TABLE IF EXISTS mjr_administrator_role;
DROP TABLE IF EXISTS mjr_administrator;
DROP TABLE IF EXISTS mjr_role_action;
DROP TABLE IF EXISTS mjr_action;
DROP TABLE IF EXISTS mjr_role;
DROP TABLE IF EXISTS mjr_login_history;
DROP TABLE IF EXISTS mjr_static_content;
DROP TABLE IF EXISTS mjr_stock;
DROP TABLE IF EXISTS mjr_order;
DROP TABLE IF EXISTS mjr_user;
DROP TABLE IF EXISTS mjr_currency;

CREATE SEQUENCE admin_id_seq START 1000;


/* Create Tables */
CREATE TABLE mjr_action
(
	id SERIAL PRIMARY KEY,
	app_name varchar(30) NOT NULL,
	page varchar(50) NOT NULL,
	action_name varchar(50) NOT NULL,
	display_name varchar(50) NOT NULL,
	action_type smallint NOT NULL,
	url varchar NOT NULL,
	description varchar(200) NOT NULL,
	record_reg_id bigint NOT NULL,
	record_upd_id bigint NOT NULL,
	record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
	record_upd_date timestamp DEFAULT current_timestamp NOT NULL,
	CONSTRAINT unk_app_action_name UNIQUE (app_name, action_name),
	CONSTRAINT unk_app_url UNIQUE (app_name, url)
) WITHOUT OIDS;

CREATE TABLE mjr_role
(
	id SERIAL PRIMARY KEY,
	app_name varchar(20) NOT NULL,
	name varchar(20) NOT NULL,
	type smallint DEFAULT 1 NOT NULL,
	description varchar(200) NOT NULL,
	record_reg_id bigint NOT NULL,
	record_upd_id bigint NOT NULL,
	record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
	record_upd_date timestamp DEFAULT current_timestamp NOT NULL
) WITHOUT OIDS;

CREATE TABLE mjr_role_action
(
    id SERIAL PRIMARY KEY,
	role_id bigint NOT NULL,
	action_id bigint NOT NULL,
	record_reg_id bigint NOT NULL,
	record_upd_id bigint NOT NULL,
	record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
	record_upd_date timestamp DEFAULT current_timestamp NOT NULL,
	CONSTRAINT unk_role_action UNIQUE (role_id, action_id)
) WITHOUT OIDS;

CREATE TABLE mjr_login_history
(
	id SERIAL PRIMARY KEY,
	client_id bigint NOT NULL,
	client_type smallint NOT NULL,
	ip_address varchar(45) NOT NULL,
	os varchar(100),
	client_agent varchar(100),
	login_date timestamp NOT NULL,
	record_reg_id bigint NOT NULL,
	record_upd_id bigint NOT NULL,
	record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
	record_upd_date timestamp DEFAULT current_timestamp NOT NULL
) WITHOUT OIDS;

CREATE TABLE mjr_static_content
(
	id SERIAL PRIMARY KEY,
	file_name varchar(200) NOT NULL,
	file_path varchar NOT NULL,
	file_size varchar(20),
	file_type smallint NOT NULL,
	record_reg_id bigint NOT NULL,
	record_upd_id bigint NOT NULL,
	record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
	record_upd_date timestamp DEFAULT current_timestamp NOT NULL
) WITHOUT OIDS;

CREATE TABLE mjr_administrator
(
	id INTEGER  DEFAULT nextval('admin_id_seq')  PRIMARY KEY,
	content_id bigint,
	name varchar(50) NOT NULL,
	login_id varchar(50) NOT NULL UNIQUE,
	password varchar(200) NOT NULL,
	status smallint DEFAULT 0 NOT NULL,
	user_id bigint,
	record_reg_id bigint NOT NULL,
	record_upd_id bigint NOT NULL,
	record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
	record_upd_date timestamp DEFAULT current_timestamp NOT NULL
) WITHOUT OIDS;

CREATE TABLE mjr_administrator_role
(
	administrator_id bigint NOT NULL,
	role_id bigint NOT NULL,
	record_reg_id bigint NOT NULL,
	record_upd_id bigint NOT NULL,
	record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
	record_upd_date timestamp DEFAULT current_timestamp NOT NULL,
	CONSTRAINT unk_user_role UNIQUE (administrator_id, role_id)
) WITHOUT OIDS;

CREATE TABLE mjr_stock
(
    id SERIAL PRIMARY KEY,
    code varchar(20) NOT NULL,
    image_url varchar(255),
    quantity bigint NOT NULL,
    original_price numeric(15,6) NOT NULL,
    sell_price numeric(15,6) NOT NULL,
    user_id bigint NOT NULL,
    record_reg_id bigint NOT NULL,
    record_upd_id bigint NOT NULL,
    record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
    record_upd_date timestamp DEFAULT current_timestamp NOT NULL
) WITHOUT OIDS;

CREATE TABLE mjr_order
(
    id SERIAL PRIMARY KEY,
    stock_code varchar(20) NOT NULL,
    quantity bigint NOT NULL,
    amount numeric(15,6) NOT NULL,
    total_amount numeric(15,6) NOT NULL,
    user_id bigint NOT NULL,
    record_reg_id bigint NOT NULL,
    record_upd_id bigint NOT NULL,
    record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
    record_upd_date timestamp DEFAULT current_timestamp NOT NULL
) WITHOUT OIDS;

CREATE TABLE mjr_user
(
    id SERIAL PRIMARY KEY,
    content_id bigint,
    name varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    email_noti_status smallint DEFAULT 0 NOT NULL,
    currency varchar(5) NOT NULL,
    stock_limit bigint NOT NULL,
    user_id bigint NOT NULL,
    record_reg_id bigint NOT NULL,
    record_upd_id bigint NOT NULL,
    record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
    record_upd_date timestamp DEFAULT current_timestamp NOT NULL
) WITHOUT OIDS;

CREATE TABLE mjr_currency
(
    id SERIAL PRIMARY KEY,
    code varchar(10) NOT NULL,
    name varchar(100) NOT NULL,
    symbol varchar(5) NOT NULL,
    convert_unit bigint NOT NULL,
    record_reg_id bigint NOT NULL,
    record_upd_id bigint NOT NULL,
    record_reg_date timestamp DEFAULT current_timestamp NOT NULL,
    record_upd_date timestamp DEFAULT current_timestamp NOT NULL,
    CONSTRAINT unk_code UNIQUE (code)
) WITHOUT OIDS;



ALTER TABLE mjr_stock
    ADD CONSTRAINT frk_user_id FOREIGN KEY (user_id)
        REFERENCES mjr_user (id)
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
;


ALTER TABLE mjr_user
    ADD CONSTRAINT frk_content_user FOREIGN KEY (content_id)
        REFERENCES mjr_static_content (id)
        ON UPDATE NO ACTION
        ON DELETE SET NULL
;

ALTER TABLE mjr_administrator
    ADD COLUMN user_type smallint DEFAULT 2 NOT NULL;


/* Create Foreign Keys */
ALTER TABLE mjr_role_action
	ADD CONSTRAINT frk_action_role FOREIGN KEY (action_id)
	REFERENCES mjr_action (id)
	ON UPDATE NO ACTION
	ON DELETE CASCADE
;

ALTER TABLE mjr_role_action
	ADD CONSTRAINT frk_role_action FOREIGN KEY (role_id)
	REFERENCES mjr_role (id)
	ON UPDATE NO ACTION
	ON DELETE CASCADE
;

ALTER TABLE mjr_administrator_role
	ADD CONSTRAINT frk_administrator_role FOREIGN KEY (administrator_id)
	REFERENCES mjr_administrator (id)
	ON UPDATE NO ACTION
	ON DELETE CASCADE
;

ALTER TABLE mjr_administrator_role
	ADD CONSTRAINT frk_role_user FOREIGN KEY (role_id)
	REFERENCES mjr_role (id)
	ON UPDATE NO ACTION
	ON DELETE CASCADE
;

ALTER TABLE mjr_administrator
	ADD CONSTRAINT frk_content_administrator FOREIGN KEY (content_id)
	REFERENCES mjr_static_content (id)
	ON UPDATE NO ACTION
	ON DELETE SET NULL
;

/* Create Indexes */
CREATE INDEX index_action_tbl_page ON mjr_action (page);
CREATE INDEX index_loginhistory_tbl_user_id ON mjr_login_history (client_id);
CREATE INDEX index_administrator_tbl_content_id ON mjr_administrator (content_id);

/* Comments */
COMMENT ON COLUMN mjr_action.action_type IS '0[main-action that is the main page action]
1[sub-action that process within a page]';
COMMENT ON COLUMN mjr_login_history.os IS 'Operating System';



