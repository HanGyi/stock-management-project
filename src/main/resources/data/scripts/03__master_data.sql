/******      Administrator        ******/
INSERT INTO mjr_administrator
(id,content_id, name, 			        login_id, 					                     password, 															  	status,  			   record_reg_id ,record_upd_id ,record_reg_date ,record_upd_date , user_type )
VALUES
/******      SU#cws$S3c43T     ******/
(1, null, 	    'Super User', 	    	'superuser@stockmgmt.com', 			         '$2a$10$gADOcQkZtCAM5Y5F3na1qebcjncCe6yTCa7vGP8UyI7pVQ0DbYoWq', 	 	1, 		 			   0,0,current_timestamp,current_timestamp,0);

/******      Role        ******/
INSERT INTO mjr_role 
(id,	  app_name,         name, 			    description, 							                                                                                                record_reg_id,record_upd_id,record_reg_date,record_upd_date)
VALUES 
(1,		 'portal-ui-app',  'Super-User',	    	'Master role to manage entire application. This role own special right, advantage, or immunity granted or available.',			 		0,0,current_timestamp,current_timestamp),
(2,		 'portal-ui-app',  'Administrator',		  'Default Administrator role',					                                                                                       	0,0,current_timestamp,current_timestamp),
(3,		 'portal-ui-app',  'User',		          'Default User role',					                                                                                       	0,0,current_timestamp,current_timestamp);

/******     Administrator Role       ******/
INSERT INTO mjr_administrator_role
(administrator_id, 	role_id, 	  	record_reg_id,record_upd_id,record_reg_date,record_upd_date)
VALUES 
/******      Super User     ******/
(1,			  		1,	      		0,0,current_timestamp,current_timestamp);

/******      action        ******/ 
INSERT INTO mjr_action  
(id,	 app_name, 					 page,						 action_name,			      		display_name,										action_type,		url,													                    description,																					                   record_reg_id,record_upd_id,record_reg_date,record_upd_date)
VALUES 	
----- Dashboard
(10011,	'portal-ui-app',			'Dashboard',			 	 'dashboard',		      			'Dashboard page for User',								0,				'^/dashboard$',									              				'Control panel page for Sign-in User.',															                    0,0,current_timestamp,current_timestamp),
----- Administrators
(10021,	'portal-ui-app',		    'Administrator',			 'adminList',		      			'Admin Home page',								        0,				'^/admins$',									              				'...',															                                                    0,0,current_timestamp,current_timestamp),
(10022,	'portal-ui-app',		    'Administrator',			 'adminAdd',		      			'Admin register page',								    1,				'^/admins/add$',									              		    '...',															                                                    0,0,current_timestamp,current_timestamp),
(10023,	'portal-ui-app',		    'Administrator',			 'adminEdit',		      			'Admin edit page',								        1,				'^/admins/[0-9]{1,}/edit$',									                '...',															                                                    0,0,current_timestamp,current_timestamp),
(10024,	'portal-ui-app',		    'Administrator',			 'adminRemove',		      			'To remove existing administrator',						1,				'^/admins/[0-9]{1,}/delete$',									            '...',															                                                    0,0,current_timestamp,current_timestamp),
(10025,	'portal-ui-app',		    'Administrator',			 'adminDetail',		      			'View detail information of an administrator',			1,				'^/admins/[0-9]{1,}$',									                    '...',															                                                    0,0,current_timestamp,current_timestamp),
----- Transactions
(10031,	'portal-ui-app',		    'Transaction',			     'transactionList',		      		'Transaction Home page',								0,				'^/transaction$',									              			'...',															                                                    0,0,current_timestamp,current_timestamp),
(10032,	'portal-ui-app',		    'Transaction',			     'transactionDetail',		      	'View detail information of a Transaction',			    1,				'^/transaction/[0-9]{1,}$',									                '...',															                                                    0,0,current_timestamp,current_timestamp),
----- CashIn
(10041,	'portal-ui-app',		    'CashIn',			         'cashInList',		      			'CashIn Home page',								        0,				'^/cashin$',									              				'...',															                                                    0,0,current_timestamp,current_timestamp),
(10042,	'portal-ui-app',		    'CashIn',			         'cashInDetail',		      		'View detail information of a CashIn',			        1,				'^/cashin/[0-9]{1,}$',									                    '...',															                                                    0,0,current_timestamp,current_timestamp),
----- Users
(10051,	'portal-ui-app',		    'User',			             'userList',		      			'User Home page',								        0,				'^/users$',									              				    '...',															                                                    0,0,current_timestamp,current_timestamp),
(10052,	'portal-ui-app',		    'User',			             'userAdd',		      			    'User register page',								    1,				'^/users/add$',									              		        '...',															                                                    0,0,current_timestamp,current_timestamp),
(10053,	'portal-ui-app',		    'User',			             'userEdit',		      			'User edit page',								        1,				'^/users/[0-9]{1,}/edit$',									                '...',															                                                    0,0,current_timestamp,current_timestamp),
(10054,	'portal-ui-app',		    'User',			             'userRemove',		      			'To remove existing user',						        1,				'^/users/[0-9]{1,}/delete$',									            '...',															                                                    0,0,current_timestamp,current_timestamp),
(10055,	'portal-ui-app',		    'User',			             'userDetail',		      			'View detail information of an user',			        1,				'^/users/[0-9]{1,}$',									                    '...',															                                                    0,0,current_timestamp,current_timestamp),
(10056,	'portal-ui-app',		    'User',			             'profile',		      			    'Profile information of an user',			            0,				'^/users/profile$',									                        '...',															                                                    0,0,current_timestamp,current_timestamp),
----- Stocks
(10061,	'portal-ui-app',		    'Stock',			         'stockList',		      			'Stock Home page',								        0,				'^/stocks$',									              				'...',															                                                    0,0,current_timestamp,current_timestamp),
(10062,	'portal-ui-app',		    'Stock',			         'stockAdd',		      			'Stock register page',								    1,				'^/stocks/add$',									              		    '...',															                                                    0,0,current_timestamp,current_timestamp),
(10063,	'portal-ui-app',		    'Stock',			         'stockEdit',		      			'Stock edit page',								        1,				'^/stocks/[0-9]{1,}/edit$',									                '...',															                                                    0,0,current_timestamp,current_timestamp),
(10064,	'portal-ui-app',		    'Stock',			         'stockRemove',		      			'To remove existing stock',						        1,				'^/stocks/[0-9]{1,}/delete$',									            '...',															                                                    0,0,current_timestamp,current_timestamp),
(10065,	'portal-ui-app',		    'Stock',			         'stockDetail',		      			'View detail information of a stock',			        1,				'^/stocks/[0-9]{1,}$',									                    '...',															                                                    0,0,current_timestamp,current_timestamp),
----- Orders
(10071,	'portal-ui-app',		    'Order',			         'orderList',		      			'Order Home page',								        0,				'^/orders$',									              				'...',															                                                    0,0,current_timestamp,current_timestamp),
(10072,	'portal-ui-app',		    'Order',			         'orderAdd',		      			'Order register page',								    1,				'^/orders/add$',									              		    '...',															                                                    0,0,current_timestamp,current_timestamp),
(10073,	'portal-ui-app',		    'Order',			         'orderEdit',		      			'Order edit page',								        1,				'^/orders/[0-9]{1,}/edit$',									                '...',															                                                    0,0,current_timestamp,current_timestamp),
(10074,	'portal-ui-app',		    'Order',			         'orderRemove',		      			'To remove existing order',						        1,				'^/orders/[0-9]{1,}/delete$',									            '...',															                                                    0,0,current_timestamp,current_timestamp),
(10075,	'portal-ui-app',		    'Order',			         'orderDetail',		      			'View detail information of a order',			        1,				'^/orders/[0-9]{1,}$',									                    '...',															                                                    0,0,current_timestamp,current_timestamp);



INSERT INTO mjr_role_action
(role_id,	action_id,		record_reg_id,record_upd_id,record_reg_date,record_upd_date)
VALUES 
----- Super User
(1,			10011,		    0,0,current_timestamp,current_timestamp),
----- Administrators
(1,			10021,      0,0,current_timestamp,current_timestamp),
(1,			10022,      0,0,current_timestamp,current_timestamp),
(1,			10023,      0,0,current_timestamp,current_timestamp),
(1,			10024,      0,0,current_timestamp,current_timestamp),
(1,			10025,      0,0,current_timestamp,current_timestamp),
----- Transactions
(1,			10031,      0,0,current_timestamp,current_timestamp),
(1,			10032,      0,0,current_timestamp,current_timestamp),
----- CashIn
(1,			10041,      0,0,current_timestamp,current_timestamp),
(1,			10042,      0,0,current_timestamp,current_timestamp),
----- Users
(1,			10051,      0,0,current_timestamp,current_timestamp),
(1,			10052,      0,0,current_timestamp,current_timestamp),
(1,			10053,      0,0,current_timestamp,current_timestamp),
(1,			10054,      0,0,current_timestamp,current_timestamp),
(1,			10055,      0,0,current_timestamp,current_timestamp),
(3,			10056,      0,0,current_timestamp,current_timestamp),
----- Stocks
(1,			10061,      0,0,current_timestamp,current_timestamp),
(1,			10062,      0,0,current_timestamp,current_timestamp),
(1,			10063,      0,0,current_timestamp,current_timestamp),
(1,			10064,      0,0,current_timestamp,current_timestamp),
(1,			10065,      0,0,current_timestamp,current_timestamp),
(3,			10061,      0,0,current_timestamp,current_timestamp),
(3,			10062,      0,0,current_timestamp,current_timestamp),
(3,			10063,      0,0,current_timestamp,current_timestamp),
(3,			10064,      0,0,current_timestamp,current_timestamp),
(3,			10065,      0,0,current_timestamp,current_timestamp),
----- Order
(1,			10071,      0,0,current_timestamp,current_timestamp),
(1,			10072,      0,0,current_timestamp,current_timestamp),
(1,			10073,      0,0,current_timestamp,current_timestamp),
(1,			10074,      0,0,current_timestamp,current_timestamp),
(1,			10075,      0,0,current_timestamp,current_timestamp),
(3,			10071,      0,0,current_timestamp,current_timestamp),
(3,			10072,      0,0,current_timestamp,current_timestamp),
(3,			10073,      0,0,current_timestamp,current_timestamp),
(3,			10074,      0,0,current_timestamp,current_timestamp),
(3,			10075,      0,0,current_timestamp,current_timestamp),
----- Administrator -----
----- Dashboard
(2,			10011,      0,0,current_timestamp,current_timestamp),
(3,			10011,      0,0,current_timestamp,current_timestamp);
----- Administrators

/***  Currency ***/
INSERT INTO mjr_currency
(id, code,	name, symbol, convert_unit,		record_reg_id,record_upd_id,record_reg_date,record_upd_date)
VALUES
 (1, 'MMK', 'Myanmar Kyat', 'MMK', 1,		0,0,current_timestamp,current_timestamp);
