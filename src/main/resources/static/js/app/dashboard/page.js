let userId;
function init(){
    loadUsers();
    if (userId) {
        setTimeout(function () {
            outOfStockDetail(userId);
            lessStockDetail(userId);
            todayOrders(userId);
            curMonthOrders(userId);
            profitDetail(userId);
        }, 2000);
    }
}
function bind(){
    $("#userId").change(function () {
        let userId = $(this).val();
        outOfStockDetail(userId);
        lessStockDetail(userId);
        todayOrders(userId);
        curMonthOrders(userId);
        profitDetail(userId);
    });
}

function outOfStockDetail(userId){
    let criteria = {};
    criteria.withExactValue = true;
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'stocks/outOfStockStatistics',
        data: JSON.stringify(criteria),
        success: function (data) {
            $("#outOfStockCount").val(data.outOfStockCount);
        }
    });
}

function lessStockDetail(userId){
    let criteria = {};
    criteria.withLessValue = true;
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'stocks/lessStockStatistics',
        data: JSON.stringify(criteria),
        success: function (data) {
            $("#lessStockCount").text(data.lessStockCount);
        }
    });
}

function todayOrders(userId){
    let criteria = {};
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'orders/todayOrderStatistics',
        data: JSON.stringify(criteria),
        success: function (data) {
            $("#todayOrdCount").text(data.todayOrdCount);
        }
    });
}

function curMonthOrders(userId){
    let criteria = {};
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'orders/monthlyOrderStatistics',
        data: JSON.stringify(criteria),
        success: function (data) {
            $("#curMonthOrdCount").text(data.curMonthOrdCount);
        }
    });
}

function profitDetail(userId){
    let criteria = {};
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath()+ 'orders/profitStatistics',
        data:JSON.stringify(criteria),
        success:function (data){
            $("#pftMessage").text(data.pftMessage);
        }
    });
}
function loadUsers() {
    let criteria = {};
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'users/search/list',
        data: JSON.stringify(criteria),
        success: function (data) {
            let options = [];
            let isFirstItem = true;
            $.each(data, function (key, item) {
                let option = "<option value='" + item.id + "'";
                // If it's the first item, mark it as selected
                if (isFirstItem) {
                    option += " selected";
                    userId = item.id;
                    isFirstItem = false; // Update flag to false after marking the first item
                }
                option += ">" + item.name + "</option>";
                options.push(option);
            });
            $("#userId").html(options).selectpicker('refresh');
        }
    });
}
