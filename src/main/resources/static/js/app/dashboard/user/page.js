let userId;
function init(){
    userId = $("#userId").val();
    outOfStockDetail(userId);
    lessStockDetail(userId);
    todayOrders(userId);
    curMonthOrders(userId);
    profitDetail(userId);
}
function bind(){
}

function outOfStockDetail(userId){
    let criteria = {};
    criteria.withExactValue = true;
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'stocks/outOfStockStatistics',
        data: JSON.stringify(criteria),
        success: function (data) {
            $("#outOfStockCount").text(data.outOfStockCount);
        }
    });
}

function lessStockDetail(userId){
    let criteria = {};
    criteria.withLessValue = true;
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'stocks/lessStockStatistics',
        data: JSON.stringify(criteria),
        success: function (data) {
            $("#lessStockCount").text(data.lessStockCount);
        }
    });
}

function todayOrders(userId){
    let criteria = {};
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'orders/todayOrderStatistics',
        data: JSON.stringify(criteria),
        success: function (data) {
            $("#todayOrdCount").text(data.todayOrdCount);
        }
    });
}

function curMonthOrders(userId){
    let criteria = {};
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'orders/monthlyOrderStatistics',
        data: JSON.stringify(criteria),
        success: function (data) {
            $("#curMonthOrdCount").text(data.curMonthOrdCount);
        }
    });
}

function profitDetail(userId){
    let criteria = {};
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath()+ 'orders/profitStatistics',
        data:JSON.stringify(criteria),
        success:function (data){
            $("#pftMessage").text(data.pftMessage);
        }
    });
}
