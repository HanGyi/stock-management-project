let dataTable;

function init() {
    loadUsers();
    initDataTable();
}

function bind() {

    $("#btnSearch").on('click', function (e) {
        dataTable.search($(this).val()).draw();
    });

    $("#btnReset").on('click', function (e) {
        $('form').trigger('reset');
        dataTable.search($(this).val()).draw();
    });

}

function initDataTable() {
    let columns = [
         {
            "mData": "stockCode",
         },
        {
            "mData": "quantity",
        },
        {
            "mData": "amount",
            "bSortable": false,
        },
        {
            "mData": "totalAmount",
            "bSortable": false,
        },];
    if (hasAuthority("orderDetail") || hasAuthority("orderEdit") || hasAuthority("orderRemove")) {
        columns.push({
            "render": function (data, type, full, meta) {
                let detailButton = {
                    label: "View",
                    authorityName: "orderDetail",
                    url: getContextPath() + "/orders/" + full.id,
                    styleClass: "",
                    data_id: full.id
                };
                /*let editButton = {
                    label: "Edit",
                    authorityName: "orderEdit",
                    url: getContextPath() + "/orders/" + full.id + '/edit',
                    styleClass: "",
                    data_id: full.id
                };*/
                let removeButton = {
                    label: "Remove",
                    authorityName: "orderRemove",
                    url: getContextPath() + "/orders/" + full.id + '/delete',
                    styleClass: "remove",
                    data_id: full.id
                };
                return generateAuthorizedButtonGroup([detailButton, removeButton]);
            },
            "bSortable": false,
            "sClass": "text-center"
        });
    }
    dataTable = $('#tblOrder').DataTable({
        aoColumns: columns,
        "aaSorting": [],
        columnDefs: [{
            width: 100,
            targets: 2
        }],
        ajax: {
            type: "POST",
            url: getApiResourcePath() + 'orders/search/paging',
            data: function (d) {
                let criteria = {};
                if (d.order.length > 0) {
                    let index = $(d.order[0])[0].column;
                    let dir = $(d.order[0])[0].dir;
                    let head = $("#tblOrder").find("thead");
                    let sortColumn = head.find("th:eq(" + index + ")");
                    criteria.order = dir.toUpperCase();
                    criteria.orderBy = $(sortColumn).attr("data-sort-key");
                }
                criteria.offset = d.start;
                criteria.limit = d.length;
                let word = $("#keyword").val();
                let userId = $("#userId").val();
                if (isNotEmpty(word)) {
                    criteria.word = word.trim();
                }
                if(isNotEmpty(userId)) {
                    criteria.userId = userId;
                }
                return JSON.stringify(criteria);
            }
        },
        initComplete: function () {
            let api = this.api();
            $('#keyword').off('.DT').on('keyup.DT', function (e) {
                if (e.keyCode === 13) {
                    api.search(this.value).draw();
                }
            });
        },
        drawCallback: function (settings) {
            bindRemoveButtonEvent();
        }
    });
}
function loadUsers() {
    let criteria = {};
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'users/search/list',
        data: JSON.stringify(criteria),
        success: function (data) {
            let options = [];
            options.push("<option value=''>All</option>");
            $.each(data, function (key, item) {
                let option = "<option value='" + item.id + "'>" + item.name + "</option>";
                options.push(option);
            });
            $("#userId").html(options).selectpicker('refresh');
        }
    });

}
