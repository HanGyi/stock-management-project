let userId;
let oldQty;
function init() {
    initValidator();
    loadStocks();
    loadUsers();
    if(getPageMode() === 'EDIT'){
        oldQty = $("#quantity").val();
    }
}

function bind() {

    $("#btnReset").on("click", function (e) {
        $("form").trigger("reset");
        clearOldValidationErrorMessages();
    });

    $("#btnCancel").on("click", function (e) {
        goToHomePage();
    });

    $("#btnSubmit").on("click", function (e) {
        if ($("#frm-order").valid()) {
            $("#stockCode").removeAttr('disabled');
            $("#availableQty").removeAttr('disabled');
            $("#amount").removeAttr('disabled');
            $("#totalAmount").removeAttr('disabled');
        }
    });

    $("#stockCode").change(function () {
        let code = $(this).val();
        let quantity = $("#quantity").val();
        let amount,totalAmount,availableQty;
        let criteria = {};
        criteria.code = code;
        $.ajax({
            type: "POST",
            url: getApiResourcePath() + 'stocks/search/record',
            data: JSON.stringify(criteria),
            success: function (data) {
                availableQty = data.quantity;
                if(getPageMode() === 'EDIT'){
                    if(oldQty >= quantity){
                        availableQty += oldQty - quantity;
                    }else{
                        availableQty -= quantity - oldQty;
                    }
                }
                amount = data.sellPrice;
                totalAmount = quantity * amount;
                $("#amount").val(amount);
                $("#totalAmount").val(totalAmount);
                $("#availableQty").val(availableQty);
            }
        });
    });

    $("#userId").change(function () {
        userId = $(this).val();
        loadStocks();
    });

    $("#quantity").change(function(){
        $("#stockCode").change();
    });

}

function loadUsers() {
    let criteria = {};
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'users/search/list',
        data: JSON.stringify(criteria),
        success: function (data) {
            let options = [];
            $.each(data, function (key, item) {
                let option = "<option value='" + item.id + "'>" + item.name + "</option>";
                options.push(option);
            });
            $("#userId").html(options).selectpicker('refresh');
        }
    });

}

function initValidator() {
    $("#frm-order").validate({
        rules: {
            "stockCode": {
                required: true,
                maxlength: 5
            },
            "quantity": {
                required: true,
                checkQty: "#availableQty"
            },
            "amount": {
                required: true
            },
            "totalAmount": {
                required: true
            },
            "userId":{
                required: true
            }
        },
        messages: {
            "stockCode": {
                required: "'Stock Code' should not be empty.",
                maxlength: "'Stock Code' should not exceeds 5 characters."
            },
            "quantity": {
                required: "'Quantity' should not be empty."
            },
            "amount": {
                required: "'Amount' should not be empty."
            },
            "totalAmount": {
                required: "Total Amount' should not be empty."
            },
            "userId":{
                required: "Please Choose at least 'One' User."
            }
        }
    });
}

function loadStocks() {
    let criteria = {};
    criteria.userId = userId;
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'stocks/search/list',
        data: JSON.stringify(criteria),
        success: function (data) {
            let options = [];
            $.each(data, function (key, item) {
                let option = "<option value='" + item.code + "'>" + item.code + "</option>";
                options.push(option);
            });
            $("#stockCode").html(options).selectpicker('refresh');
        }
    });

}

jQuery.validator.addMethod("math", function(value, params) {
    return this.optional(element) || value == params[0] + params[1];
}, jQuery.validator.format("Please enter the correct value for {0} + {1}"));


jQuery.validator.addMethod("checkQty", function(value, element,params) {
    // Convert values to numbers
    let quantity = parseFloat(value);
    let availableQty = parseFloat($(params).val());
    // Check if quantity is less than or equal to available quantity
    return quantity <= availableQty;
}, "Quantity must be equal or less than Available Qty!");
