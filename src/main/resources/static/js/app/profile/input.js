function init() {
    initValidator();
    loadCurrencies();
}

function bind() {

    $("#btnReset").on("click", function (e) {
        $("form").trigger("reset");
        clearOldValidationErrorMessages();
    });

    $("#btnCancel").on("click", function (e) {
        goToHomePage();
    });

    $('[data-dismiss="modal"]').click(function(e) {
       clearOldValidationErrorMessages();
       clearDatas();
    });

    $("#changePassword").on("click", function (e) {
        $("#modal-change-password").modal({
            backdrop: 'static',
            keyboard: false
        });
       /* $("#btn-update-password").off('click').on('click', function (e) {
            $("#modal-change-password").modal("hide");
        });*/
    });

    $("#btn-update-password").on('click', function (e) {
        e.preventDefault();
        if ($("#frm-ChangePassword").valid()) {
            //$(this).prop('disabled', true).addClass('disabled');
            $.ajax({
                url: getContextPath() + "/users/update/password",
                type: "POST",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                data: {
                    'oldPassword': $("#change_oldPassword").val(),
                    'newPassword': $("#change_newPassword").val(),
                    'confirmPassword': $("#change_confirmPassword").val(),
                },
                success: function (response) {
                    handleServerResponse(response);
                    //$("#btn-update-password").prop('disabled', false).removeClass('disabled');
                    if (response.status === "METHOD_NOT_ALLOWED") {
                        $.each(response.fieldErrors, function(key, value) {
                            $("#"+key).addClass("is-invalid");
                            $("#"+key).parent().append(
                                '<div class="invalid-feedback"><span>' + value + '</span></div>');
                        });
                    }else if (response.status == "OK"){
                        clearDatas();
                    }
                },
            });
        }
    });

}

function loadCurrencies() {
    let criteria = {};
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'currencies/search/list',
        data: JSON.stringify(criteria),
        success: function (data) {
            let options = [];
            $.each(data, function (key, item) {
                let option = "<option value='" + item.code + "'>" + item.name + "</option>";
                options.push(option);
            });
            $("#currency").html(options).selectpicker('refresh');
        }
    });

}

function initValidator() {
    $("#frm-user").validate({
        rules: {
            "name": {
                required: true,
                maxlength: 50
            },
            "email": {
                required: true,
                maxlength: 50
            },
            "currency": {
                required: true
            },
            "stockLimit": {
                required: true
            },
        },
        messages: {
            "name": {
                required: "'Name' should not be empty.",
                maxlength: "'Name' should not exceeds 50 characters."
            },
            "email": {
                required: "'Email' should not be empty.",
                maxlength: "'Email' should not exceeds 50 characters."
            },
            "currency": {
                required: "Currency' should not be empty.",
            },
            "stockLimit": {
                required: "'Stock Limit' should not be empty.",
            },
        }
    });

    $("#frm-ChangePassword").validate({
        rules: {
            "change_oldPassword": {
                required: true,
                minlength: 8
            },
            "change_newPassword": {
                required: true,
                minlength: 8
            },
            "change_confirmPassword": {
                required: true,
                minlength: 8,
                equalTo: "#change_newPassword"
            },
        },
        messages: {
            "change_oldPassword": {
                required: "'Old Password' should not be empty.",
                minlength: "'Old Password' should be at lease 8 characters."
            },
            "change_newPassword": {
                required: "'Password' should not be empty.",
                minlength: "'Password' should be at least 8 characters.",
            },
            "change_confirmPassword": {
                required: "'Confirm Password' should not be empty.",
                minlength: "'Confirm Password' should be at least 8 characters.",
                equalTo: "'Password' and 'Confirm Password' do not match."
            },
        }
    });
}

function clearDatas(){
    $("#change_oldPassword").val('');
    $("#change_newPassword").val('');
    $("#change_confirmPassword").val('')
}






