function init() {
    initValidator();
    loadUsers();
}

function bind() {

    $("#btnReset").on("click", function (e) {
        $("form").trigger("reset");
        clearOldValidationErrorMessages();
    });

    $("#btnCancel").on("click", function (e) {
        goToHomePage();
    });
}

function initValidator() {
    $("#frm-stock").validate({
        rules: {
            "code": {
                required: true,
                maxlength: 5
            },
            "quantity": {
                required: true
            },
            "originalPrice": {
                required: true
            },
            "sellPrice": {
                required: true
            },
            "userId":{
                required: true
            }
        },
        messages: {
            "code": {
                required: "'Code' should not be empty.",
                maxlength: "'Code' should not exceeds 5 characters."
            },
            "quantity": {
                required: "'Quantity' should not be empty."
            },
            "originalPrice": {
                required: "'Original Price' should not be empty."
            },
            "sellPrice": {
                required: "Sell Price' should not be empty."
            },
            "userId":{
                required: "Please Choose at least 'One' User."
            }
        }
    });
}

function loadUsers() {
    let criteria = {};
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'users/search/list',
        data: JSON.stringify(criteria),
        success: function (data) {
            let options = [];
            $.each(data, function (key, item) {
                let option = "<option value='" + item.id + "'>" + item.name + "</option>";
                options.push(option);
            });
            $("#userId").html(options).selectpicker('refresh');
        }
    });

}


