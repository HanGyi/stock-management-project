function init() {
    initValidator();
    loadCurrencies();
}

function bind() {

    $("#btnReset").on("click", function (e) {
        $("form").trigger("reset");
        clearOldValidationErrorMessages();
    });

    $("#btnCancel").on("click", function (e) {
        goToHomePage();
    });
}

function initValidator() {
    $("#frm-user").validate({
        rules: {
            "name": {
                required: true,
                maxlength: 50
            },
            "email": {
                required: true,
                maxlength: 50
            },
            "password": {
                required: getPageMode() === 'CREATE',
                minlength: 8
            },
            /*"confirmPassword": {
                required: getPageMode() === 'CREATE',
                minlength: 8,
                equalTo: "#password"
            },*/
            "currency": {
                required: true
            },
            "stockLimit": {
                required: true
            },
        },
        messages: {
            "name": {
                required: "'Name' should not be empty.",
                maxlength: "'Name' should not exceeds 50 characters."
            },
            "email": {
                required: "'Email' should not be empty.",
                maxlength: "'Email' should not exceeds 50 characters."
            },
            "password": {
                required: "'Password' should not be empty.",
                minlength: "'Password' should be at least 8 characters.",
            },
            /*"confirmPassword": {
                required: "'Confirm Password' should not be empty.",
                minlength: "'Confirm Password' should be atleast 8 characters.",
                equalTo: "'Password' and 'Confirm Password' do not match."
            },*/
            "currency": {
                required: "Currency' should not be empty.",
            },
            "stockLimit": {
                required: "'Stock Limit' should not be empty.",
            },
        }
    });
}

function loadCurrencies() {
    let criteria = {};
    $.ajax({
        type: "POST",
        url: getApiResourcePath() + 'currencies/search/list',
        data: JSON.stringify(criteria),
        success: function (data) {
            let options = [];
            $.each(data, function (key, item) {
                let option = "<option value='" + item.code + "'>" + item.name + "</option>";
                options.push(option);
            });
            $("#currency").html(options).selectpicker('refresh');
        }
    });

}


