<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Spring Boot Email using FreeMarker</title>
</head>
<body>
<div style="margin-top: 10px">Greetings, ${userName}</div>
<br/>
<div>
    <b>we would like to inform that your stock balance of ${stockCode} has been reached the limit that
        your predefined balance ${stockLimit} in system.
        <br/>
        The remaining balance is ${quantity}.
    </b>
</div>
<div> Have a nice day..!</div>
<br/>
<br/>
<div> Thanks and Best Regards,</div>
<div> Innovative Solution</div>
</body>
</html>

