package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.model.bean.ActionBean;
import com.cws.easybill.portal.merchantportal.model.criteria.ActionCriteria;
import com.cws.easybill.portal.merchantportal.model.criteria.CommonCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.ActionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

@SpringBootTest
public class ActionUnitTest extends BaseUnitTest {
	private Logger testLogger = LogManager.getLogger("testLogs." + getClass());

	@Autowired
	private ActionRepository repository;

	@Test(groups = {"fetch"})
	public void testSelectAll() throws Exception {
		ActionCriteria criteria = new ActionCriteria();
		List<ActionBean> results = repository.selectList(criteria);
		assertTrue(results.size() > 0, "Records should not be empty.");
		showEntriesOfCollection(results);
	}

	@Test(groups = {"fetch"})
	public void testSelectAllCount() throws Exception {
		ActionCriteria criteria = new ActionCriteria();
		long count = repository.selectCounts(criteria);
		assertTrue(count > 0, "Record count must greater than 0.");
		testLogger.info("Total counts ==> " + count);
	}

	@Test(groups = {"fetch"})
	public void testSelectByPrimaryKey() throws Exception {
		ActionBean result = repository.select(10011L);
		assertNotNull(result, "Expected record must not be null.");
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectByCriteria() throws Exception {
		ActionCriteria criteria = new ActionCriteria();
		criteria.setId(10011L);
		criteria.setPage("Dashboard");
		criteria.setActionName("dashboard");
		criteria.setDisplayName("Dashboard page for User");
		criteria.setActionType(ActionBean.ActionType.MAIN);
		criteria.setUrl("/dashboard");

		criteria.setIncludeIds(Arrays.asList(10011L, 10022L, 1003L));
		criteria.setExcludeIds(Arrays.asList(1005L, 1006L, 1007L));
		criteria.setOffset(0L);
		criteria.setLimit(10);
		criteria.setWord("page");
		criteria.setOrderBy("page");
		criteria.setOrder(CommonCriteria.Order.ASC);
		ActionBean result = repository.select(criteria);
		assertNotNull(result, "Expected record must not be null.");
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectListByCriteria() throws Exception {
		ActionCriteria criteria = new ActionCriteria();
		criteria.setPage("User");
		List<ActionBean> results = repository.selectList(criteria);
		assertTrue(results.size() > 0, "Records should not be empty.");
		showEntriesOfCollection(results);
	}

	@Test(groups = {"fetch"})
	public void testSelectCountByCriteria() throws Exception {
		ActionCriteria criteria = new ActionCriteria();
		criteria.setPage("User");
		long count = repository.selectCounts(criteria);
		assertTrue(count > 0, "Record count must greater than 0.");
		testLogger.info("Total counts ==> " + count);
	}

	@Test(groups = {"fetch"})
	public void testSelectAvailableActionsForAuthenticatedUser() throws Exception {
		List<String> results = repository.selectAvailableActionsForAuthenticatedUser(null, "portal-ui-app", Arrays.asList(1L));
		assertTrue(results.size() > 0, "Records should not be empty.");
		showEntriesOfCollection(results);
	}
}
