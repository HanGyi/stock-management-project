package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.model.bean.AdministratorRoleBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorRoleCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRoleRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;
@SpringBootTest
public class AdministratorRoleUnitTest extends BaseUnitTest {

	private Logger testLogger = LogManager.getLogger("testLogs." + getClass());

	@Autowired
	private AdministratorRoleRepository repository;

	@Test(groups = {"fetch"})
	public void testSelectAll() throws Exception {
		AdministratorRoleCriteria criteria = new AdministratorRoleCriteria();
		List<AdministratorRoleBean> results = repository.selectList(criteria);
		showEntriesOfCollection(results);
	}

	@Test(groups = {"fetch"})
	public void testSelectAllCount() throws Exception {
		AdministratorRoleCriteria criteria = new AdministratorRoleCriteria();
		long count = repository.selectCounts(criteria);
		testLogger.info("Total counts ==> " + count);
	}

	@Test(groups = {"fetch"})
	public void testSelectByKey1() throws Exception {
		Set<Long> relatedIds = repository.selectByKey1(1L);
		showEntriesOfCollection(relatedIds);
	}

	@Test(groups = {"fetch"})
	public void testSelectByKey2() throws Exception {
		Set<Long> relatedIds = repository.selectByKey2(2L);
		showEntriesOfCollection(relatedIds);
	}

	@Test(groups = {"fetch"})
	public void testSelectByKeys() throws Exception {
		AdministratorRoleBean result = repository.select(1L, 1L);
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectByCriteria() throws Exception {
		AdministratorRoleCriteria criteria = new AdministratorRoleCriteria();
		criteria.setAdministratorId(1L);
		criteria.setRoleId(1L);
		criteria.setWithAdministrator(true);
		criteria.setWithRole(true);
		List<AdministratorRoleBean> results = repository.selectList(criteria);
		showEntriesOfCollection(results);
	}

	@Test(groups = {"insert"})
	public void testInsertSingle() throws Exception {
		AdministratorRoleBean record = new AdministratorRoleBean(1L, 1L);
		repository.insert(record, TEST_CREATE_USER_ID);
	}

	@Test(groups = {"insert"})
	public void testInsertSingleByKeys() throws Exception {
		repository.insert(1L, 2L, TEST_CREATE_USER_ID);
	}

	@Test(groups = {"delete"})
	public void testDeleteByKeys() throws Exception {
		long totalAffectedRows = repository.delete(1L, 1L, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows ==> " + totalAffectedRows);
	}
}
