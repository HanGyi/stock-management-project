package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.AdministratorBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.criteria.AdministratorCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.AdministratorRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
public class AdministratorUnitTest extends BaseUnitTest {

	private Logger testLogger = LogManager.getLogger("testLogs." + getClass());

	@Autowired
	private AdministratorRepository repository;

	@Test(groups = {"fetch"})
	public void testSelectAll() throws Exception {
		AdministratorCriteria criteria = new AdministratorCriteria();
		List<AdministratorBean> results = repository.selectList(criteria);
		showEntriesOfCollection(results);
	}

	@Test(groups = {"fetch"})
	public void testSelectByPrimaryKey() throws Exception {
		AdministratorBean result = repository.select(1002L);
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectAllCount() throws Exception {
		AdministratorCriteria criteria = new AdministratorCriteria();
		long count = repository.selectCounts(criteria);
		testLogger.info("Total counts ==> " + count);
	}

	@Test(groups = {"fetch"})
	public void testSelectByCriteria() throws Exception {
		AdministratorCriteria criteria = new AdministratorCriteria();
		criteria.setIncludeIds(Arrays.asList(1L, 2L, 3L));
		criteria.setExcludeIds(Arrays.asList(7L, 8L, 9L));
		criteria.setWithStaticContent(true);

		criteria.setName("Super User");
		criteria.setStatus(AuthenticatedClientBean.Status.ACTIVE);
		AdministratorBean result = repository.select(criteria);
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectAuthenticatedClientInfo() throws Exception {
		AuthenticatedClientBean result = repository.selectAuthenticatedClient("Smith");
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"insert"})
	public void insertSingle() throws DAOException, DuplicatedEntryException {
		AdministratorBean record = new AdministratorBean();
		//record.setContentId(1L);
		record.setName("Mg Mg");
		record.setLoginId("Mg Mg");
		record.setPassword("sample_password");
		record.setUserType(AuthenticatedClientBean.ClientType.USER.ordinal());
		record.setStatus(AuthenticatedClientBean.Status.ACTIVE);

		long lastInsertedRecordId = repository.insert(record, TEST_CREATE_USER_ID);
		testLogger.info("Last inserted ID ==> " + lastInsertedRecordId);
	}

	@Test(groups = {"insert"})
	public void insertMulti() throws DAOException, DuplicatedEntryException {

		List<AdministratorBean> records = new ArrayList<>();

		AdministratorBean record1 = new AdministratorBean();
		//record1.setContentId(2L);
		record1.setName("Kyaw Kyaw");
		record1.setPassword("sample_password");
		record1.setStatus(AuthenticatedClientBean.Status.ACTIVE);
		record1.setLoginId("Kyaw Kyaw");
		record1.setUserType(AuthenticatedClientBean.ClientType.ADMINISTRATOR.ordinal());
		records.add(record1);

		AdministratorBean record2 = new AdministratorBean();
		//record2.setContentId(3L);
		record2.setName("Hla Hla");
		record2.setPassword("sample_password");
		record2.setStatus(AuthenticatedClientBean.Status.ACTIVE);
		record2.setLoginId("Hla Hla");
		record2.setUserType(AuthenticatedClientBean.ClientType.ADMINISTRATOR.ordinal());
		records.add(record2);

		repository.insert(records, TEST_CREATE_USER_ID);
	}

	@Test(groups = {"update"})
	public void testSingleRecordUpdate() throws Exception {
		AdministratorBean record = new AdministratorBean();
		record.setId(1002L);
		record.setName("Ma Ma");
		record.setPassword("mamaP@ssword");
		record.setStatus(AuthenticatedClientBean.Status.ACTIVE);

		long totalAffectedRows = repository.update(record, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}

	@Test(groups = {"update"})
	public void testUpdateByCriteria() throws Exception {
		AdministratorCriteria criteria = new AdministratorCriteria();
		criteria.setId(1003L);
		criteria.setIncludeIds(Arrays.asList(3L, 5L , 1003L));
		criteria.setExcludeIds(Arrays.asList(1L, 2L));

		HashMap<String, Object> updateItems = new HashMap<>();
		updateItems.put("status", 2);
		repository.update(criteria, updateItems, TEST_UPDATE_USER_ID);
	}

	@Test(groups = {"delete"})
	public void testDeleteByPrimaryKey() throws Exception {
		long totalAffectedRows = repository.delete(1003, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}

	@Test(groups = {"delete"})
	public void testDeleteByCriteria() throws Exception {
		AdministratorCriteria criteria = new AdministratorCriteria();
		criteria.setIncludeIds(Arrays.asList(1L, 2L, 1003L));
		criteria.setExcludeIds(Arrays.asList(7L, 8L, 9L));

		criteria.setName("Smith");
		criteria.setStatus(AuthenticatedClientBean.Status.ACTIVE);
		long totalAffectedRows = repository.delete(criteria, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}
}
