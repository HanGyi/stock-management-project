package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.TestConfig;
import com.cws.easybill.portal.merchantportal.config.MerchantPortalDataSourceConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;
import java.util.Collection;

@ActiveProfiles("dev")
@SpringBootTest(classes = TestConfig.class, webEnvironment = WebEnvironment.NONE)
@Rollback
@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class, transactionManager = MerchantPortalDataSourceConfig.TX_MANAGER)
public class BaseUnitTest extends AbstractTransactionalTestNGSpringContextTests {
	protected static final long TEST_CREATE_USER_ID = 10009L;
	protected static final long TEST_UPDATE_USER_ID = 90001L;
	private static final Logger testLogger = LogManager.getLogger("testLogs." + BaseUnitTest.class.getName());

	@BeforeMethod
	public void beforeMethod(Method method) {
		testLogger.info("***** Unit-TEST : Testing method '" + method.getName() + "' has started. *****");
		MockitoAnnotations.initMocks(this); // This could be pulled up into a shared base class
	}

	@AfterMethod
	public void afterMethod(Method method) {
		testLogger.info("----- Unit-TEST : Testing method '" + method.getName() + "' has finished. -----");
	}

	protected <T> void showEntriesOfCollection(Collection<T> collection) {
		if (collection != null) {
			for (Object obj : collection) {
				testLogger.info(" >>> " + obj.toString());
			}
		}
	}
}
