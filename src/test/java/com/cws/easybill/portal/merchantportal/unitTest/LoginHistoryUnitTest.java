package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.bean.LoginHistoryBean;
import com.cws.easybill.portal.merchantportal.model.criteria.LoginHistoryCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.LoginHistoryRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

@SpringBootTest
public class LoginHistoryUnitTest extends BaseUnitTest {

	private Logger testLogger = LogManager.getLogger("testLogs." + getClass());

	@Autowired
	private LoginHistoryRepository repository;

	@Test(groups = {"fetch"})
	public void testSelectAll() throws Exception {
		LoginHistoryCriteria criteria = new LoginHistoryCriteria();
		List<LoginHistoryBean> results = repository.selectList(criteria);
		assertTrue(results.size() > 0, "Records should not be empty.");
		showEntriesOfCollection(results);
	}

	@Test(groups = {"fetch"})
	public void testSelectAllCount() throws Exception {
		LoginHistoryCriteria criteria = new LoginHistoryCriteria();
		long count = repository.selectCounts(criteria);
		assertTrue(count > 0, "Record count must greater than 0.");
		testLogger.info("Total counts ==> " + count);
	}

	@Test(groups = {"fetch"})
	public void testSelectByPrimaryKey() throws Exception {
		LoginHistoryBean result = repository.select(2L);
		assertNotNull(result, "Expected record must not be null.");
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectByCriteria() throws Exception {
		LoginHistoryCriteria criteria = new LoginHistoryCriteria();
		criteria.setId(6L);
		criteria.setClientId(1000L);
		criteria.setClientType(AuthenticatedClientBean.ClientType.ADMINISTRATOR);
		criteria.setIncludeIds(Arrays.asList(1L, 2L, 6L));
		criteria.setExcludeIds(Arrays.asList(5L, 8L, 7L));
		LoginHistoryBean result = repository.select(criteria);
		assertNotNull(result, "Expected record must not be null.");
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectListByCriteria() throws Exception {
		LoginHistoryCriteria criteria = new LoginHistoryCriteria();
		criteria.setClientId(1L);
		criteria.setClientType(AuthenticatedClientBean.ClientType.SUPER_USER);
		List<LoginHistoryBean> results = repository.selectList(criteria);
		assertTrue(results.size() > 0, "Records should not be empty.");
		showEntriesOfCollection(results);
	}

	@Test(groups = {"insert"})
	public void insertSingle() throws DAOException, DuplicatedEntryException {
		LoginHistoryBean record = new LoginHistoryBean();
		record.setClientId(2L);
		record.setClientType(AuthenticatedClientBean.ClientType.ADMINISTRATOR);
		record.setIpAddress("127.0.0.1");
		record.setOs("Microsoft Window");
		record.setClientAgent("Mozilla Firefox");
		record.setLoginDate(LocalDateTime.now());
		long lastInsertedRecordId = repository.insert(record, TEST_CREATE_USER_ID);
		testLogger.info("Last inserted ID ==> " + lastInsertedRecordId);

		LoginHistoryBean result = repository.select(lastInsertedRecordId);
		assertNotNull(result, "Expected record must not be null.");
	}

	@Test(groups = {"insert"})
	public void insertMulti() throws DAOException, DuplicatedEntryException {

		List<LoginHistoryBean> records = new ArrayList<>();

		LoginHistoryBean record1 = new LoginHistoryBean();
		record1.setClientId(2L);
		record1.setClientType(AuthenticatedClientBean.ClientType.ADMINISTRATOR);
		record1.setIpAddress("192.168.0.1");
		record1.setOs("Linux Ubuntu");
		record1.setClientAgent("Opera");
		record1.setLoginDate(LocalDateTime.now());
		records.add(record1);

		LoginHistoryBean record2 = new LoginHistoryBean();
		record2.setClientType(AuthenticatedClientBean.ClientType.ADMINISTRATOR);
		record2.setClientId(3L);
		record2.setIpAddress("192.168.0.2");
		record2.setOs("Mac OSX");
		record2.setClientAgent("Google Chrome");
		record2.setLoginDate(LocalDateTime.now());
		records.add(record2);

		repository.insert(records, TEST_CREATE_USER_ID);
	}

	@Test(groups = {"delete"})
	public void testDeleteByPrimaryKey() throws Exception {
		long totalAffectedRows = repository.delete(2, TEST_UPDATE_USER_ID);
		assertEquals(totalAffectedRows, 1, "Total affected rows should be one record.");
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}

	@Test(groups = {"delete"})
	public void testDeleteByCriteria() throws Exception {
		LoginHistoryCriteria criteria = new LoginHistoryCriteria();
		criteria.setClientId(1L);

		criteria.setIncludeIds(Arrays.asList(1L, 2L, 3L));
		criteria.setExcludeIds(Arrays.asList(5L, 6L, 7L));
		long totalAffectedRows = repository.delete(criteria, TEST_UPDATE_USER_ID);
		assertTrue(totalAffectedRows >= 1, "Total affected rows should be at least one.");
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}
}
