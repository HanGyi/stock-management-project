package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.stock.OrderBean;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.OrderCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.OrderRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
public class OrderUnitTest extends BaseUnitTest{

    private Logger testLogger = LogManager.getLogger("testLogs." + getClass());

    @Autowired
    private OrderRepository repository;

    @Test(groups = {"fetch"})
    public void testSelectAll() throws Exception {
        OrderCriteria criteria = new OrderCriteria();
        List<OrderBean> results = repository.selectList(criteria);
        showEntriesOfCollection(results);
    }

    @Test(groups = {"fetch"})
    public void testSelectByPrimaryKey() throws Exception {
        OrderBean result = repository.select(7L);
        testLogger.info("Result ==> " + result);
    }

    @Test(groups = {"fetch"})
    public void testSelectAllCount() throws Exception {
        OrderCriteria criteria = new OrderCriteria();
        long count = repository.selectCounts(criteria);
        testLogger.info("Total counts ==> " + count);
    }

    @Test(groups = {"fetch"})
    public void testSelectByCriteria() throws Exception {
        OrderCriteria criteria = new OrderCriteria();
        criteria.setIncludeIds(Arrays.asList(7L, 8L,93L));
        criteria.setExcludeIds(Arrays.asList(1L, 2L, 3L));

        criteria.setStockCode("A0001");
        criteria.setUserId(1L);
        OrderBean result = repository.select(criteria);
        testLogger.info("Result ==> " + result);
    }

    @Test(groups = {"insert"})
    public void insertSingle() throws DAOException, DuplicatedEntryException {
        OrderBean record = new OrderBean();
        record.setStockCode("AZ001");
        record.setQuantity(10);
        record.setAmount(new BigDecimal(2000));
        record.setTotalAmount(new BigDecimal(20000));
        record.setUserId(1L);

        long lastInsertedRecordId = repository.insert(record, TEST_CREATE_USER_ID);
        testLogger.info("Last inserted ID ==> " + lastInsertedRecordId);
    }

    @Test(groups = {"insert"})
    public void insertMulti() throws DAOException, DuplicatedEntryException {

        List<OrderBean> records = new ArrayList<>();

        OrderBean record1 = new OrderBean();
        record1.setStockCode("AZ001");
        record1.setQuantity(10);
        record1.setAmount(new BigDecimal(2000));
        record1.setTotalAmount(new BigDecimal(20000));
        record1.setUserId(1L);
        records.add(record1);

        OrderBean record2 = new OrderBean();
        record2.setStockCode("ZA001");
        record2.setQuantity(2);
        record2.setAmount(new BigDecimal(3000));
        record2.setTotalAmount(new BigDecimal(6000));
        record2.setUserId(2L);
        records.add(record2);

        repository.insert(records, TEST_CREATE_USER_ID);
    }

    @Test(groups = {"update"})
    public void testSingleRecordUpdate() throws Exception {
        OrderBean record = new OrderBean();
        record.setId(7L);
        record.setStockCode("AZ001");
        record.setQuantity(10);
        record.setAmount(new BigDecimal(2000));
        record.setTotalAmount(new BigDecimal(20000));
        record.setUserId(1L);

        long totalAffectedRows = repository.update(record, TEST_CREATE_USER_ID);
        testLogger.info("Total Affected rows = " + totalAffectedRows);
    }

    @Test(groups = {"update"})
    public void testUpdateByCriteria() throws Exception {
        OrderCriteria criteria = new OrderCriteria();
        criteria.setId(1L);
        criteria.setIncludeIds(Arrays.asList(1L,3L, 5L));
        criteria.setExcludeIds(Arrays.asList(11L, 2L));

        HashMap<String, Object> updateItems = new HashMap<>();
        updateItems.put("stockCode", "ABC");
        repository.update(criteria, updateItems, TEST_CREATE_USER_ID);
    }

    @Test(groups = {"delete"})
    public void testDeleteByPrimaryKey() throws Exception {
        long totalAffectedRows = repository.delete(7, TEST_CREATE_USER_ID);
        testLogger.info("Total Affected rows = " + totalAffectedRows);
    }

    @Test(groups = {"delete"})
    public void testDeleteByCriteria() throws Exception {
        OrderCriteria criteria = new OrderCriteria();
        criteria.setIncludeIds(Arrays.asList(1L, 2L, 7L));
        criteria.setExcludeIds(Arrays.asList(3L, 8L, 9L));

        criteria.setStockCode("A0001");
        long totalAffectedRows = repository.delete(criteria, TEST_CREATE_USER_ID);
        testLogger.info("Total Affected rows = " + totalAffectedRows);
    }
}
