package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.model.bean.RoleActionBean;
import com.cws.easybill.portal.merchantportal.model.criteria.RoleActionCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.RoleActionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;

@SpringBootTest
public class RoleActionUnitTest extends BaseUnitTest {

	private Logger testLogger = LogManager.getLogger("testLogs." + getClass());

	@Autowired
	private RoleActionRepository repository;

	@Test(groups = {"fetch"})
	public void testSelectAll() throws Exception {
		RoleActionCriteria criteria = new RoleActionCriteria();
		List<RoleActionBean> results = repository.selectList(criteria);
		showEntriesOfCollection(results);
	}

	@Test(groups = {"fetch"})
	public void testSelectAllCount() throws Exception {
		RoleActionCriteria criteria = new RoleActionCriteria();
		long count = repository.selectCounts(criteria);
		testLogger.info("Total counts ==> " + count);
	}

	@Test(groups = {"fetch"})
	public void testSelectByKey1() throws Exception {
		Set<Long> relatedIds = repository.selectByKey1(1L);
		showEntriesOfCollection(relatedIds);
	}

	@Test(groups = {"fetch"})
	public void testSelectByKey2() throws Exception {
		Set<Long> relatedIds = repository.selectByKey2(2001L);
		showEntriesOfCollection(relatedIds);
	}

	@Test(groups = {"fetch"})
	public void testSelectByKeys() throws Exception {
		RoleActionBean result = repository.select(1L, 10011L);
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectByCriteria() throws Exception {
		RoleActionCriteria criteria = new RoleActionCriteria();
		criteria.setRoleId(1L);
		criteria.setActionId(10011L);
		criteria.setWithRole(true);
		criteria.setWithAction(true);
		List<RoleActionBean> results = repository.selectList(criteria);
		showEntriesOfCollection(results);
	}

	@Test(groups = {"insert"})
	public void testInsertSingle() throws Exception {
		RoleActionBean record = new RoleActionBean(3L, 10021L);
		repository.insert(record, TEST_CREATE_USER_ID);
	}

	@Test(groups = {"insert"})
	public void testInsertSingleByKeys() throws Exception {
		repository.insert(3L, 10021L, TEST_CREATE_USER_ID);
	}

	@Test(groups = {"delete"})
	public void testDeleteByKeys() throws Exception {
		long totalAffectedRows = repository.delete(1L, 2001L, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows ==> " + totalAffectedRows);
	}
}
