package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.RoleBean;
import com.cws.easybill.portal.merchantportal.model.criteria.RoleCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.RoleRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
public class RoleUnitTest extends BaseUnitTest {

	private Logger testLogger = LogManager.getLogger("testLogs." + getClass());

	@Autowired
	private RoleRepository repository;

	@Test(groups = {"fetch"})
	public void testSelectAll() throws Exception {
		RoleCriteria criteria = new RoleCriteria();
		List<RoleBean> results = repository.selectList(criteria);
		showEntriesOfCollection(results);
	}

	@Test(groups = {"fetch"})
	public void testSelectByPrimaryKey() throws Exception {
		RoleBean result = repository.select(2L);
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectAllCount() throws Exception {
		RoleCriteria criteria = new RoleCriteria();
		long count = repository.selectCounts(criteria);
		testLogger.info("Total counts ==> " + count);
	}

	@Test(groups = {"fetch"})
	public void testSelectByCriteria() throws Exception {
		RoleCriteria criteria = new RoleCriteria();
		criteria.setIncludeIds(Arrays.asList(1L, 2L, 3L));
		criteria.setExcludeIds(Arrays.asList(7L, 8L, 9L));

		criteria.setName("Administrator");
		RoleBean result = repository.select(criteria);
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectRolesByActionUrl() throws Exception {
		List<String> results = repository.selectRolesByActionUrl("portal-ui-app", "/dashboard");
		showEntriesOfCollection(results);
	}

	@Test(groups = {"insert"})
	public void insertSingle() throws DAOException, DuplicatedEntryException {
		RoleBean record = new RoleBean();
		record.setName("MANAGER");
		record.setAppName("sample-ui-app");
		record.setRoleType(RoleBean.RoleType.BUILT_IN);
		record.setDescription("This role is for Manager users.");
		long lastInsertedRecordId = repository.insert(record, TEST_CREATE_USER_ID);
		testLogger.info("Last inserted ID ==> " + lastInsertedRecordId);
	}

	@Test(groups = {"insert"})
	public void insertMulti() throws DAOException, DuplicatedEntryException {

		List<RoleBean> records = new ArrayList<>();

		RoleBean record1 = new RoleBean();
		record1.setName("ACCOUNTANT");
		record1.setAppName("sample-ui-app");
		record1.setRoleType(RoleBean.RoleType.BUILT_IN);
		record1.setDescription("This role is for Accountant users");
		records.add(record1);

		RoleBean record2 = new RoleBean();
		record2.setName("HR_MANAGER");
		record2.setAppName("sample-ui-app");
		record2.setRoleType(RoleBean.RoleType.CUSTOM);
		record2.setDescription("this role is for HR-Managers");
		records.add(record2);

		repository.insert(records, TEST_CREATE_USER_ID);
	}

	@Test(groups = {"update"})
	public void testSingleRecordUpdate() throws Exception {
		RoleBean record = new RoleBean();
		record.setId(2L);
		record.setName("RECEPTIONIST");
		record.setDescription("This role is for receiptionists.");
		long totalAffectedRows = repository.update(record, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}

	@Test(groups = {"update"})
	public void testUpdateByCriteria() throws Exception {
		RoleCriteria criteria = new RoleCriteria();
		criteria.setId(3L);
		criteria.setIncludeIds(Arrays.asList(3L, 2L, 5L));
		criteria.setExcludeIds(Arrays.asList(1L, 2L));

		HashMap<String, Object> updateItems = new HashMap<>();
		updateItems.put("name", "WEB_DEVELOPER");
		repository.update(criteria, updateItems, TEST_UPDATE_USER_ID);
	}

	@Test(groups = {"delete"})
	public void testDeleteByPrimaryKey() throws Exception {
		long totalAffectedRows = repository.delete(2, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}

	@Test(groups = {"delete"})
	public void testDeleteByCriteria() throws Exception {
		RoleCriteria criteria = new RoleCriteria();
		criteria.setIncludeIds(Arrays.asList(1L, 2L, 3L));
		criteria.setExcludeIds(Arrays.asList(7L, 8L, 9L));

		criteria.setName("Administrator");
		long totalAffectedRows = repository.delete(criteria, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}
}
