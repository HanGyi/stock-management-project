package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.stock.StockBean;
import com.cws.easybill.portal.merchantportal.model.criteria.stock.StockCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.stock.StockRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
public class StockUnitTest extends BaseUnitTest{
    
    private Logger testLogger = LogManager.getLogger("testLogs." + getClass());

    @Autowired
    private StockRepository repository;

    @Test(groups = {"fetch"})
    public void testSelectAll() throws Exception {
        StockCriteria criteria = new StockCriteria();
        List<StockBean> results = repository.selectList(criteria);
        showEntriesOfCollection(results);
    }

    @Test(groups = {"fetch"})
    public void testSelectByPrimaryKey() throws Exception {
        StockBean result = repository.select(1L);
        testLogger.info("Result ==> " + result);
    }

    @Test(groups = {"fetch"})
    public void testSelectAllCount() throws Exception {
        StockCriteria criteria = new StockCriteria();
        long count = repository.selectCounts(criteria);
        testLogger.info("Total counts ==> " + count);
    }

    @Test(groups = {"fetch"})
    public void testSelectByCriteria() throws Exception {
        StockCriteria criteria = new StockCriteria();
        criteria.setIncludeIds(Arrays.asList(1L, 2L, 3L));
        criteria.setExcludeIds(Arrays.asList(7L, 8L, 9L));

        criteria.setCode("A0001");
        criteria.setQuantity(2000L);
        criteria.setWithExactValue(true);
        StockBean result = repository.select(criteria);
        testLogger.info("Result ==> " + result);
    }

    @Test(groups = {"insert"})
    public void insertSingle() throws DAOException, DuplicatedEntryException {
        StockBean record = new StockBean();
        record.setCode("AZ001");
        record.setQuantity(200L);
        record.setOriginalPrice(new BigDecimal(2000));
        record.setSellPrice(new BigDecimal(4000));
        record.setUserId(1L);

        long lastInsertedRecordId = repository.insert(record, TEST_CREATE_USER_ID);
        testLogger.info("Last inserted ID ==> " + lastInsertedRecordId);
    }

    @Test(groups = {"insert"})
    public void insertMulti() throws DAOException, DuplicatedEntryException {

        List<StockBean> records = new ArrayList<>();

        StockBean record1 = new StockBean();
        record1.setCode("AZ001");
        record1.setQuantity(200L);
        record1.setOriginalPrice(new BigDecimal(2000));
        record1.setSellPrice(new BigDecimal(4000));
        record1.setUserId(1L);
        records.add(record1);

        StockBean record2 = new StockBean();
        record2.setCode("AB089");
        record2.setQuantity(1000L);
        record2.setOriginalPrice(new BigDecimal(45000));
        record2.setSellPrice(new BigDecimal(60000));
        record2.setUserId(2L);
        records.add(record2);

        repository.insert(records, TEST_CREATE_USER_ID);
    }

    @Test(groups = {"update"})
    public void testSingleRecordUpdate() throws Exception {
        StockBean record = new StockBean();
        record.setId(1L);
        record.setCode("GGXX9");
        record.setSellPrice(new BigDecimal(15000));
        record.setOriginalPrice(new BigDecimal(20000));
        record.setUserId(2L);

        long totalAffectedRows = repository.update(record, TEST_CREATE_USER_ID);
        testLogger.info("Total Affected rows = " + totalAffectedRows);
    }

    @Test(groups = {"update"})
    public void testUpdateByCriteria() throws Exception {
        StockCriteria criteria = new StockCriteria();
        criteria.setId(1L);
        criteria.setIncludeIds(Arrays.asList(1L,3L, 5L));
        criteria.setExcludeIds(Arrays.asList(11L, 2L));

        HashMap<String, Object> updateItems = new HashMap<>();
        updateItems.put("code", "ABC");
        repository.update(criteria, updateItems, TEST_CREATE_USER_ID);
    }

    @Test(groups = {"delete"})
    public void testDeleteByPrimaryKey() throws Exception {
        long totalAffectedRows = repository.delete(3, TEST_CREATE_USER_ID);
        testLogger.info("Total Affected rows = " + totalAffectedRows);
    }

    @Test(groups = {"delete"})
    public void testDeleteByCriteria() throws Exception {
        StockCriteria criteria = new StockCriteria();
        criteria.setIncludeIds(Arrays.asList(1L, 2L, 3L));
        criteria.setExcludeIds(Arrays.asList(7L, 8L, 9L));

        criteria.setCode("A0001");
        long totalAffectedRows = repository.delete(criteria, TEST_CREATE_USER_ID);
        testLogger.info("Total Affected rows = " + totalAffectedRows);
    }
}
