package com.cws.easybill.portal.merchantportal.unitTest;

import com.cws.easybill.portal.merchantportal.exception.DAOException;
import com.cws.easybill.portal.merchantportal.exception.DuplicatedEntryException;
import com.cws.easybill.portal.merchantportal.model.bean.UserBean;
import com.cws.easybill.portal.merchantportal.model.bean.AuthenticatedClientBean;
import com.cws.easybill.portal.merchantportal.model.criteria.UserCriteria;
import com.cws.easybill.portal.merchantportal.persistence.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
public class UserUnitTest extends BaseUnitTest{
	private Logger testLogger = LogManager.getLogger("testLogs." + getClass());

	@Autowired
	private UserRepository repository;

	@Test(groups = {"fetch"})
	public void testSelectAll() throws Exception {
		UserCriteria criteria = new UserCriteria();
		List<UserBean> results = repository.selectList(criteria);
		showEntriesOfCollection(results);
	}

	@Test(groups = {"fetch"})
	public void testSelectByPrimaryKey() throws Exception {
		UserBean result = repository.select(1L);
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"fetch"})
	public void testSelectAllCount() throws Exception {
		UserCriteria criteria = new UserCriteria();
		long count = repository.selectCounts(criteria);
		testLogger.info("Total counts ==> " + count);
	}

	@Test(groups = {"fetch"})
	public void testSelectByCriteria() throws Exception {
		UserCriteria criteria = new UserCriteria();
		criteria.setIncludeIds(Arrays.asList(1L, 2L, 3L));
		criteria.setExcludeIds(Arrays.asList(7L, 8L, 9L));
		criteria.setWithStaticContent(true);

		criteria.setName("Super User");
		criteria.setStatus(UserBean.Status.ON);
		UserBean result = repository.select(criteria);
		testLogger.info("Result ==> " + result);
	}

	@Test(groups = {"insert"})
	//@Rollback(value = false)
	public void insertSingle() throws DAOException, DuplicatedEntryException {
		UserBean record = new UserBean();
		record.setName("Mg Mg");
		record.setPassword("sample password");
		record.setEmail("sample email");
		record.setStockLimit(20L);
		record.setCurrency("MMK");
		record.setStatus(UserBean.Status.ON);

		long lastInsertedRecordId = repository.insert(record, TEST_CREATE_USER_ID);
		testLogger.info("Last inserted ID ==> " + lastInsertedRecordId);
	}

	@Test(groups = {"insert"})
	public void insertMulti() throws DAOException, DuplicatedEntryException {

		List<UserBean> records = new ArrayList<>();

		UserBean record1 = new UserBean();
		record1.setName("Mg Mg");
		record1.setPassword("sample password");
		record1.setEmail("sample email");
		record1.setStockLimit(20L);
		record1.setCurrency("MMK");
		record1.setStatus(UserBean.Status.ON);
		records.add(record1);

		UserBean record2 = new UserBean();
		record2.setName("Hla Hla");
		record2.setPassword("sample_password");
		record2.setEmail("sample email");
		record2.setStockLimit(50L);
		record2.setStatus(UserBean.Status.OFF);
		record2.setCurrency("CNY");
		records.add(record2);

		repository.insert(records, TEST_CREATE_USER_ID);
	}

	@Test(groups = {"update"})
	public void testSingleRecordUpdate() throws Exception {
		UserBean record = new UserBean();
		record.setId(3L);
		record.setName("Ma Ma");
		record.setPassword("mamaP@ssword");
		record.setStatus(UserBean.Status.OFF);

		long totalAffectedRows = repository.update(record, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}

	@Test(groups = {"update"})
	public void testUpdateByCriteria() throws Exception {
		UserCriteria criteria = new UserCriteria();
		criteria.setId(3L);
		criteria.setIncludeIds(Arrays.asList(3L, 5L));
		criteria.setExcludeIds(Arrays.asList(1L, 2L));

		HashMap<String, Object> updateItems = new HashMap<>();
		updateItems.put("status", 2);
		repository.update(criteria, updateItems, TEST_UPDATE_USER_ID);
	}

	@Test(groups = {"delete"})
	public void testDeleteByPrimaryKey() throws Exception {
		long totalAffectedRows = repository.delete(3, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}

	@Test(groups = {"delete"})
	public void testDeleteByCriteria() throws Exception {
		UserCriteria criteria = new UserCriteria();
		criteria.setIncludeIds(Arrays.asList(1L, 2L, 3L));
		criteria.setExcludeIds(Arrays.asList(7L, 8L, 9L));

		criteria.setName("Smith");
		criteria.setStatus(UserBean.Status.ON);
		long totalAffectedRows = repository.delete(criteria, TEST_UPDATE_USER_ID);
		testLogger.info("Total Affected rows = " + totalAffectedRows);
	}
}
